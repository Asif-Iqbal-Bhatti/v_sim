#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: set fileencoding=utf-8

## EXTRAITS DE LA LICENCE
## Copyright CEA, contributeurs : Damien CALISTE, laboratoire L_Sim, (2012-2012)
## Adresse mèl :
## CALISTE, damien P caliste AT cea P fr.

## Ce logiciel est un programme informatique servant à visualiser des
## structures atomiques dans un rendu pseudo-3D. 
## Ce logiciel est régi par la licence CeCILL soumise au droit français et
## respectant les principes de diffusion des logiciels libres. Vous pouvez
## utiliser, modifier et/ou redistribuer ce programme sous les conditions
## de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
## sur le site "http://www.cecill.info".
## Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
## pris connaissance de la licence CeCILL, et que vous en avez accepté les
## termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).

## LICENCE SUM UP
## Copyright CEA, contributors: Damien CALISTE, L_Sim laboratory, (2012-2012)
## E-mail address:
## CALISTE, damien P caliste AT cea P fr.

## This software is a computer program whose purpose is to visualize atomic
## configurations in 3D.
## This software is governed by the CeCILL  license under French law and
## abiding by the rules of distribution of free software.  You can  use, 
## modify and/ or redistribute the software under the terms of the CeCILL
## license as circulated by CEA, CNRS and INRIA at the following URL
## "http://www.cecill.info". 
## The fact that you are presently reading this means that you have had
## knowledge of the CeCILL license and that you accept its terms. You can
## find a copy of this licence shipped with this software at
## Documentation/licence.en.txt.

from gi.repository import v_sim
from gi.repository import Gtk, GLib

def onMeasure(item, marks, nodeId):
  if marks.setInfos(nodeId, True):
    v_sim.Object.redrawForce("rightMenu script")

def onClean(item, marks, nodeId):
  if marks.setInfos(nodeId, False):
    v_sim.Object.redrawForce("rightMenu script")

def buildMenu(inter, node):
  menu = Gtk.Menu.new()

  marks = v_sim.UiMainClass.getDefaultRendering().getMarks()

  # The default contents (single node case).
  if node is not None:
    item = Gtk.MenuItem.new_with_label("Measure node neighbourhood")
    item.connect("activate", onMeasure, marks, node.number)
    menu.append(item)
    item = Gtk.MenuItem.new_with_label("Remove measures from node")
    item.connect("activate", onClean, marks, node.number)
    item.set_sensitive(marks.getActive(node.number))
    menu.append(item)

  # The user added items.
  for (lblNone, lblSingle, lblSelection, callback, kwargs) in inter.actionList.values():
    if node is None:
      item = Gtk.MenuItem.new_with_label(lblNone)
      item.connect("activate", callback, None, kwargs)
    else:
      item = Gtk.MenuItem.new_with_label(lblSingle)
      item.connect("activate", callback, node.number, kwargs)
    menu.append(item)
  
  return menu

def menuPosition(menu, (x, y)):
  return (x, y, True)

def onActionMenu(inter, x, y, node):
  print node
  menu = buildMenu(inter, node)
  menu.show_all()
  menu.popup(None, None, menuPosition, (x, y), 0, Gtk.get_current_event_time())

# Define a right clic menu on rendering window.
inter = v_sim.UiRenderingWindowClass.getInteractive()
if 'actionMenuId' in globals():
  inter.disconnect(actionMenuId)
actionMenuId = inter.connect("menu", onActionMenu)

# Add new methods to the interactive object of the rendering window.
def addNodeAction(self, id, labelNone, labelSingle, labelSelection, callback,
                  kwargs = {}):
  self.actionList[id] = (labelNone, labelSingle, labelSelection, callback, kwargs)

def removeNodeAction(self, id):
  self.actionList.pop(id, None)

# Add a list of custom action to inter.
if not(hasattr(v_sim.Interactive, "actionList")):
  setattr(v_sim.Interactive, "actionList", {})
  # Add methods to review this list.
  v_sim.Interactive.addNodeAction = addNodeAction
  v_sim.Interactive.removeNodeAction = removeNodeAction
