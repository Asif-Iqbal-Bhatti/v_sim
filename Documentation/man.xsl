<?xml version="1.0" encoding="iso-8859-15"?>
<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:date="http://exslt.org/dates-and-times"
  version="1.0">

  <xsl:output method="text" indent="no" encoding="utf-8"/>

  <xsl:variable name="now" select="date:date-time()"/>       
  <xsl:variable name="day" select="date:day-in-month($now)"/>       
  <xsl:variable name="month" select="date:month-name($now)"/>       
  <xsl:variable name="year" select="date:year($now)"/> 

  <xsl:template match="/commandLine">
    <xsl:text>
.\"                                      Hey, EMACS: -*- nroff -*-
.\" First parameter, NAME, should be all caps
.\" Second parameter, SECTION, should be 1-8, maybe w/ subsection
.\" other parameters are allowed: see man(7), man(1)
.TH "V_SIM" "1" "</xsl:text>
<xsl:value-of select="$month" />
<xsl:text> </xsl:text><xsl:value-of select="$day" />
<xsl:text>, </xsl:text><xsl:value-of select="$year" />
<xsl:text>"
.\" Please adjust this date whenever revising the manpage.
.\"
.\" Some roff macros, for reference:
.\" .nh        disable hyphenation
.\" .hy        enable hyphenation
.\" .ad l      left justify
.\" .ad b      justify to both left and right margins
.\" .nf        disable filling
.\" .fi        enable filling
.\" .br        insert line break
.\" .sp &lt;n&gt;    insert n+1 empty lines
.\" for manpage-specific macros, see man(7)
.SH NAME
v_sim \- a GTK program for 3D visualisation of atomic systems
.SH SYNOPSIS
.B v_sim</xsl:text>
<xsl:call-template name="synopsis" />
<xsl:text>
.PP
This manual page documents briefly the
.B v_sim
commands. It is a software to visualise atomic structures with OpenGl rendering.
.SH OPTIONS
.PP
This program follows the usual GNU command line syntax, with long
options starting with two dashes (`-').
.PP
A complete list of options is included below.
    </xsl:text>
    <xsl:call-template name="complete" />
    <xsl:text>
.SH FILES
.IR /usr/share/v_sim/v_sim.res
specify all rendering informations from position of the camera to colour of the elements.
.br
.IR /usr/share/v_sim/v_sim.par
specify all interface and non-rendering options such as the positions of the panels.
.br
.IR ~/$XDG_CONFIG_DIR/v_sim
a directory where overloading
.B v_sim.res
and
.B v_sim.par
can be put.
.SH AUTHOR
V_Sim was written by Luc Billard, Damien Caliste, Olivier D'Astier
Aur�lien Lherbier, J�r�my Blanc, Simon Plan�s, Thomas Jourdan and Yoann Ratao.
.PP
This manual page was written by Damien Caliste &lt;damien.caliste@cea.fr&gt;.
    </xsl:text>
  </xsl:template>

  <xsl:template name="synopsis">
    <xsl:for-each select="option">
      <xsl:text>
.B [</xsl:text>
      <xsl:if test="@short">
        <xsl:text>-</xsl:text><xsl:value-of select="@short" />
      </xsl:if>
      <xsl:if test="not(@short)">
        <xsl:text>--</xsl:text><xsl:value-of select="@name" />
      </xsl:if>
      <xsl:if test="description/@arg">
        <xsl:text>
.I </xsl:text><xsl:value-of select="description/@arg" />
      </xsl:if>
      <xsl:text>
.B ]</xsl:text>
    </xsl:for-each>

    <xsl:text>
.B [</xsl:text>
    <xsl:text>
.I fileToRender</xsl:text>
    <xsl:text>
.B ]</xsl:text>
  </xsl:template>

  <xsl:template name="complete">
    <xsl:for-each select="option">
      <xsl:text>
.TP</xsl:text>
      <xsl:if test="@short">
        <xsl:text>
.B \-</xsl:text><xsl:value-of select="@short" /><xsl:text>, </xsl:text>
      </xsl:if>
      <xsl:if test="not(@short)">
        <xsl:text>
.B   </xsl:text>
      </xsl:if>
      <xsl:text>\-\-</xsl:text><xsl:value-of select="@name" />
      <xsl:if test="description/@arg">
        <xsl:text> </xsl:text><xsl:value-of select="description/@arg" />
      </xsl:if>
      <xsl:text> (from v</xsl:text><xsl:value-of select="@version" /><xsl:text>.0)
</xsl:text>
      <xsl:value-of select="description/text()" />
      <xsl:if test="description/@default">
        <xsl:text> (Default value: </xsl:text><xsl:value-of select="description/@default" /><xsl:text>)</xsl:text>
      </xsl:if>
      <xsl:text>
</xsl:text>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="text()"></xsl:template>

</xsl:stylesheet>
