  V_Sim visualizes atomic structures such as crystals, grain boundaries, molecules and so on (either in binary format, or in plain text format).

  The rendering is done in pseudo-3D with spheres (atoms) or arrows (spins). The user can interact through many functions to choose the view, set the pairs, draw cutting planes, compute surfaces from scalar fields, duplicate nodes, measure geometry... Moreover V_Sim allows to export the view as images in PNG, JPG or scheme in PDF, SVG and other formats. Some tools are also available to colorize atoms from data values or to animate on screen many position files. The tool is fully scriptable with Python.

  A comprehensive manual is available on the web site, see https://l_sim.gitlab.io/v_sim/user_guide.html.

  Compiling and installing V_Sim is detailled at https://l_sim.gitlab.io/v_sim/install.html and in the INSTALL file of the package (providing dependencies information as well).

 � 1998 - 2023, CEA (France)
