/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2016)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2016)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef VISU_DATAATOMIC_H
#define VISU_DATAATOMIC_H

#include <glib.h>
#include <glib-object.h>

#include "visu_dataloadable.h"
#include "visu_dataloader.h"
#include "extraFunctions/vectorProp.h"
#include "iface_dumpable.h"

G_BEGIN_DECLS

/**
 * VISU_TYPE_DATA_ATOMIC:
 *
 * return the type of #VisuDataAtomic.
 */
#define VISU_TYPE_DATA_ATOMIC	     (visu_data_atomic_get_type ())
/**
 * VISU_DATA_ATOMIC:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuDataAtomic type.
 */
#define VISU_DATA_ATOMIC(obj)	     (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_DATA_ATOMIC, VisuDataAtomic))
/**
 * VISU_DATA_ATOMIC_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuDataAtomicClass.
 */
#define VISU_DATA_ATOMIC_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_DATA_ATOMIC, VisuDataAtomicClass))
/**
 * VISU_IS_DATA_ATOMIC:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuDataAtomic object.
 */
#define VISU_IS_DATA_ATOMIC(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_DATA_ATOMIC))
/**
 * VISU_IS_DATA_ATOMIC_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuDataAtomicClass class.
 */
#define VISU_IS_DATA_ATOMIC_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_DATA_ATOMIC))
/**
 * VISU_DATA_ATOMIC_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_DATA_ATOMIC_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_DATA_ATOMIC, VisuDataAtomicClass))

typedef struct _VisuDataAtomicPrivate VisuDataAtomicPrivate;
typedef struct _VisuDataAtomic VisuDataAtomic;

/**
 * VisuDataAtomic:
 *
 * Structure used to define #VisuDataAtomic objects.
 *
 * Since: 3.8
 */
struct _VisuDataAtomic
{
  VisuDataLoadable parent;

  VisuDataAtomicPrivate *priv;
};

/**
 * VisuDataAtomicClass:
 * @parent: the parent class.
 *
 * A short way to identify #_VisuDataAtomicClass structure.
 */
typedef struct _VisuDataAtomicClass VisuDataAtomicClass;
struct _VisuDataAtomicClass
{
  VisuDataLoadableClass parent;
};

/**
 * visu_data_atomic_get_type:
 *
 * This method returns the type of #VisuDataAtomic, use VISU_TYPE_DATA_ATOMIC instead.
 *
 * Returns: the type of #VisuDataAtomic.
 */
GType visu_data_atomic_get_type(void);

VisuDataAtomic* visu_data_atomic_new(const gchar *file, VisuDataLoader *format);

const gchar* visu_data_atomic_getFile(VisuDataAtomic *data, VisuDataLoader **format);
gboolean visu_data_atomic_loadAt(VisuDataAtomic *data, const gchar *filename, guint iSet, GCancellable *cancel, GError **error);

VisuNodeValuesVector* visu_data_atomic_getForces(VisuDataAtomic *dataObj,
                                                 gboolean create);

void visu_data_atomic_class_addLoader(VisuDataLoader *loader);
GList* visu_data_atomic_class_getLoaders(void);
const gchar* visu_data_atomic_class_getFileDescription(void);
void visu_data_atomic_class_finalize(void);

G_END_DECLS

#endif
