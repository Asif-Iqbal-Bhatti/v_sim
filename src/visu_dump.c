/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "visu_dump.h"

#include "visu_tools.h"

#include "dumpModules/dumpToAscii.h"
#include "dumpModules/dumpToXyz.h"
#include "dumpModules/dumpToYaml.h"
#include "dumpModules/dumpToABINIT.h"

#include "dumpModules/dumpToTiff.h"
#include "dumpModules/dumpToGif.h"
#include "dumpModules/dumpToPsAndPdf.h"
#include "dumpModules/dumpThroughGdkPixbuf.h"
#include "dumpModules/dumpToSVG.h"

/**
 * SECTION:visu_dump
 * @short_description: Some resources to add the ability to export the
 * rendered data to an other format (usually image format).
 *
 * <para>V_Sim can export loaded data to othe formats. This module
 * descibes the methods and structure to create a dumping
 * extension. Basically, a dumping extension is just a #ToolFileFormat and
 * a method that is called when exporting is required.</para>
 *
 * <para>The #writeDumpFunc should suspend its process to allow the
 * calling program to refresh itself if the dump process is
 * slow. Ideally, the argument @waitFunction should be called exactly
 * 100 times.</para>
 */

static GQuark quark;
static GList *allDumpModuleList = (GList*)0;

/**
 * VisuDump:
 * @bitmap: (in): TRUE if the format requires to export the view
 * into a bitmap ;
 * @glRequired: (in): TRUE if the method requires to run OpenGL.
 * @fileType: a #ToolFileFormat ;
 * @hasAlpha: TRUE if the format support alpha channel ;
 * @writeFunc: (scope call): a pointer to a write func.
 *
 * This structure is used to store a dumping extension. Such an
 * extension is characterized by its #ToolFileFormat and a method that can
 * write a file from the current rendered data.
 */

static void ensureDumps();

G_DEFINE_TYPE(VisuDump, visu_dump, TOOL_TYPE_FILE_FORMAT)

static void visu_dump_class_init(VisuDumpClass *klass _U_)
{
  g_debug("Visu Dump: creating the class of the object.");

  quark = g_quark_from_static_string("visu_dump");
}
static void visu_dump_init(VisuDump *obj)
{
  g_debug("Visu Dump: initializing a new object (%p).",
	      (gpointer)obj);

  allDumpModuleList = g_list_append(allDumpModuleList, obj);
}

/**
 * visu_dump_pool_getAllModules:
 *
 * All dumping extensions are stored in an opaque way in V_Sim. But
 * they can be listed by a call to this method.
 *
 * Returns: (transfer none) (element-type VisuDump*): a list of all
 * the known dumping extensions. This list is own by V_Sim and should
 * be considered read-only.
 */
GList* visu_dump_pool_getAllModules()
{
  ensureDumps();
  g_debug("Visu Dump: give list of dumps (%p).", (gpointer)allDumpModuleList);
  return allDumpModuleList;
}
/**
 * visu_dump_pool_finalize:
 *
 * Free the list of #VisuDump modules.
 *
 * Since: 3.8
 **/
void visu_dump_pool_finalize(void)
{
  g_list_free_full(allDumpModuleList, (GDestroyNotify)g_object_unref);
  allDumpModuleList = (GList*)0;
}

static void ensureDumps()
{
  static gboolean done = FALSE;

  if (done)
    return;

  g_debug("Visu Dump: Init export list.");
  visu_dump_ascii_getStatic();
  visu_dump_xyz_getStatic();
  visu_dump_yaml_getStatic();
  visu_dump_abinit_getStatic();
  visu_dump_jpeg_getStatic();
  visu_dump_png_getStatic();
  visu_dump_tiff_getStatic();
  visu_dump_cairo_svg_getStatic();
  visu_dump_cairo_pdf_getStatic();
  visu_dump_bitmap_pdf_getStatic();
  visu_dump_bitmap_ps_getStatic();
  dumpToGif_init();
  g_debug("Visu Dump: %d valid dump module(s) found.", g_list_length(allDumpModuleList));
  done = TRUE;
}
/**
 * visu_dump_getQuark: (skip)
 * 
 * Internal routine to get the #GQuark to handle error related to dump
 * actions.
 */
GQuark visu_dump_getQuark()
{
  return quark;
}
