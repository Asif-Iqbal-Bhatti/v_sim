/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2016)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2016)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include "ui_spin.h"

#include <support.h>
#include <interface.h>

#include <visu_dataspin.h>
#include <renderingMethods/elementSpin.h>

#include "ui_atomic.h"

/**
 * SECTION:ui_spin
 * @short_description: Defines a widget to setup a spin.
 *
 * <para>A set of widgets to setup the rendring of a spin.</para>
 */

/**
 * VisuUiSpinClass:
 * @parent: the parent class;
 *
 * A short way to identify #_VisuUiSpinClass structure.
 *
 * Since: 3.8
 */
/**
 * VisuUiSpin:
 *
 * An opaque structure.
 *
 * Since: 3.8
 */
/**
 * VisuUiSpinPrivate:
 *
 * Private fields for #VisuUiSpin objects.
 *
 * Since: 3.8
 */
struct _VisuUiSpinPrivate
{
  gboolean dispose_has_run;

  GtkWidget *comboShape;
  GtkWidget *hLengthSpin;
  GtkWidget *tLengthSpin;
  GtkWidget *hRadiusSpin;
  GtkWidget *tRadiusSpin;
  GtkWidget *tFollowCheck;
  GtkWidget *hFollowCheck;
  GtkWidget *ratioElipsoidSpin;
  GtkWidget *lengthElipsoidSpin;
  GtkWidget *elipFollowCheck;
  GtkWidget *expandAtomic;

  VisuUiAtomic *atomic;

  VisuNodeArrayRenderer *renderer;

  VisuElementSpin *model;
  GBinding *bind_shape, *bind_tl, *bind_tr, *bind_hl, *bind_hr, *bind_tf, *bind_hf;
  GBinding *bind_a, *bind_b, *bind_follow;

  GList *targets;
};

static void visu_ui_spin_finalize(GObject* obj);
static void visu_ui_spin_dispose(GObject* obj);

/* Local callbacks. */

G_DEFINE_TYPE_WITH_CODE(VisuUiSpin, visu_ui_spin, GTK_TYPE_BOX,
                        G_ADD_PRIVATE(VisuUiSpin))

static void visu_ui_spin_class_init(VisuUiSpinClass *klass)
{
  g_debug("Ui Spin: creating the class of the widget.");
  g_debug("                     - adding new signals ;");

  /* Connect freeing methods. */
  G_OBJECT_CLASS(klass)->dispose = visu_ui_spin_dispose;
  G_OBJECT_CLASS(klass)->finalize = visu_ui_spin_finalize;
}
static void visu_ui_spin_dispose(GObject *obj)
{
  VisuUiSpin *self = VISU_UI_SPIN(obj);
  g_debug("Ui Spin: dispose object %p.", (gpointer)obj);

  if (self->priv->dispose_has_run)
    return;
  self->priv->dispose_has_run = TRUE;

  visu_ui_spin_bind(self, (GList*)0);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_ui_spin_parent_class)->dispose(obj);
}
static void visu_ui_spin_finalize(GObject *obj)
{
  g_return_if_fail(obj);

  g_debug("Ui Spin: finalize object %p.", (gpointer)obj);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_ui_spin_parent_class)->finalize(obj);
  g_debug(" | freeing ... OK.");
}

static gboolean toArrowVisible(GBinding *bind _U_, const GValue *source_value,
                               GValue *target_value, gpointer data _U_)
{
  guint id;
  
  id = (guint)g_value_get_int(source_value);
  g_value_set_boolean(target_value, (id == VISU_ELEMENT_SPIN_ARROW_SMOOTH ||
                                     id == VISU_ELEMENT_SPIN_ARROW_SHARP));
  return TRUE;
}
static gboolean toElipVisible(GBinding *bind _U_, const GValue *source_value,
                               GValue *target_value, gpointer data _U_)
{
  guint id;
  
  id = (guint)g_value_get_int(source_value);
  g_value_set_boolean(target_value, (id == VISU_ELEMENT_SPIN_ELLIPSOID ||
                                     id == VISU_ELEMENT_SPIN_TORUS));
  return TRUE;
}
/* static gboolean toAtomic(GBinding *bind, const GValue *source_value _U_, */
/*                          GValue *target_value, gpointer data _U_) */
/* { */
/*   VisuDataSpinDrawingPolicy hide; */
/*   gboolean useAt; */

/*   g_object_get(g_binding_get_source(bind), "hiding-mode", &hide, */
/*                "use-atomic", &useAt, NULL); */
/*   g_value_set_boolean(target_value, (useAt || hide == VISU_DATA_SPIN_ATOMIC_NULL)); */
/*   return TRUE; */
/* } */
static void visu_ui_spin_init(VisuUiSpin *obj)
{
  GtkWidget *label, *hbox;
  GtkWidget *vboxArrowShape;
  GtkWidget *vboxElipsoidShape;
  int i;
  const gchar **shapes;

  gtk_orientable_set_orientation(GTK_ORIENTABLE(obj), GTK_ORIENTATION_VERTICAL);

  obj->priv = visu_ui_spin_get_instance_private(obj);
  obj->priv->dispose_has_run = FALSE;

  obj->priv->renderer = (VisuNodeArrayRenderer*)0;
  obj->priv->model = (VisuElementSpin*)0;
  obj->priv->targets = (GList*)0;

  gtk_widget_set_sensitive(GTK_WIDGET(obj), FALSE);

  /* Shape */
  hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
  gtk_box_pack_start(GTK_BOX(obj), hbox, FALSE, FALSE, 0);

  label = gtk_label_new("");
  gtk_label_set_markup(GTK_LABEL(label), _("Shape: "));
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);

  obj->priv->comboShape = gtk_combo_box_text_new();
  shapes = visu_element_spin_getShapeNames(TRUE);
  for (i=0; shapes[i]; i++)
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(obj->priv->comboShape),
                              (const gchar*)0, shapes[i]);
  gtk_box_pack_end(GTK_BOX(hbox), obj->priv->comboShape, FALSE, FALSE, 0);

  /* Sizes */
  label = gtk_label_new("");
  gtk_label_set_markup(GTK_LABEL(label), _("Size and color properties:"));
  gtk_label_set_xalign(GTK_LABEL(label), 0.);
  gtk_box_pack_start(GTK_BOX(obj), label, FALSE, FALSE, 0);

  /* Arrow sizes. */
  vboxArrowShape = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
  g_object_bind_property_full(obj->priv->comboShape, "active",
                              vboxArrowShape, "visible", G_BINDING_SYNC_CREATE,
                              toArrowVisible, NULL, (gpointer)0, (GDestroyNotify)0);
  gtk_box_pack_start(GTK_BOX(obj), vboxArrowShape, FALSE, FALSE, 0);

  hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
  gtk_box_pack_start(GTK_BOX(vboxArrowShape), hbox, FALSE, FALSE, 2);
  label = gtk_label_new(_("Hat length:"));
  gtk_widget_set_margin_start(label, 10);
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
  obj->priv->hLengthSpin = gtk_spin_button_new_with_range(0, 9, 0.05);
  gtk_box_pack_end(GTK_BOX(hbox), obj->priv->hLengthSpin, FALSE, FALSE, 0);

  hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
  gtk_box_pack_start(GTK_BOX(vboxArrowShape), hbox, FALSE, FALSE, 2);
  label = gtk_label_new(_("Tail length:"));
  gtk_widget_set_margin_start(label, 10);
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
  obj->priv->tLengthSpin = gtk_spin_button_new_with_range(0, 9, 0.05);
  gtk_box_pack_end(GTK_BOX(hbox), obj->priv->tLengthSpin, FALSE, FALSE, 0);

  hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
  gtk_box_pack_start(GTK_BOX(vboxArrowShape), hbox, FALSE, FALSE, 2);
  label = gtk_label_new(_("Hat radius:"));
  gtk_widget_set_margin_start(label, 10);
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
  obj->priv->hRadiusSpin = gtk_spin_button_new_with_range(0, 9, 0.05);
  gtk_box_pack_end(GTK_BOX(hbox), obj->priv->hRadiusSpin, FALSE, FALSE, 0);

  hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
  gtk_box_pack_start(GTK_BOX(vboxArrowShape), hbox, FALSE, FALSE, 2);
  label = gtk_label_new(_("Tail radius:"));
  gtk_widget_set_margin_start(label, 10);
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
  obj->priv->tRadiusSpin = gtk_spin_button_new_with_range(0, 9, 0.05);
  gtk_box_pack_end(GTK_BOX(hbox), obj->priv->tRadiusSpin, FALSE, FALSE, 0);

  hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
  gtk_box_pack_start(GTK_BOX(vboxArrowShape), hbox, FALSE, FALSE, 2);
  label = gtk_label_new(_("Use element color on:"));
  gtk_widget_set_margin_start(label, 10);
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
  obj->priv->tFollowCheck = gtk_check_button_new_with_label(_(" tail"));
  gtk_box_pack_end(GTK_BOX(hbox), obj->priv->tFollowCheck, FALSE, FALSE, 0);
  obj->priv->hFollowCheck = gtk_check_button_new_with_label(_(" hat"));
  gtk_box_pack_end(GTK_BOX(hbox), obj->priv->hFollowCheck, FALSE, FALSE, 0);

  /* Elipsoid sizes. */
  vboxElipsoidShape = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
  g_object_bind_property_full(obj->priv->comboShape, "active",
                              vboxElipsoidShape, "visible", G_BINDING_SYNC_CREATE,
                              toElipVisible, NULL, (gpointer)0, (GDestroyNotify)0);
  gtk_box_pack_start(GTK_BOX(obj), vboxElipsoidShape, FALSE, FALSE, 0);

  hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
  gtk_box_pack_start(GTK_BOX(vboxElipsoidShape), hbox, FALSE, FALSE, 0);
  label = gtk_label_new(_("A axis: "));
  gtk_label_set_xalign(GTK_LABEL(label), 1.);
  gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 0);
  obj->priv->lengthElipsoidSpin = gtk_spin_button_new_with_range(0, 9, 0.05);
  gtk_box_pack_end(GTK_BOX(hbox), obj->priv->lengthElipsoidSpin, FALSE, FALSE, 0);

  hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
  gtk_box_pack_start(GTK_BOX(vboxElipsoidShape), hbox, FALSE, FALSE, 0);
  label = gtk_label_new(_("B axis: "));
  gtk_label_set_xalign(GTK_LABEL(label), 1.);
  gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 0);
  obj->priv->ratioElipsoidSpin = gtk_spin_button_new_with_range(0, 9, 0.05);
  gtk_box_pack_end(GTK_BOX(hbox), obj->priv->ratioElipsoidSpin, FALSE, FALSE, 0);

  obj->priv->elipFollowCheck = gtk_check_button_new_with_label(_("Use element color"));
  gtk_box_pack_start(GTK_BOX(vboxElipsoidShape), obj->priv->elipFollowCheck, FALSE, FALSE, 0);

  /* Atomic options. */
  obj->priv->expandAtomic = gtk_expander_new(_("Atomic rendering options"));
  gtk_box_pack_start(GTK_BOX(obj), obj->priv->expandAtomic, FALSE, FALSE, 0);

  g_object_bind_property(obj->priv->expandAtomic, "sensitive",
                         obj->priv->expandAtomic, "expanded",
                         G_BINDING_SYNC_CREATE);

  label = gtk_label_new(_("<i>Enable the atomic rendering in the method tab.</i>"));
  gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
  gtk_box_pack_start(GTK_BOX(obj), label, FALSE, FALSE, 0);
  g_object_bind_property(obj->priv->expandAtomic, "sensitive", label, "visible",
                         G_BINDING_SYNC_CREATE | G_BINDING_INVERT_BOOLEAN);

  /* g_object_bind_property_full(spin, "hiding-mode", obj->priv->expandAtomic, "sensitive", */
  /*                             G_BINDING_SYNC_CREATE, toAtomic, NULL, */
  /*                             (gpointer)0, (GDestroyNotify)0); */
  /* g_object_bind_property_full(spin, "use-atomic", obj->priv->expandAtomic, "sensitive", */
  /*                             G_BINDING_SYNC_CREATE, toAtomic, NULL, */
  /*                             (gpointer)0, (GDestroyNotify)0); */
}

/**
 * visu_ui_spin_new:
 * @renderer: a #VisuNodeArrayRenderer object.
 *
 * Creates a new #VisuUiSpin to allow to setup spin rendering characteristics.
 *
 * Since: 3.8
 *
 * Returns: a pointer to the newly created widget.
 */
GtkWidget* visu_ui_spin_new(VisuNodeArrayRenderer *renderer)
{
  VisuUiSpin *spin;

  g_debug("Ui Spin: new object.");

  spin = VISU_UI_SPIN(g_object_new(VISU_TYPE_UI_SPIN, NULL));
  spin->priv->renderer = renderer;
  spin->priv->atomic = VISU_UI_ATOMIC(visu_ui_atomic_new(renderer));
  gtk_container_add(GTK_CONTAINER(spin->priv->expandAtomic), GTK_WIDGET(spin->priv->atomic));
  return GTK_WIDGET(spin);
}

static gboolean setForAll(GBinding *bind, const GValue *source_value,
                          GValue *target_value, gpointer data)
{
  VisuUiSpin *spin = VISU_UI_SPIN(data);
  GList *lst;
  
  for (lst = spin->priv->targets; lst; lst = g_list_next(lst))
    if (lst->data != spin->priv->model)
      g_object_set_property(lst->data, g_binding_get_source_property(bind), source_value);
  
  return g_value_transform(source_value, target_value);
}

static void _bind(VisuUiSpin *spin, VisuElementSpin *element)
{
  if (spin->priv->model == element)
    return;

  if (spin->priv->model)
    {
      g_object_unref(spin->priv->bind_shape);
      g_object_unref(spin->priv->bind_tl);
      g_object_unref(spin->priv->bind_tr);
      g_object_unref(spin->priv->bind_hl);
      g_object_unref(spin->priv->bind_hr);
      g_object_unref(spin->priv->bind_tf);
      g_object_unref(spin->priv->bind_hf);
      g_object_unref(spin->priv->bind_a);
      g_object_unref(spin->priv->bind_b);
      g_object_unref(spin->priv->bind_follow);
      g_object_unref(spin->priv->model);
    }
  spin->priv->model = element;
  if (element)
    {
      g_object_ref(element);
      spin->priv->bind_shape =
        g_object_bind_property_full(element, "spin-shape", spin->priv->comboShape, "active",
                                    G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL,
                                    NULL, setForAll, spin, (GDestroyNotify)0);
      spin->priv->bind_tl =
        g_object_bind_property_full(element, "tail-length", spin->priv->tLengthSpin, "value",
                                    G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL,
                                    NULL, setForAll, spin, (GDestroyNotify)0);
      spin->priv->bind_tr =
        g_object_bind_property_full(element, "tail-radius", spin->priv->tRadiusSpin, "value",
                                    G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL,
                                    NULL, setForAll, spin, (GDestroyNotify)0);
      spin->priv->bind_hl =
        g_object_bind_property_full(element, "hat-length", spin->priv->hLengthSpin, "value",
                                    G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL,
                                    NULL, setForAll, spin, (GDestroyNotify)0);
      spin->priv->bind_hr =
        g_object_bind_property_full(element, "hat-radius", spin->priv->hRadiusSpin, "value",
                                    G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL,
                                    NULL, setForAll, spin, (GDestroyNotify)0);
      spin->priv->bind_tf =
        g_object_bind_property_full(element, "tail-spin-colored", spin->priv->tFollowCheck, "active",
                                    G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL,
                                    NULL, setForAll, spin, (GDestroyNotify)0);
      spin->priv->bind_hf =
        g_object_bind_property_full(element, "hat-spin-colored", spin->priv->hFollowCheck, "active",
                                    G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL,
                                    NULL, setForAll, spin, (GDestroyNotify)0);

      spin->priv->bind_a =
        g_object_bind_property_full(element, "a-axis", spin->priv->lengthElipsoidSpin, "value",
                                    G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL,
                                    NULL, setForAll, spin, (GDestroyNotify)0);
      spin->priv->bind_b =
        g_object_bind_property_full(element, "b-axis", spin->priv->ratioElipsoidSpin, "value",
                                    G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL,
                                    NULL, setForAll, spin, (GDestroyNotify)0);
      spin->priv->bind_follow =
        g_object_bind_property_full(element, "spin-colored", spin->priv->elipFollowCheck, "active",
                                    G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL,
                                    NULL, setForAll, spin, (GDestroyNotify)0);
    }
}
/**
 * visu_ui_spin_bind:
 * @spin: a #VisuUiSpin object.
 * @eleList: (element-type VisuElement): a list of #VisuElement.
 *
 * Use the list @eleList to be handled by @elements. Any change in
 * @elements will be applied to the #VisuElementRenderer corresponding
 * to each #VisuElement of @eleList.
 *
 * Since: 3.8
 **/
void visu_ui_spin_bind(VisuUiSpin *spin, GList *eleList)
{
  GList *lst;

  g_return_if_fail(VISU_IS_UI_SPIN(spin));
  g_return_if_fail(spin->priv->renderer);

  if (!eleList)
    _bind(spin, (VisuElementSpin*)0);
  else
    {
      if (!spin->priv->model || !g_list_find(eleList, visu_element_renderer_getElement(VISU_ELEMENT_RENDERER(spin->priv->model))))
        _bind(spin, VISU_ELEMENT_SPIN(visu_node_array_renderer_get(spin->priv->renderer, VISU_ELEMENT(eleList->data))));
    }
  
  if (spin->priv->targets)
    g_list_free(spin->priv->targets);

  spin->priv->targets = (GList*)0;
  for (lst = eleList; lst; lst = g_list_next(lst))
    spin->priv->targets = g_list_prepend(spin->priv->targets, visu_node_array_renderer_get(spin->priv->renderer, VISU_ELEMENT(lst->data)));

  gtk_widget_set_sensitive(GTK_WIDGET(spin), (spin->priv->model != (VisuElementSpin*)0));

  visu_ui_atomic_bind(spin->priv->atomic, eleList);
}
