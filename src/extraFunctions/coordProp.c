/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2016)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2016)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include "coordProp.h"
#include <coreTools/toolMatrix.h>

#include <math.h>

/**
 * SECTION:coordProp
 * @short_description: define a #VisuNodeValues object to access node coordinates.
 *
 * <para>Defines a #VisuNodeValues object to be notified about
 * coordinate changes.</para>
 */


static gboolean _getAt(const VisuNodeValues *vals, const VisuNode *node,
                       GValue *value);
static gboolean _setAt(VisuNodeValues *vals, const VisuNode *node,
                       GValue *value);
static gboolean _parse(VisuNodeValues *vals, VisuNode *node, const gchar *from);
static gchar* _serialize(const VisuNodeValues *vals, const VisuNode *node);


G_DEFINE_TYPE(VisuNodeValuesCoord, visu_node_values_coord, VISU_TYPE_NODE_VALUES_FARRAY)

static void visu_node_values_coord_class_init(VisuNodeValuesCoordClass *klass)
{
  /* Connect the overloading methods. */
  VISU_NODE_VALUES_CLASS(klass)->getAt = _getAt;
  VISU_NODE_VALUES_CLASS(klass)->setAt = _setAt;
  VISU_NODE_VALUES_CLASS(klass)->parse = _parse;
  VISU_NODE_VALUES_CLASS(klass)->serialize = _serialize;
}

static void visu_node_values_coord_init(VisuNodeValuesCoord *self _U_)
{
}

static gboolean _parse(VisuNodeValues *vals, VisuNode *node, const gchar *from)
{
  gfloat fvals[3];
  gchar **datas;
  guint i;
  GValue value = {0, {{0}, {0}}};

  g_return_val_if_fail(from, FALSE);

  datas = g_strsplit_set(from, "(;)", 3);
  for (i = 0; datas[i]; i++)
    if (sscanf(from, "%f", fvals + i) != 1)
      {
        g_strfreev(datas);
        return FALSE;
      }
  g_strfreev(datas);
  g_value_init(&value, G_TYPE_POINTER);
  g_value_set_pointer(&value, fvals);
  return _setAt(vals, node, &value);
}
static gchar* _serialize(const VisuNodeValues *vals _U_, const VisuNode *node)
{
  return g_strdup_printf("(%#.3g ; %#.3g ; %#.3g)",
                         node->xyz[0], node->xyz[1], node->xyz[2]);
}

/**
 * visu_node_values_coord_new:
 * @dataObj: a #VisuData object.
 *
 * Create a #VisuNodeValues object to handle coordinates of nodes.
 *
 * Since: 3.8
 *
 * Returns: (transfer full): a newly created #VisuNodeValuesCoord object.
 **/
VisuNodeValuesCoord* visu_node_values_coord_new(VisuData *dataObj)
{
  VisuNodeValuesCoord *vals;

  vals = VISU_NODE_VALUES_COORD(g_object_new(VISU_TYPE_NODE_VALUES_COORD,
                                             "label", _("Coord. (x, y, z)"),
                                             "internal", TRUE,
                                             "nodes", dataObj,
                                             "type", G_TYPE_FLOAT,
                                             "n-elements", 3,
                                             NULL));
  return vals;
}

static gfloat zeros[3] = {0.f, 0.f, 0.f};
/**
 * visu_node_values_coord_getAt:
 * @vect: a #VisuNodeValuesCoord object.
 * @node: a #VisuNode object.
 *
 * Retrieves the coord hosted on @node.
 *
 * Since: 3.8
 *
 * Returns: (array fixed-size=3) (transfer none): the coordinates of
 * coord for @node.
 **/
const gfloat* visu_node_values_coord_getAt(const VisuNodeValuesCoord *vect,
                                           const VisuNode *node)
{
  g_return_val_if_fail(VISU_IS_NODE_VALUES_COORD(vect), zeros);

  return node->xyz;
}

static gboolean _getAt(const VisuNodeValues *vals _U_, const VisuNode *node,
                       GValue *value)
{
  g_value_set_pointer(value, (gpointer)node->xyz);
  return TRUE;
}
static gboolean _setAt(VisuNodeValues *vect, const VisuNode *node,
                       GValue *value)
{
  VisuNodeArray *arr;
  gfloat *coords;
  gboolean res;
  VisuNode *n;

  arr = visu_node_values_getArray(vect);
  n = visu_node_array_getFromId(arr, node->number);
  g_object_unref(arr);
  g_return_val_if_fail(n, FALSE);

  coords = (gfloat*)g_value_get_pointer(value);
  if (n->xyz[0] == coords[0] &&
      n->xyz[1] == coords[1] &&
      n->xyz[2] == coords[2])
    return FALSE;

  n->xyz[0] = coords[0];
  n->xyz[1] = coords[1];
  n->xyz[2] = coords[2];
      
  /* Chain up to parent. */
  res = VISU_NODE_VALUES_CLASS(visu_node_values_coord_parent_class)->setAt(vect, node, value);

  return res;
}
/**
 * visu_node_values_coord_setAt:
 * @vect: a #VisuNodeValuesCoord object.
 * @node: a #VisuNode object.
 * @xyz: (array fixed-size=3): coord coordinates.
 *
 * Changes the coord hosted at @node for one of coordinates defined
 * by @xyz.
 *
 * Since: 3.8
 *
 * Returns: TRUE if coord for @node is indeed changed.
 **/
gboolean visu_node_values_coord_setAt(VisuNodeValuesCoord *vect,
                                      const VisuNode *node,
                                      const gfloat xyz[3])
{
  GValue value = {0, {{0}, {0}}};

  g_value_init(&value, G_TYPE_POINTER);
  g_value_set_pointer(&value, (gpointer)xyz);
  return visu_node_values_setAt(VISU_NODE_VALUES(vect), node, &value);
}
/**
 * visu_node_values_coord_setAtDbl:
 * @vect: a #VisuNodeValuesCoord object.
 * @node: a #VisuNode object.
 * @dxyz: (array fixed-size=3): coord coordinates.
 *
 * Same as visu_node_values_coord_setAt() but for double values.
 *
 * Since: 3.8
 *
 * Returns: TRUE if coord for @node is indeed changed.
 **/
gboolean visu_node_values_coord_setAtDbl(VisuNodeValuesCoord *vect,
                                          const VisuNode *node,
                                          const double dxyz[3])
{
  gfloat fxyz[3];

  fxyz[0] = dxyz[0];
  fxyz[1] = dxyz[1];
  fxyz[2] = dxyz[2];
  return visu_node_values_coord_setAt(vect, node, fxyz);
}
