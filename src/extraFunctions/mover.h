/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2017)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2017)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at COPYING.
*/
#ifndef MOVER_H
#define MOVER_H

#include <glib.h>
#include <glib-object.h>

#include <visu_tools.h>
#include <visu_nodes.h>

G_BEGIN_DECLS

/**
 * VISU_TYPE_NODE_MOVER:
 *
 * return the type of #VisuNodeMover.
 *
 * Since: 3.8
 */
#define VISU_TYPE_NODE_MOVER	     (visu_node_mover_get_type ())
/**
 * VISU_NODE_MOVER:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuNodeMover type.
 *
 * Since: 3.8
 */
#define VISU_NODE_MOVER(obj)	     (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_NODE_MOVER, VisuNodeMover))
/**
 * VISU_NODE_MOVER_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuNodeMoverClass.
 *
 * Since: 3.8
 */
#define VISU_NODE_MOVER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_NODE_MOVER, VisuNodeMoverClass))
/**
 * VISU_IS_NODE_MOVER:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuNodeMover object.
 *
 * Since: 3.8
 */
#define VISU_IS_NODE_MOVER(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_NODE_MOVER))
/**
 * VISU_IS_NODE_MOVER_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuNodeMoverClass class.
 *
 * Since: 3.8
 */
#define VISU_IS_NODE_MOVER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_NODE_MOVER))
/**
 * VISU_NODE_MOVER_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 *
 * Since: 3.8
 */
#define VISU_NODE_MOVER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_NODE_MOVER, VisuNodeMoverClass))

typedef struct _VisuNodeMover        VisuNodeMover;
typedef struct _VisuNodeMoverPrivate VisuNodeMoverPrivate;
typedef struct _VisuNodeMoverClass   VisuNodeMoverClass;

struct _VisuNodeMover
{
  VisuObject parent;

  VisuNodeMoverPrivate *priv;
};

struct _VisuNodeMoverClass
{
  VisuObjectClass parent;

  gboolean (*validate)(const VisuNodeMover *mover);
  void (*setup)(VisuNodeMover *mover);
  gboolean (*apply)(VisuNodeMover *mover, VisuNodeArray *arr,
                    const GArray *ids, gfloat completion);
  gboolean (*push)(VisuNodeMover *mover);
  void (*undo)(VisuNodeMover *mover, VisuNodeArray *arr, const GArray *ids);
};

/**
 * visu_node_mover_get_type:
 *
 * This method returns the type of #VisuNodeMover, use
 * VISU_TYPE_NODE_MOVER instead.
 *
 * Since: 3.8
 *
 * Returns: the type of #VisuNodeMover.
 */
GType visu_node_mover_get_type(void);

void visu_node_mover_setNodes(VisuNodeMover *mover, GArray *ids);
const GArray* visu_node_mover_getNodes(const VisuNodeMover *mover);

void visu_node_mover_apply(VisuNodeMover *mover);
void visu_node_mover_animate(VisuNodeMover *mover);
void visu_node_mover_push(VisuNodeMover *mover);
void visu_node_mover_undo(VisuNodeMover *mover);

G_END_DECLS

#endif
