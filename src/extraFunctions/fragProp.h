/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2016-2019)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2016-2019)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#ifndef FRAGPROP_H
#define FRAGPROP_H

#include <glib.h>
#include <glib-object.h>

#include "nodeProp.h"

G_BEGIN_DECLS

/**
 * VisuNodeFragment:
 * @label: a label.
 * @id: the fragment id for this label.
 *
 * Defines property for a given fragment.
 *
 * Since: 3.8
 */
typedef struct _VisuNodeFragment VisuNodeFragment;
struct _VisuNodeFragment
{
  gchar *label;
  guint id;
};

#define VISU_TYPE_NODE_FRAGMENT (visu_node_fragment_get_type())
GType   visu_node_fragment_get_type(void);
VisuNodeFragment* visu_node_fragment_new(const gchar *label, guint id);

#define VISU_TYPE_NODE_VALUES_FRAG	      (visu_node_values_frag_get_type ())
#define VISU_NODE_VALUES_FRAG(obj)	      (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_NODE_VALUES_FRAG, VisuNodeValuesFrag))
#define VISU_NODE_VALUES_FRAG_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_NODE_VALUES_FRAG, VisuNodeValuesFragClass))
#define VISU_IS_NODE_VALUES_FRAG(obj)         (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_NODE_VALUES_FRAG))
#define VISU_IS_NODE_VALUES_FRAG_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_NODE_VALUES_FRAG))
#define VISU_NODE_VALUES_FRAG_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_NODE_VALUES_FRAG, VisuNodeValuesFragClass))

/**
 * VisuNodeValuesFragPrivate:
 * 
 * Private data for #VisuNodeValuesFrag objects.
 */
typedef struct _VisuNodeValuesFragPrivate VisuNodeValuesFragPrivate;

/**
 * VisuNodeValuesFrag:
 * 
 * Common name to refer to a #_VisuNodeValuesFrag.
 */
typedef struct _VisuNodeValuesFrag VisuNodeValuesFrag;
struct _VisuNodeValuesFrag
{
  VisuNodeValues parent;

  VisuNodeValuesFragPrivate *priv;
};

/**
 * VisuNodeValuesFragClass:
 * @parent: private.
 * 
 * Common name to refer to a #_VisuNodeValuesFragClass.
 */
typedef struct _VisuNodeValuesFragClass VisuNodeValuesFragClass;
struct _VisuNodeValuesFragClass
{
  VisuNodeValuesClass parent;
};

/**
 * visu_node_values_frag_get_type:
 *
 * This method returns the type of #VisuNodeValuesFrag, use
 * VISU_TYPE_NODE_VALUES_FRAG instead.
 *
 * Since: 3.8
 *
 * Returns: the type of #VisuNodeValuesFrag.
 */
GType visu_node_values_frag_get_type(void);

VisuNodeValuesFrag* visu_node_values_frag_new(VisuNodeArray *arr,
                                              const gchar *label);

const VisuNodeFragment* visu_node_values_frag_getAt(VisuNodeValuesFrag *vect,
                                                    const VisuNode *node);
const VisuNodeFragment* visu_node_values_frag_getAtIter(VisuNodeValuesFrag *vect,
                                                        const VisuNodeValuesIter *iter);

gboolean visu_node_values_frag_setAt(VisuNodeValuesFrag *vect,
                                     const VisuNode *node,
                                     const VisuNodeFragment *frag);

GHashTable* visu_node_values_frag_getLabels(VisuNodeValuesFrag *frag);

GArray* visu_node_values_frag_getNodeIds(const VisuNodeValuesFrag *frag,
                                         const gchar *label);

G_END_DECLS

#endif
