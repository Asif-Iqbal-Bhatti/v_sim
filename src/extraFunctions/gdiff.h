/* -*- mode: C; c-basic-offset: 2 -*- */
/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2017)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2017)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#ifndef GDIFF_H
#define GDIFF_H

#include <glib.h>
#include <glib-object.h>

#include "vectorProp.h"
#include "finder.h"
#include <visu_data.h>

G_BEGIN_DECLS

/**
 * VISU_TYPE_DATA_DIFF:
 *
 * return the type of #VisuDataDiff.
 */
#define VISU_TYPE_DATA_DIFF	     (visu_data_diff_get_type ())
/**
 * VISU_DATA_DIFF:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuDataDiff type.
 */
#define VISU_DATA_DIFF(obj)	        (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_DATA_DIFF, VisuDataDiff))
/**
 * VISU_DATA_DIFF_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuDataDiffClass.
 */
#define VISU_DATA_DIFF_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_DATA_DIFF, VisuDataDiffClass))
/**
 * VISU_IS_DATA_DIFF:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuDataDiff object.
 */
#define VISU_IS_DATA_DIFF(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_DATA_DIFF))
/**
 * VISU_IS_DATA_DIFF_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuDataDiffClass class.
 */
#define VISU_IS_DATA_DIFF_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_DATA_DIFF))
/**
 * VISU_DATA_DIFF_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_DATA_DIFF_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_DATA_DIFF, VisuDataDiffClass))

typedef struct _VisuDataDiffPrivate VisuDataDiffPrivate;
/**
 * VisuDataDiff:
 * 
 * Common name to refer to a #VisuDataDiff.
 */
typedef struct _VisuDataDiff VisuDataDiff;
struct _VisuDataDiff
{
  VisuNodeValuesVector parent;

  VisuDataDiffPrivate *priv;
};

/**
 * VisuDataDiffClass:
 * @parent: private.
 * 
 * Common name to refer to a #VisuDataDiffClass.
 */
typedef struct _VisuDataDiffClass VisuDataDiffClass;
struct _VisuDataDiffClass
{
  VisuNodeValuesVectorClass parent;
};

/**
 * visu_data_diff_get_type:
 *
 * This method returns the type of #VisuDataDiff, use
 * VISU_TYPE_DATA_DIFF instead.
 *
 * Since: 3.8
 *
 * Returns: the type of #VisuDataDiff.
 */
GType visu_data_diff_get_type(void);

/**
 * VISU_DATA_DIFF_DEFAULT_ID:
 *
 * A default label that is used to store a #VisuDataDiff in a #VisuData.
 *
 * Since: 3.8
 */
#define VISU_DATA_DIFF_DEFAULT_ID "geometry_diff"

VisuDataDiff* visu_data_diff_new(const VisuData *dataRef, VisuData *data, gboolean reorder, const gchar *label);
gboolean visu_data_diff_isEmpty(const VisuDataDiff *self);

gchar*   visu_data_diff_export(const VisuDataDiff *self);
void     visu_data_diff_apply(const VisuDataDiff *geodiff, VisuNodeArray *target);
void     visu_data_diff_applyWithFinder(const VisuDataDiff *geodiff,
                                        VisuNodeFinder *finder, gfloat tol);

G_END_DECLS

#endif
