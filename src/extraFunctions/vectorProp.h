/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2015)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2015)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#ifndef VECTORPROP_H
#define VECTORPROP_H

#include <glib.h>
#include <glib-object.h>

#include "floatProp.h"

G_BEGIN_DECLS

/**
 * VISU_TYPE_NODE_VALUES_VECTOR:
 *
 * return the type of #VisuNodeValuesVector.
 */
#define VISU_TYPE_NODE_VALUES_VECTOR	     (visu_node_values_vector_get_type ())
/**
 * VISU_NODE_VALUES_VECTOR:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuNodeValuesVector type.
 */
#define VISU_NODE_VALUES_VECTOR(obj)	        (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_NODE_VALUES_VECTOR, VisuNodeValuesVector))
/**
 * VISU_NODE_VALUES_VECTOR_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuNodeValuesVectorClass.
 */
#define VISU_NODE_VALUES_VECTOR_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_NODE_VALUES_VECTOR, VisuNodeValuesVectorClass))
/**
 * VISU_IS_NODE_VALUES_VECTOR:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuNodeValuesVector object.
 */
#define VISU_IS_NODE_VALUES_VECTOR(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_NODE_VALUES_VECTOR))
/**
 * VISU_IS_NODE_VALUES_VECTOR_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuNodeValuesVectorClass class.
 */
#define VISU_IS_NODE_VALUES_VECTOR_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_NODE_VALUES_VECTOR))
/**
 * VISU_NODE_VALUES_VECTOR_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_NODE_VALUES_VECTOR_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_NODE_VALUES_VECTOR, VisuNodeValuesVectorClass))

/**
 * VisuNodeValuesVector:
 * 
 * Common name to refer to a #_VisuNodeValuesVector.
 */
typedef struct _VisuNodeValuesVector VisuNodeValuesVector;
struct _VisuNodeValuesVector
{
  VisuNodeValuesFarray parent;
};

/**
 * VisuNodeValuesVectorShift:
 * @vect: a #VisuNodeValuesVector object.
 * @node: a #VisuNode pointer:
 * @dxyz: (array fixed-size=3): a shift in cartesian coordinates.
 *
 * Prototype of functions used to define a shift between the centre of
 * @node and the origin of the stored vector for this node.
 */
typedef void (*VisuNodeValuesVectorShift)(const VisuNodeValuesVector *vect,
                                          const VisuNode *node, gfloat dxyz[3]);

/**
 * VisuNodeValuesVectorClass:
 * @parent: private.
 * @shift: a routine to add a shift on each node.
 * 
 * Common name to refer to a #_VisuNodeValuesVectorClass.
 */
typedef struct _VisuNodeValuesVectorClass VisuNodeValuesVectorClass;
struct _VisuNodeValuesVectorClass
{
  VisuNodeValuesFarrayClass parent;

  VisuNodeValuesVectorShift shift;
};

/**
 * visu_node_values_vector_get_type:
 *
 * This method returns the type of #VisuNodeValuesVector, use
 * VISU_TYPE_NODE_VALUES_VECTOR instead.
 *
 * Since: 3.8
 *
 * Returns: the type of #VisuNodeValuesVector.
 */
GType visu_node_values_vector_get_type(void);

VisuNodeValuesVector* visu_node_values_vector_new(VisuNodeArray *arr,
                                                  const gchar *label);

const gfloat* visu_node_values_vector_getAt(const VisuNodeValuesVector *vect,
                                            const VisuNode *node);
const gfloat* visu_node_values_vector_getAtSpherical(const VisuNodeValuesVector *vect,
                                                     const VisuNode *node);
const gfloat* visu_node_values_vector_getAtIterSpherical(const VisuNodeValuesVector *vect,
                                                         const VisuNodeValuesIter *iter);
void visu_node_values_vector_getShift(const VisuNodeValuesVector *vect,
                                      const VisuNode *node, gfloat dxyz[3]);

gboolean visu_node_values_vector_setAt(VisuNodeValuesVector *vect,
                                       const VisuNode *node,
                                       const gfloat dxyz[3]);
gboolean visu_node_values_vector_setAtSpherical(VisuNodeValuesVector *vect,
                                                const VisuNode *node,
                                                const gfloat sph[3]);
gboolean visu_node_values_vector_setAtDbl(VisuNodeValuesVector *vect,
                                          const VisuNode *node,
                                          const double dxyz[3]);
gboolean visu_node_values_vector_set(VisuNodeValuesVector *vect,
                                     const GArray *data);

G_END_DECLS

#endif
