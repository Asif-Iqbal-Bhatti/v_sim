/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2011-2019)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2011-2019)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at COPYING.
*/
#ifndef NODE_VECTORS_H
#define NODE_VECTORS_H

#include <visu_extension.h>
#include <extraFunctions/iface_sourceable.h>
#include <extraFunctions/vectorProp.h>
#include <renderingMethods/iface_nodeArrayRenderer.h>

/**
 * VisuGlExtNodeVectorsColorScheme:
 * @VISU_COLOR_ELEMENT: use the #VisuElement colour for vectors.
 * @VISU_COLOR_BRIGHT: use a bright colour derived from the
 * #VisuElement colour for the vectors.
 * @VISU_COLOR_ORIENTATION: use the orientation to colour the vectors.
 *
 * Different colour schemes used to colourize the vectors.
 *
 * Since: 3.8
 */
typedef enum
  {
    VISU_COLOR_ELEMENT,
    VISU_COLOR_BRIGHT,
    VISU_COLOR_ORIENTATION
  } VisuGlExtNodeVectorsColorScheme;

/**
 * VisuGlExtNodeVectorsCentering:
 * @VISU_ARROW_ORIGIN_CENTERED: the arrows are positioned with respect to the
 * point between tail and hat.
 * @VISU_ARROW_BOTTOM_CENTERED: the arrows are positioned with respect to the bottom
 * of the arrow ;
 * @VISU_ARROW_TAIL_CENTERED: the arrows are positioned with respect to the
 * center of the tail part ;
 * @VISU_ARROW_CENTERED: the arrows are centered.
 *
 * The way to draw arrows.
 */
typedef enum
  {
    VISU_ARROW_ORIGIN_CENTERED,
    VISU_ARROW_BOTTOM_CENTERED,
    VISU_ARROW_TAIL_CENTERED,
    VISU_ARROW_CENTERED
  } VisuGlExtNodeVectorsCentering;

#define VISU_TYPE_GL_EXT_NODE_VECTORS	     (visu_gl_ext_node_vectors_get_type ())
#define VISU_GL_EXT_NODE_VECTORS(obj)	     (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_GL_EXT_NODE_VECTORS, VisuGlExtNodeVectors))
#define VISU_GL_EXT_NODE_VECTORS_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_GL_EXT_NODE_VECTORS, VisuGlExtNodeVectorsClass))
#define VISU_IS_GL_EXT_NODE_VECTORS(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_GL_EXT_NODE_VECTORS))
#define VISU_IS_GL_EXT_NODE_VECTORS_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_GL_EXT_NODE_VECTORS))
#define VISU_GL_EXT_NODE_VECTORS_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_GL_EXT_NODE_VECTORS, VisuGlExtNodeVectorsClass))

typedef struct _VisuGlExtNodeVectors        VisuGlExtNodeVectors;
typedef struct _VisuGlExtNodeVectorsPrivate VisuGlExtNodeVectorsPrivate;
typedef struct _VisuGlExtNodeVectorsClass   VisuGlExtNodeVectorsClass;

GType visu_gl_ext_node_vectors_get_type(void);

struct _VisuGlExtNodeVectors
{
  VisuGlExt parent;

  VisuGlExtNodeVectorsPrivate *priv;
};

struct _VisuGlExtNodeVectorsClass
{
  VisuGlExtClass parent;
};

VisuGlExtNodeVectors* visu_gl_ext_node_vectors_new(const gchar *name);

gboolean visu_gl_ext_node_vectors_setNodeRenderer(VisuGlExtNodeVectors *vect,
                                                  VisuNodeArrayRenderer *renderer);

gboolean visu_gl_ext_node_vectors_setRenderedSize(VisuGlExtNodeVectors *vect, gfloat scale);
gboolean visu_gl_ext_node_vectors_setNormalisation(VisuGlExtNodeVectors *vect, float norm);
gboolean visu_gl_ext_node_vectors_setTranslation(VisuGlExtNodeVectors *vect, float trans);
gboolean visu_gl_ext_node_vectors_setColor(VisuGlExtNodeVectors *vect,
                                           VisuGlExtNodeVectorsColorScheme scheme);
gboolean visu_gl_ext_node_vectors_setCentering(VisuGlExtNodeVectors *vect,
                                               VisuGlExtNodeVectorsCentering centering);
gboolean visu_gl_ext_node_vectors_setAddLength(VisuGlExtNodeVectors *vect, gfloat addLength);
gboolean visu_gl_ext_node_vectors_setVectorThreshold(VisuGlExtNodeVectors *vect, float val);
gboolean visu_gl_ext_node_vectors_setLabelThreshold(VisuGlExtNodeVectors *vect, float val);
gboolean visu_gl_ext_node_vectors_setArrow(VisuGlExtNodeVectors *vect, float tailLength,
                                           float tailRadius, guint tailN,
                                           float headLength, float headRadius, guint headN);

float visu_gl_ext_node_vectors_getNormalisation(VisuGlExtNodeVectors *vect);

#endif
