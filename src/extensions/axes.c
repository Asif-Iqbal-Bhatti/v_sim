/* -*- mode: C; c-basic-offset: 2 -*- */
/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2020)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2020)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "axes.h"

#include <epoxy/gl.h>

#include <math.h>

#include <visu_configFile.h>
#include <coreTools/toolColor.h>
#include <coreTools/toolWriter.h>

/**
 * SECTION:axes
 * @short_description: Defines methods to draw axes.
 *
 * <para>The axes are the X, Y and Z lines drawn on the bottom right of the
 * screen defining a given orthogonal basis set in which the box is
 * projected.</para>
 * <para>The axis may be different, depending on the rendering method
 * currently used. For instance, when the spin is used, a projection
 * of the colour scheme is added to the simple lines of the basis
 * set. Besides that, axes are defined by their width (see
 * visu_gl_ext_lined_setWidth()) and their colour (see
 * visu_gl_ext_lined_setRGBA()).</para>
 */

/* Parameters & resources*/
/* This is a boolean to control is the axes is render or not. */
#define FLAG_RESOURCE_AXES_USED   "axes_are_on"
#define DESC_RESOURCE_AXES_USED   "Control if the axes are drawn ; boolean (0 or 1)"
static gboolean RESOURCE_AXES_USED_DEFAULT = FALSE;
/* A resource to control the color used to render the lines of the axes. */
#define FLAG_RESOURCE_AXES_COLOR   "axes_color"
#define DESC_RESOURCE_AXES_COLOR   "Define the color of the axes ; three floating point values (0. <= v <= 1.)"
static float rgbDefault[4] = {1.0, 0.5, 0.1, 1.};
/* A resource to control the width to render the lines of the axes. */
#define FLAG_RESOURCE_AXES_LINE   "axes_line_width"
#define DESC_RESOURCE_AXES_LINE   "Define the width of the lines of the axes ; one floating point values (1. <= v <= 10.)"
static float LINE_WIDTH_DEFAULT = 1.f;
/* A resource to control the stipple to render the lines of the axes. */
#define FLAG_RESOURCE_AXES_STIPPLE   "axes_line_stipple"
#define DESC_RESOURCE_AXES_STIPPLE   "Dot scheme detail for the lines of the axes ; 0 < integer < 2^16"
static guint16 LINE_STIPPLE_DEFAULT = 65535;
/* A resource to control the position to render the axes. */
#define FLAG_RESOURCE_AXES_POSITION   "axes_position"
#define DESC_RESOURCE_AXES_POSITION   "Position of the representation of the axes ; two floating point values (0. <= v <= 1.)"
static float POSITION_DEFAULT[2] = {1.f, 1.f};

#define FLAG_RESOURCE_AXES_LABEL_X    "axes_label_x"
#define FLAG_RESOURCE_AXES_LABEL_Y    "axes_label_y"
#define FLAG_RESOURCE_AXES_LABEL_Z    "axes_label_z"
#define DESC_RESOURCE_AXES_LABEL      "Label to be drawn beside each axis ; string"
static gchar *LABEL_DEFAULT[3] = {NULL, NULL, NULL};

#define FLAG_RESOURCE_AXES_FACTOR   "axes_size"
#define DESC_RESOURCE_AXES_FACTOR   "Portion of the screen used to draw the axis ; one floating point value (0. <= v <= 1.)"
static float SIZE_DEFAULT = .16f;

/* Export function that is called by visu_module to write the
   values of resources to a file. */
static void exportResources(GString *data, VisuData *dataObj);

enum { nVertices = 6, nEdges = 16 };
struct _VisuGlExtAxesPrivate
{
  gboolean dispose_has_run;

  /* Basis definition. */
  GLfloat matrix[nVertices][3];
  VisuBox *box;
  gulong box_signal;

  /* Rendenring parameters. */
  float xpos, ypos;
  float lgFact;
  float rgb[4];
  float lineWidth;
  guint16 lineStipple;
  gchar *lbl[3];
  gboolean displayOrientation;
  float orientation[3];

  /* Signals for the current view. */
  VisuGlView *view;
  gulong widthHeight_signal, persp_signal, refLength_signal, color_signal;
};
enum
  {
   COLOR,
   VERTEX,
   N_STORE
  };
static GLfloat vCone[nEdges / 2][2 * (nEdges + 1)][N_STORE][4];
enum
  {
   AXES_ARRAY,
   CONE_ARRAY,
   N_BUFFERS
  };
enum
  {
   AXES_SHADER,
   CONE_SHADER,
   N_SHADERS
  };
enum
  {
   LBL_X,
   LBL_Y,
   LBL_Z,
   LBL_FRONT,
   LBL_BACK,
   N_TEXTURES
  };
enum
  {
    PROP_0,
    XPOS_PROP,
    YPOS_PROP,
    SIZE_PROP,
    COLOR_PROP,
    WIDTH_PROP,
    STIPPLE_PROP,
    VIEW_PROP,
    BOX_PROP,
    LBLX_PROP,
    LBLY_PROP,
    LBLZ_PROP,
    USE_ORIENTATION_PROP,
    CONE_THETA_PROP,
    CONE_PHI_PROP,
    CONE_OMEGA_PROP,
    N_PROP
  };
static GParamSpec *properties[N_PROP];

static VisuGlExtAxes* defaultAxes;

static void visu_gl_ext_lined_interface_init(VisuGlExtLinedInterface *iface);
static void visu_gl_ext_axes_finalize(GObject* obj);
static void visu_gl_ext_axes_dispose(GObject* obj);
static GObject* visu_gl_ext_axes_constructor(GType gtype, guint nprops, GObjectConstructParam *props);
static void visu_gl_ext_axes_get_property(GObject* obj, guint property_id,
                                          GValue *value, GParamSpec *pspec);
static void visu_gl_ext_axes_set_property(GObject* obj, guint property_id,
                                          const GValue *value, GParamSpec *pspec);
static void visu_gl_ext_axes_rebuild(VisuGlExt *ext);
static void visu_gl_ext_axes_render(const VisuGlExt *ext);
static void visu_gl_ext_axes_renderText(const VisuGlExt *ext);
static void visu_gl_ext_axes_draw(VisuGlExt *ext);
static gboolean visu_gl_ext_axes_setGlView(VisuGlExt *axes, VisuGlView *view);
static void _setBox(VisuGlExtAxes *axes, VisuBox *box);

static gboolean _setRGB(VisuGlExtLined *axes, float rgb[4], int mask);
static gboolean _setLineWidth(VisuGlExtLined *axes, float width);
static gboolean _setLineStipple(VisuGlExtLined *axes, guint16 stipple);
static float*   _getRGB(const VisuGlExtLined *axes);
static float    _getLineWidth(const VisuGlExtLined *axes);
static guint16  _getLineStipple(const VisuGlExtLined *axes);
static void     _buildVCone(void);

/* Local callbacks */
static void onAxesParametersChange(VisuGlExtAxes *axes);
static void onBoxChange(VisuBox *box, float extens, gpointer data);
static void onEntryUsed(VisuGlExtAxes *axes, VisuConfigFileEntry *entry, VisuConfigFile *obj);
static void onEntryColor(VisuGlExtAxes *axes, VisuConfigFileEntry *entry, VisuConfigFile *obj);
static void onEntryWidth(VisuGlExtAxes *axes, VisuConfigFileEntry *entry, VisuConfigFile *obj);
static void onEntryStipple(VisuGlExtAxes *axes, VisuConfigFileEntry *entry, VisuConfigFile *obj);
static void onEntryPosition(VisuGlExtAxes *axes, VisuConfigFileEntry *entry, VisuConfigFile *obj);
static void onEntryLabel(VisuGlExtAxes *axes, VisuConfigFileEntry *entry, VisuConfigFile *obj);
static void onEntryFactor(VisuGlExtAxes *axes, VisuConfigFileEntry *entry, VisuConfigFile *obj);

G_DEFINE_TYPE_WITH_CODE(VisuGlExtAxes, visu_gl_ext_axes, VISU_TYPE_GL_EXT,
                        G_ADD_PRIVATE(VisuGlExtAxes)
                        G_IMPLEMENT_INTERFACE(VISU_TYPE_GL_EXT_LINED,
                                              visu_gl_ext_lined_interface_init))

static void visu_gl_ext_lined_interface_init(VisuGlExtLinedInterface *iface)
{
  iface->get_width   = _getLineWidth;
  iface->set_width   = _setLineWidth;
  iface->get_stipple = _getLineStipple;
  iface->set_stipple = _setLineStipple;
  iface->get_rgba    = _getRGB;
  iface->set_rgba    = _setRGB;
}
static void visu_gl_ext_axes_class_init(VisuGlExtAxesClass *klass)
{
  float rgColor[2] = {0.f, 1.f};
  float rgWidth[2] = {0.f, 10.f};
  float rgFactor[2] = {0.f, 1.f};
  VisuConfigFileEntry *resourceEntry;

  g_debug("Extension Axes: creating the class of the object.");
  /* g_debug("                - adding new signals ;"); */
  LABEL_DEFAULT[TOOL_XYZ_X] = g_strdup("x");
  LABEL_DEFAULT[TOOL_XYZ_Y] = g_strdup("y");
  LABEL_DEFAULT[TOOL_XYZ_Z] = g_strdup("z");

  g_debug("                - adding new resources ;");
  resourceEntry = visu_config_file_addBooleanEntry(VISU_CONFIG_FILE_RESOURCE,
                                                   FLAG_RESOURCE_AXES_USED,
                                                   DESC_RESOURCE_AXES_USED,
                                                   &RESOURCE_AXES_USED_DEFAULT, FALSE);
  resourceEntry = visu_config_file_addFloatArrayEntry(VISU_CONFIG_FILE_RESOURCE,
                                                      FLAG_RESOURCE_AXES_COLOR,
                                                      DESC_RESOURCE_AXES_COLOR,
                                                      3, rgbDefault, rgColor, FALSE);
  resourceEntry = visu_config_file_addFloatArrayEntry(VISU_CONFIG_FILE_RESOURCE,
                                                      FLAG_RESOURCE_AXES_LINE,
                                                      DESC_RESOURCE_AXES_LINE,
                                                      1, &LINE_WIDTH_DEFAULT, rgWidth, FALSE);
  resourceEntry = visu_config_file_addStippleArrayEntry(VISU_CONFIG_FILE_RESOURCE,
                                                        FLAG_RESOURCE_AXES_STIPPLE,
                                                        DESC_RESOURCE_AXES_STIPPLE,
                                                        1, &LINE_STIPPLE_DEFAULT);
  visu_config_file_entry_setVersion(resourceEntry, 3.4f);
  resourceEntry = visu_config_file_addFloatArrayEntry(VISU_CONFIG_FILE_RESOURCE,
                                                      FLAG_RESOURCE_AXES_POSITION,
                                                      DESC_RESOURCE_AXES_POSITION,
                                                      2, POSITION_DEFAULT, rgColor, FALSE);
  visu_config_file_entry_setVersion(resourceEntry, 3.7f);
  resourceEntry = visu_config_file_addStringEntry(VISU_CONFIG_FILE_RESOURCE,
                                                  FLAG_RESOURCE_AXES_LABEL_X,
                                                  DESC_RESOURCE_AXES_LABEL,
                                                  LABEL_DEFAULT + TOOL_XYZ_X);
  visu_config_file_entry_setVersion(resourceEntry, 3.8f);
  resourceEntry = visu_config_file_addStringEntry(VISU_CONFIG_FILE_RESOURCE,
                                                  FLAG_RESOURCE_AXES_LABEL_Y,
                                                  DESC_RESOURCE_AXES_LABEL,
                                                  LABEL_DEFAULT + TOOL_XYZ_Y);
  visu_config_file_entry_setVersion(resourceEntry, 3.8f);
  resourceEntry = visu_config_file_addStringEntry(VISU_CONFIG_FILE_RESOURCE,
                                                  FLAG_RESOURCE_AXES_LABEL_Z,
                                                  DESC_RESOURCE_AXES_LABEL,
                                                  LABEL_DEFAULT + TOOL_XYZ_Z);
  visu_config_file_entry_setVersion(resourceEntry, 3.8f);
  resourceEntry = visu_config_file_addFloatArrayEntry(VISU_CONFIG_FILE_RESOURCE,
                                                      FLAG_RESOURCE_AXES_FACTOR,
                                                      DESC_RESOURCE_AXES_FACTOR,
                                                      1, &SIZE_DEFAULT, rgFactor, FALSE);
  visu_config_file_entry_setVersion(resourceEntry, 3.8f);
  visu_config_file_addExportFunction(VISU_CONFIG_FILE_RESOURCE, exportResources);

  defaultAxes = (VisuGlExtAxes*)0;

  /* Connect the overloading methods. */
  G_OBJECT_CLASS(klass)->dispose  = visu_gl_ext_axes_dispose;
  G_OBJECT_CLASS(klass)->finalize = visu_gl_ext_axes_finalize;
  G_OBJECT_CLASS(klass)->constructor = visu_gl_ext_axes_constructor;
  G_OBJECT_CLASS(klass)->set_property = visu_gl_ext_axes_set_property;
  G_OBJECT_CLASS(klass)->get_property = visu_gl_ext_axes_get_property;
  VISU_GL_EXT_CLASS(klass)->rebuild = visu_gl_ext_axes_rebuild;
  VISU_GL_EXT_CLASS(klass)->render = visu_gl_ext_axes_render;
  VISU_GL_EXT_CLASS(klass)->renderText = visu_gl_ext_axes_renderText;
  VISU_GL_EXT_CLASS(klass)->draw = visu_gl_ext_axes_draw;
  VISU_GL_EXT_CLASS(klass)->setGlView = visu_gl_ext_axes_setGlView;

  /**
   * VisuGlExtAxes::x-pos:
   *
   * Store the position along x of the axes.
   *
   * Since: 3.8
   */
  properties[XPOS_PROP] = g_param_spec_float("x-pos", "x position",
                                             "position along x axis",
                                             0., 1., POSITION_DEFAULT[0],
                                             G_PARAM_READWRITE);
  g_object_class_install_property(G_OBJECT_CLASS(klass), XPOS_PROP,
				  properties[XPOS_PROP]);
  /**
   * VisuGlExtAxes::y-pos:
   *
   * Store the position along y of the axes.
   *
   * Since: 3.8
   */
  properties[YPOS_PROP] = g_param_spec_float("y-pos", "y position",
                                             "position along y axis",
                                             0., 1., POSITION_DEFAULT[1],
                                             G_PARAM_READWRITE);
  g_object_class_install_property(G_OBJECT_CLASS(klass), YPOS_PROP,
				  properties[YPOS_PROP]);
  /**
   * VisuGlExtAxes::size:
   *
   * Store the portion of screen the axis occupy.
   *
   * Since: 3.8
   */
  properties[SIZE_PROP] = g_param_spec_float("size", "Size",
                                               "portion of the screen for the axis",
                                               0., 1., .16f, G_PARAM_READWRITE);
  g_object_class_install_property(G_OBJECT_CLASS(klass), SIZE_PROP,
				  properties[SIZE_PROP]);
  g_object_class_override_property(G_OBJECT_CLASS(klass), COLOR_PROP, "color");
  g_object_class_override_property(G_OBJECT_CLASS(klass), WIDTH_PROP, "width");
  g_object_class_override_property(G_OBJECT_CLASS(klass), STIPPLE_PROP, "stipple");
  /**
   * VisuGlExtAxes::view:
   *
   * Store the view where the axes are rendered.
   *
   * Since: 3.8
   */
  properties[VIEW_PROP] = g_param_spec_object("view", "OpenGl View",
                                              "rendering view for the axes",
                                              VISU_TYPE_GL_VIEW, G_PARAM_READWRITE);
  g_object_class_install_property(G_OBJECT_CLASS(klass), VIEW_PROP,
				  properties[VIEW_PROP]);
  /**
   * VisuGlExtAxes::basis:
   *
   * Store the #VisuBoxed object that defines the axes. If %NULL, a
   * cartesian basis-set is assumed.
   *
   * Since: 3.8
   */
  properties[BOX_PROP] = g_param_spec_object("basis", "basis-set",
                                             "provides the basis-set to draw the axes",
                                             VISU_TYPE_BOX, G_PARAM_READWRITE);
  g_object_class_install_property(G_OBJECT_CLASS(klass), BOX_PROP,
				  properties[BOX_PROP]);
  /**
   * VisuGlExtAxes::x-label:
   *
   * Store the label for x axis.
   *
   * Since: 3.8
   */
  properties[LBLX_PROP] = g_param_spec_string("x-label", "X label", "label for the x axis",
                                             "x", G_PARAM_READWRITE);
  g_object_class_install_property(G_OBJECT_CLASS(klass), LBLX_PROP,
				  properties[LBLX_PROP]);
  /**
   * VisuGlExtAxes::y-label:
   *
   * Store the label for y axis.
   *
   * Since: 3.8
   */
  properties[LBLY_PROP] = g_param_spec_string("y-label", "Y label", "label for the y axis",
                                             "y", G_PARAM_READWRITE);
  g_object_class_install_property(G_OBJECT_CLASS(klass), LBLY_PROP,
				  properties[LBLY_PROP]);
  /**
   * VisuGlExtAxes::z-label:
   *
   * Store the label for z axis.
   *
   * Since: 3.8
   */
  properties[LBLZ_PROP] = g_param_spec_string("z-label", "Z label", "label for the z axis",
                                             "z", G_PARAM_READWRITE);
  g_object_class_install_property(G_OBJECT_CLASS(klass), LBLZ_PROP,
				  properties[LBLZ_PROP]);
  /**
   * VisuGlExtAxes::display-orienation:
   *
   * If TRUE, it draws a coloured cone to display orientation information.
   *
   * Since: 3.8
   */
  properties[USE_ORIENTATION_PROP] = g_param_spec_boolean("display-orientation",
                                                          "Display orientation",
                                                          "display orientation information",
                                                          FALSE, G_PARAM_READWRITE);
  g_object_class_install_property(G_OBJECT_CLASS(klass), USE_ORIENTATION_PROP,
				  properties[USE_ORIENTATION_PROP]);
  /**
   * VisuGlExtAxes::orientation-theta:
   *
   * Store the theta angle used to defined the coloured cone position
   * when axis are used to display a coloured orientation.
   *
   * Since: 3.8
   */
  properties[CONE_THETA_PROP] = g_param_spec_float("orientation-theta",
                                                   "Theta angle in degrees",
                                                   "theta defining top",
                                                   0., 180., 0.f, G_PARAM_READWRITE);
  g_object_class_install_property(G_OBJECT_CLASS(klass), CONE_THETA_PROP,
				  properties[CONE_THETA_PROP]);
  /**
   * VisuGlExtAxes::orientation-phi:
   *
   * Store the phi angle used to defined the coloured cone position
   * when axis are used to display a coloured orientation.
   *
   * Since: 3.8
   */
  properties[CONE_PHI_PROP] = g_param_spec_float("orientation-phi",
                                                   "Phi angle in degrees",
                                                   "phi defining top",
                                                   0., 360., 0.f, G_PARAM_READWRITE);
  g_object_class_install_property(G_OBJECT_CLASS(klass), CONE_PHI_PROP,
				  properties[CONE_PHI_PROP]);
  /**
   * VisuGlExtAxes::orientation-omega:
   *
   * Store the omega angle used to defined the coloured cone position
   * when axis are used to display a coloured orientation.
   *
   * Since: 3.8
   */
  properties[CONE_OMEGA_PROP] = g_param_spec_float("orientation-omega",
                                                   "Omega angle in degrees",
                                                   "omega defining top",
                                                   0., 360., 0.f, G_PARAM_READWRITE);
  g_object_class_install_property(G_OBJECT_CLASS(klass), CONE_OMEGA_PROP,
				  properties[CONE_OMEGA_PROP]);

  _buildVCone();
}

static void visu_gl_ext_axes_init(VisuGlExtAxes *obj)
{
  g_debug("Extension Axes: initializing a new object (%p).",
	      (gpointer)obj);
  
  obj->priv = visu_gl_ext_axes_get_instance_private(obj);
  obj->priv->dispose_has_run = FALSE;

  /* Private data. */
  obj->priv->rgb[0]      = rgbDefault[0];
  obj->priv->rgb[1]      = rgbDefault[1];
  obj->priv->rgb[2]      = rgbDefault[2];
  obj->priv->rgb[3]      = 1.f;
  obj->priv->lgFact      = SIZE_DEFAULT;
  obj->priv->lineWidth   = LINE_WIDTH_DEFAULT;
  obj->priv->lineStipple = LINE_STIPPLE_DEFAULT;
  obj->priv->xpos        = POSITION_DEFAULT[0];
  obj->priv->ypos        = POSITION_DEFAULT[1];
  obj->priv->view               = (VisuGlView*)0;
  obj->priv->widthHeight_signal = 0;
  obj->priv->persp_signal     = 0;
  obj->priv->refLength_signal   = 0;
  obj->priv->color_signal       = 0;
  obj->priv->matrix[0][0] = 0.f;
  obj->priv->matrix[0][1] = 0.f;
  obj->priv->matrix[0][2] = 0.f;
  obj->priv->matrix[1][0] = 1.f;
  obj->priv->matrix[1][1] = 0.f;
  obj->priv->matrix[1][2] = 0.f;
  obj->priv->matrix[2][0] = 0.f;
  obj->priv->matrix[2][1] = 0.f;
  obj->priv->matrix[2][2] = 0.f;
  obj->priv->matrix[3][0] = 0.f;
  obj->priv->matrix[3][1] = 1.f;
  obj->priv->matrix[3][2] = 0.f;
  obj->priv->matrix[4][0] = 0.f;
  obj->priv->matrix[4][1] = 0.f;
  obj->priv->matrix[4][2] = 0.f;
  obj->priv->matrix[5][0] = 0.f;
  obj->priv->matrix[5][1] = 0.f;
  obj->priv->matrix[5][2] = 1.f;
  obj->priv->box          = (VisuBox*)0;
  obj->priv->box_signal   = 0;
  obj->priv->lbl[0]       = g_strdup(LABEL_DEFAULT[TOOL_XYZ_X]);
  obj->priv->lbl[1]       = g_strdup(LABEL_DEFAULT[TOOL_XYZ_Y]);
  obj->priv->lbl[2]       = g_strdup(LABEL_DEFAULT[TOOL_XYZ_Z]);
  obj->priv->displayOrientation = FALSE;
  obj->priv->orientation[0] = 0.f;
  obj->priv->orientation[1] = 0.f;
  obj->priv->orientation[2] = 0.f;

  g_signal_connect_object(VISU_CONFIG_FILE_RESOURCE, "parsed::" FLAG_RESOURCE_AXES_USED,
                          G_CALLBACK(onEntryUsed), (gpointer)obj, G_CONNECT_SWAPPED);
  g_signal_connect_object(VISU_CONFIG_FILE_RESOURCE, "parsed::" FLAG_RESOURCE_AXES_COLOR,
                          G_CALLBACK(onEntryColor), (gpointer)obj, G_CONNECT_SWAPPED);
  g_signal_connect_object(VISU_CONFIG_FILE_RESOURCE, "parsed::" FLAG_RESOURCE_AXES_LINE,
                          G_CALLBACK(onEntryWidth), (gpointer)obj, G_CONNECT_SWAPPED);
  g_signal_connect_object(VISU_CONFIG_FILE_RESOURCE, "parsed::" FLAG_RESOURCE_AXES_STIPPLE,
                          G_CALLBACK(onEntryStipple), (gpointer)obj, G_CONNECT_SWAPPED);
  g_signal_connect_object(VISU_CONFIG_FILE_RESOURCE, "parsed::" FLAG_RESOURCE_AXES_POSITION,
                          G_CALLBACK(onEntryPosition), (gpointer)obj, G_CONNECT_SWAPPED);
  g_signal_connect_object(VISU_CONFIG_FILE_RESOURCE, "parsed::" FLAG_RESOURCE_AXES_LABEL_X,
                          G_CALLBACK(onEntryLabel), (gpointer)obj, G_CONNECT_SWAPPED);
  g_signal_connect_object(VISU_CONFIG_FILE_RESOURCE, "parsed::" FLAG_RESOURCE_AXES_LABEL_Y,
                          G_CALLBACK(onEntryLabel), (gpointer)obj, G_CONNECT_SWAPPED);
  g_signal_connect_object(VISU_CONFIG_FILE_RESOURCE, "parsed::" FLAG_RESOURCE_AXES_LABEL_Z,
                          G_CALLBACK(onEntryLabel), (gpointer)obj, G_CONNECT_SWAPPED);

  g_signal_connect_object(VISU_CONFIG_FILE_RESOURCE, "parsed::" FLAG_RESOURCE_AXES_FACTOR,
                          G_CALLBACK(onEntryFactor), (gpointer)obj, G_CONNECT_SWAPPED);

  if (!defaultAxes)
    defaultAxes = obj;
}

/* This method can be called several times.
   It should unref all of its reference to
   GObjects. */
static void visu_gl_ext_axes_dispose(GObject* obj)
{
  VisuGlExtAxes *axes;

  g_debug("Extension Axes: dispose object %p.", (gpointer)obj);

  axes = VISU_GL_EXT_AXES(obj);
  if (axes->priv->dispose_has_run)
    return;
  axes->priv->dispose_has_run = TRUE;

  /* Disconnect signals. */
  visu_gl_ext_axes_setGlView(VISU_GL_EXT(axes), (VisuGlView*)0);
  _setBox(axes, (VisuBox*)0);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_gl_ext_axes_parent_class)->dispose(obj);
}
/* This method is called once only. */
static void visu_gl_ext_axes_finalize(GObject* obj)
{
  VisuGlExtAxes *axes;

  g_return_if_fail(obj);

  g_debug("Extension Axes: finalize object %p.", (gpointer)obj);

  axes = VISU_GL_EXT_AXES(obj);

  /* Free privs elements. */
  if (axes->priv)
    {
      g_debug("Extension Axes: free private axes.");
      g_free(axes->priv->lbl[0]);
      g_free(axes->priv->lbl[1]);
      g_free(axes->priv->lbl[2]);
    }
  /* The free is called by g_type_free_instance... */
/*   g_free(axes); */

  /* Chain up to the parent class */
  g_debug("Extension Axes: chain to parent.");
  G_OBJECT_CLASS(visu_gl_ext_axes_parent_class)->finalize(obj);
  g_debug("Extension Axes: freeing ... OK.");
}
static GObject* visu_gl_ext_axes_constructor(GType gtype, guint nprops, GObjectConstructParam *props)
{
  guint i;

  for (i = 0; i < nprops; ++i)
    {
      if (!g_strcmp0(g_param_spec_get_name(props[i].pspec), "nGlProg"))
        g_value_set_uint(props[i].value, N_SHADERS);
      if (!g_strcmp0(g_param_spec_get_name(props[i].pspec), "nGlObj"))
        g_value_set_uint(props[i].value, N_BUFFERS);
      if (!g_strcmp0(g_param_spec_get_name(props[i].pspec), "nGlTex"))
        g_value_set_uint(props[i].value, N_TEXTURES);
      if (!g_strcmp0(g_param_spec_get_name(props[i].pspec), "priority"))
        g_value_set_uint(props[i].value, VISU_GL_EXT_PRIORITY_LAST);
    }

  return G_OBJECT_CLASS(visu_gl_ext_axes_parent_class)->constructor(gtype, nprops, props);
}
static void visu_gl_ext_axes_get_property(GObject* obj, guint property_id,
                                          GValue *value, GParamSpec *pspec)
{
  VisuGlExtAxes *self = VISU_GL_EXT_AXES(obj);

  g_debug("Extension Axes: get property '%s' -> ",
	      g_param_spec_get_name(pspec));
  switch (property_id)
    {
    case SIZE_PROP:
      g_value_set_float(value, self->priv->lgFact);
      g_debug("%g.", self->priv->lgFact);
      break;
    case XPOS_PROP:
      g_value_set_float(value, self->priv->xpos);
      g_debug("%g.", self->priv->xpos);
      break;
    case YPOS_PROP:
      g_value_set_float(value, self->priv->ypos);
      g_debug("%g.", self->priv->ypos);
      break;
    case COLOR_PROP:
      g_value_take_boxed(value, tool_color_new(self->priv->rgb));
      g_debug("%gx%gx%g.", self->priv->rgb[0], self->priv->rgb[1], self->priv->rgb[2]);
      break;
    case WIDTH_PROP:
      g_value_set_float(value, self->priv->lineWidth);
      g_debug("%g.", self->priv->lineWidth);
      break;
    case STIPPLE_PROP:
      g_value_set_uint(value, (guint)self->priv->lineStipple);
      g_debug("%d.", (guint)self->priv->lineStipple);
      break;
    case VIEW_PROP:
      g_value_set_object(value, self->priv->view);
      g_debug("%p.", (gpointer)self->priv->view);
      break;
    case BOX_PROP:
      g_value_set_object(value, self->priv->box);
      g_debug("%p.", g_value_get_object(value));
      break;
    case LBLX_PROP:
      g_value_set_static_string(value, self->priv->lbl[0]);
      g_debug("'%s'.", self->priv->lbl[0]);
      break;
    case LBLY_PROP:
      g_value_set_static_string(value, self->priv->lbl[1]);
      g_debug("'%s'.", self->priv->lbl[1]);
      break;
    case LBLZ_PROP:
      g_value_set_static_string(value, self->priv->lbl[2]);
      g_debug("'%s'.", self->priv->lbl[2]);
      break;
    case USE_ORIENTATION_PROP:
      g_value_set_boolean(value, self->priv->displayOrientation);
      g_debug("%d.", self->priv->displayOrientation);
      break;
    case CONE_THETA_PROP:
      g_value_set_float(value, self->priv->orientation[0]);
      g_debug("%g.", self->priv->orientation[0]);
      break;
    case CONE_PHI_PROP:
      g_value_set_float(value, self->priv->orientation[1]);
      g_debug("%g.", self->priv->orientation[1]);
      break;
    case CONE_OMEGA_PROP:
      g_value_set_float(value, self->priv->orientation[2]);
      g_debug("%g.", self->priv->orientation[2]);
      break;
    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
      break;
    }
}
static void visu_gl_ext_axes_set_property(GObject* obj, guint property_id,
                                          const GValue *value, GParamSpec *pspec)
{
  ToolColor *color;
  VisuGlExtAxes *self = VISU_GL_EXT_AXES(obj);
  float orientation[3];

  g_debug("Extension Axes: set property '%s' -> ",
	      g_param_spec_get_name(pspec));
  switch (property_id)
    {
    case SIZE_PROP:
      visu_gl_ext_axes_setLengthFactor(self, g_value_get_float(value));
      g_debug("%g.", self->priv->lgFact);
      break;
    case XPOS_PROP:
      visu_gl_ext_axes_setPosition(self, g_value_get_float(value), self->priv->ypos);
      g_debug("%g.", self->priv->xpos);
      break;
    case YPOS_PROP:
      visu_gl_ext_axes_setPosition(self, self->priv->xpos, g_value_get_float(value));
      g_debug("%g.", self->priv->ypos);
      break;
    case COLOR_PROP:
      color = (ToolColor*)g_value_get_boxed(value);
      _setRGB((VisuGlExtLined*)self, color->rgba, TOOL_COLOR_MASK_RGBA);
      g_debug("%gx%gx%g.", self->priv->rgb[0], self->priv->rgb[1], self->priv->rgb[2]);
      break;
    case WIDTH_PROP:
      _setLineWidth((VisuGlExtLined*)self, g_value_get_float(value));
      g_debug("%g.", self->priv->lineWidth);
      break;
    case STIPPLE_PROP:
      _setLineStipple((VisuGlExtLined*)self, (guint16)g_value_get_uint(value));
      g_debug("%d.", (guint)self->priv->lineStipple);
      break;
    case VIEW_PROP:
      visu_gl_ext_axes_setGlView(VISU_GL_EXT(self),
                                 VISU_GL_VIEW(g_value_get_object(value)));
      g_debug("%p.", (gpointer)self->priv->view);
      break;
    case BOX_PROP:
      g_debug("%p.", (gpointer)g_value_get_object(value));
      visu_gl_ext_axes_setBasisFromBox(self, VISU_BOX(g_value_get_object(value)));
      break;
    case LBLX_PROP:
      visu_gl_ext_axes_setLabel(self, g_value_get_string(value), TOOL_XYZ_X);
      g_debug("'%s'.", self->priv->lbl[0]);
      break;
    case LBLY_PROP:
      visu_gl_ext_axes_setLabel(self, g_value_get_string(value), TOOL_XYZ_Y);
      g_debug("'%s'.", self->priv->lbl[1]);
      break;
    case LBLZ_PROP:
      visu_gl_ext_axes_setLabel(self, g_value_get_string(value), TOOL_XYZ_Z);
      g_debug("'%s'.", self->priv->lbl[2]);
      break;
    case USE_ORIENTATION_PROP:
      visu_gl_ext_axes_useOrientation(self, g_value_get_boolean(value));
      g_debug("%d.", self->priv->displayOrientation);
      break;
    case CONE_THETA_PROP:
      orientation[0] = g_value_get_float(value);
      visu_gl_ext_axes_setOrientationTop(self, orientation, TOOL_GL_CAMERA_THETA);
      g_debug("%g.", self->priv->orientation[0]);
      break;
    case CONE_PHI_PROP:
      orientation[1] = g_value_get_float(value);
      visu_gl_ext_axes_setOrientationTop(self, orientation, TOOL_GL_CAMERA_PHI);
      g_debug("%g.", self->priv->orientation[1]);
      break;
    case CONE_OMEGA_PROP:
      g_debug("%g.", g_value_get_float(value));
      orientation[2] = g_value_get_float(value);
      visu_gl_ext_axes_setOrientationTop(self, orientation, TOOL_GL_CAMERA_OMEGA);
      break;
    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
      break;
    }
}
static void _setBox(VisuGlExtAxes *axes, VisuBox *box)
{
  if (axes->priv->box == box)
    return;
  
  if (axes->priv->box)
    {
      g_signal_handler_disconnect(G_OBJECT(axes->priv->box), axes->priv->box_signal);
      g_object_unref(axes->priv->box);
    }
  if (box)
    {
      g_object_ref(box);
      axes->priv->box_signal =
        g_signal_connect(G_OBJECT(box), "SizeChanged",
                         G_CALLBACK(onBoxChange), (gpointer)axes);
    }
  else
    axes->priv->box_signal = 0;
  axes->priv->box = box;
  g_object_notify_by_pspec(G_OBJECT(axes), properties[BOX_PROP]);
}

/**
 * visu_gl_ext_axes_new:
 * @name: (allow-none): the name to give to the extension (default is #VISU_GL_EXT_AXES_ID).
 *
 * Creates a new #VisuGlExt to draw axes.
 *
 * Since: 3.7
 *
 * Returns: a pointer to the #VisuGlExt it created or
 * NULL otherwise.
 */
VisuGlExtAxes* visu_gl_ext_axes_new(const gchar *name)
{
  char *name_ = VISU_GL_EXT_AXES_ID;
  char *description = _("Draw {x,y,z} axes.");

  g_debug("Extension Axes: new object.");
  
  return g_object_new(VISU_TYPE_GL_EXT_AXES,
                      "name", name ? name : name_, "label", _(name),
                      "description", description, NULL);
}

static gboolean _setRGB(VisuGlExtLined *axes, float rgb[4], int mask)
{
  VisuGlExtAxesPrivate *self;
  g_return_val_if_fail(VISU_IS_GL_EXT_AXES(axes), FALSE);

  self = VISU_GL_EXT_AXES(axes)->priv;
  
  if (mask & TOOL_COLOR_MASK_R)
    self->rgb[0] = rgb[0];
  if (mask & TOOL_COLOR_MASK_G)
    self->rgb[1] = rgb[1];
  if (mask & TOOL_COLOR_MASK_B)
    self->rgb[2] = rgb[2];

  visu_gl_ext_setDirty(VISU_GL_EXT(axes), VISU_GL_RENDER_REQUIRED);
  return TRUE;
}
static gboolean _setLineWidth(VisuGlExtLined *axes, float width)
{
  VisuGlExtAxesPrivate *self;
  g_return_val_if_fail(VISU_IS_GL_EXT_AXES(axes), FALSE);
  self = VISU_GL_EXT_AXES(axes)->priv;
  
  self->lineWidth = width;
  visu_gl_ext_setDirty(VISU_GL_EXT(axes), VISU_GL_RENDER_REQUIRED);
  return TRUE;
}
static gboolean _setLineStipple(VisuGlExtLined *axes, guint16 stipple)
{
  VisuGlExtAxesPrivate *self;
  g_return_val_if_fail(VISU_IS_GL_EXT_AXES(axes), FALSE);
  self = VISU_GL_EXT_AXES(axes)->priv;

  self->lineStipple = stipple;
  visu_gl_ext_setDirty(VISU_GL_EXT(axes), VISU_GL_RENDER_REQUIRED);
  return TRUE;
}
static void _setBasis(VisuGlExtAxes *axes, double matrix[3][3])
{
  double sum2;

  g_debug("Extension Axes: %8g %8g %8g\n"
              "                %8g %8g %8g\n"
	      "                %8g %8g %8g",
	      matrix[0][0], matrix[0][1], matrix[0][2],
	      matrix[1][0], matrix[1][1], matrix[1][2],
	      matrix[2][0], matrix[2][1], matrix[2][2]);
  /* We normalise it and copy it. */
  sum2 = 1. / sqrt(matrix[0][0] * matrix[0][0] +
                   matrix[1][0] * matrix[1][0] +
                   matrix[2][0] * matrix[2][0]);
  axes->priv->matrix[1][0] = matrix[0][0] * sum2;
  axes->priv->matrix[1][1] = matrix[1][0] * sum2;
  axes->priv->matrix[1][2] = matrix[2][0] * sum2;
  sum2 = 1. / sqrt(matrix[0][1] * matrix[0][1] +
                   matrix[1][1] * matrix[1][1] +
                   matrix[2][1] * matrix[2][1]);
  axes->priv->matrix[3][0] = matrix[0][1] * sum2;
  axes->priv->matrix[3][1] = matrix[1][1] * sum2;
  axes->priv->matrix[3][2] = matrix[2][1] * sum2;
  sum2 = 1. / sqrt(matrix[0][2] * matrix[0][2] +
                   matrix[1][2] * matrix[1][2] +
                   matrix[2][2] * matrix[2][2]);
  axes->priv->matrix[5][0] = matrix[0][2] * sum2;
  axes->priv->matrix[5][1] = matrix[1][2] * sum2;
  axes->priv->matrix[5][2] = matrix[2][2] * sum2;

  visu_gl_ext_setDirty(VISU_GL_EXT(axes), VISU_GL_DRAW_REQUIRED);
}
/**
 * visu_gl_ext_axes_setBasis:
 * @axes: the #VisuGlExtAxes object to modify.
 * @matrix: the definition of the three basis axis.
 *
 * The @axes can represent an arbitrary basis-set, provided by
 * @matrix. @matrix[{0,1,2}] represents the {x,y,z} axis vector in a
 * cartesian basis-set. See visu_gl_ext_axes_setBasisFromBox() if the
 * basis-set should follow the one of a given #VisuBox.
 *
 * Since: 3.7
 *
 * Returns: TRUE if the basis is actually changed.
 **/
gboolean visu_gl_ext_axes_setBasis(VisuGlExtAxes *axes, double matrix[3][3])
{
  g_return_val_if_fail(VISU_IS_GL_EXT_AXES(axes), FALSE);

  _setBox(axes, (VisuBox*)0);
  _setBasis(axes, matrix);

  return visu_gl_ext_getActive(VISU_GL_EXT(axes));
}
/**
 * visu_gl_ext_axes_setBasisFromBox:
 * @axes: the #VisuGlExtAxes object to modify.
 * @box: (allow-none): the #VisuBox to use as basis-set.
 *
 * The @axes can follow the basis-set defined by @box. If NULL is
 * passed, then the orthorombic default basis-set is used.
 *
 * Since: 3.7
 *
 * Returns: TRUE if the basis is actually changed.
 **/
gboolean visu_gl_ext_axes_setBasisFromBox(VisuGlExtAxes *axes, VisuBox *box)
{
  double m[3][3];

  g_return_val_if_fail(VISU_IS_GL_EXT_AXES(axes), FALSE);

  if (box)
    visu_box_getCellMatrix(box, m);
  else
    {
      memset(m, '\0', sizeof(double) * 9);
      m[0][0] = 1.;
      m[1][1] = 1.;
      m[2][2] = 1.;
    }
  _setBox(axes, box);
  _setBasis(axes, m);

  return visu_gl_ext_getActive(VISU_GL_EXT(axes));
}
/**
 * visu_gl_ext_axes_setLabel:
 * @axes: a #VisuGlExtAxes object.
 * @lbl: a string.
 * @dir: an axis direction.
 *
 * Set the label @lbl for the given axis @dir.
 *
 * Since: 3.8
 *
 * Returns: TRUE if the label is modified.
 **/
gboolean visu_gl_ext_axes_setLabel(VisuGlExtAxes *axes, const gchar *lbl, ToolXyzDir dir)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_AXES(axes) && lbl, FALSE);
  
  if (!g_strcmp0(axes->priv->lbl[dir], lbl))
    return FALSE;

  g_free(axes->priv->lbl[dir]);
  axes->priv->lbl[dir] = g_strdup(lbl);
  g_object_notify_by_pspec(G_OBJECT(axes), properties[LBLX_PROP + dir]);
  visu_gl_ext_setDirty(VISU_GL_EXT(axes), VISU_GL_DRAW_REQUIRED);
  return TRUE;
}
/**
 * visu_gl_ext_axes_setPosition:
 * @axes: the #VisuGlExtAxes object to modify.
 * @xpos: the reduced x position (1 to the right).
 * @ypos: the reduced y position (1 to the bottom).
 *
 * Change the position of the axes representation.
 *
 * Since: 3.7
 *
 * Returns: TRUE if the position is actually changed.
 **/
gboolean visu_gl_ext_axes_setPosition(VisuGlExtAxes *axes, float xpos, float ypos)
{
  gboolean changed;

  g_return_val_if_fail(VISU_IS_GL_EXT_AXES(axes), FALSE);

  xpos = CLAMP(xpos, 0.f, 1.f);
  ypos = CLAMP(ypos, 0.f, 1.f);
  changed = FALSE;

  g_object_freeze_notify(G_OBJECT(axes));
  if (xpos != axes->priv->xpos)
    {
      axes->priv->xpos = xpos;
      g_object_notify_by_pspec(G_OBJECT(axes), properties[XPOS_PROP]);
      changed = TRUE;
    }
  if (ypos != axes->priv->ypos)
    {
      axes->priv->ypos = ypos;
      g_object_notify_by_pspec(G_OBJECT(axes), properties[YPOS_PROP]);
      changed = TRUE;
    }
  if (changed)
    visu_gl_ext_setDirty(VISU_GL_EXT(axes), VISU_GL_RENDER_REQUIRED);
  g_object_thaw_notify(G_OBJECT(axes));

  return changed;
}
/**
 * visu_gl_ext_axes_useOrientation:
 * @axes: a #VisuGlExtAxes object.
 * @use: a boolean.
 *
 * If TRUE, @axes also draws a coloured cone to display orientation information.
 *
 * Since: 3.8
 *
 * Returns: TRUE if value is actually changed.
 **/
gboolean visu_gl_ext_axes_useOrientation(VisuGlExtAxes *axes, gboolean use)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_AXES(axes), FALSE);
  
  if (axes->priv->displayOrientation == use)
    return FALSE;

  axes->priv->displayOrientation = use;
  g_object_notify_by_pspec(G_OBJECT(axes), properties[USE_ORIENTATION_PROP]);
  visu_gl_ext_setDirty(VISU_GL_EXT(axes), VISU_GL_DRAW_REQUIRED);
  return TRUE;
}
/**
 * visu_gl_ext_axes_setOrientationTop:
 * @axes: a #VisuGlExtAxes object.
 * @top: (array fixed-size=3): a camera orientation.
 * @dir: flags used to specify which angles to set.
 *
 * Define the camera orientation of the top orientation when @axes are
 * used to display orientation information.
 *
 * Since: 3.8
 *
 * Returns: TRUE if value is actually changed.
 **/
gboolean visu_gl_ext_axes_setOrientationTop(VisuGlExtAxes *axes,
                                            const gfloat top[3], int dir)
{
  gboolean changed;

  g_return_val_if_fail(VISU_IS_GL_EXT_AXES(axes), FALSE);

  changed = FALSE;

  g_object_freeze_notify(G_OBJECT(axes));
  if ((dir & TOOL_GL_CAMERA_THETA) &&
      (CLAMP(top[0], 0.f, 180.) != axes->priv->orientation[0]))
    {
      axes->priv->orientation[0] = CLAMP(top[0], 0.f, 180.);
      g_object_notify_by_pspec(G_OBJECT(axes), properties[CONE_THETA_PROP]);
      changed = TRUE;
    }
  if ((dir & TOOL_GL_CAMERA_PHI) &&
      (CLAMP(top[1], 0.f, 360.) != axes->priv->orientation[1]))
    {
      axes->priv->orientation[1] = CLAMP(top[1], 0.f, 360.);
      g_object_notify_by_pspec(G_OBJECT(axes), properties[CONE_PHI_PROP]);
      changed = TRUE;
    }
  if ((dir & TOOL_GL_CAMERA_OMEGA) &&
      (CLAMP(top[2], 0.f, 360.) != axes->priv->orientation[2]))
    {
      axes->priv->orientation[2] = CLAMP(top[2], 0.f, 360.);
      g_object_notify_by_pspec(G_OBJECT(axes), properties[CONE_OMEGA_PROP]);
      changed = TRUE;
    }
  if (changed && axes->priv->displayOrientation)
    visu_gl_ext_setDirty(VISU_GL_EXT(axes), VISU_GL_RENDER_REQUIRED);
  g_object_thaw_notify(G_OBJECT(axes));

  return changed;
}
static gboolean visu_gl_ext_axes_setGlView(VisuGlExt *axes, VisuGlView *view)
{
  VisuGlExtAxesPrivate *priv = ((VisuGlExtAxes*)axes)->priv;

  /* No change to be done. */
  if (view == priv->view)
    return FALSE;

  if (priv->view)
    {
      g_signal_handler_disconnect(G_OBJECT(priv->view), priv->persp_signal);
      g_signal_handler_disconnect(G_OBJECT(priv->view), priv->refLength_signal);
      g_signal_handler_disconnect(G_OBJECT(priv->view), priv->widthHeight_signal);
      g_object_unref(priv->view);
      priv->persp_signal     = 0;
      priv->refLength_signal   = 0;
      priv->widthHeight_signal = 0;
      priv->color_signal       = 0;
    }
  if (view)
    {
      g_object_ref(view);
      priv->persp_signal =
        g_signal_connect_swapped(G_OBJECT(view), "notify::perspective",
                                 G_CALLBACK(onAxesParametersChange), (gpointer)axes);
      priv->refLength_signal =
        g_signal_connect_swapped(G_OBJECT(view), "RefLengthChanged",
                                 G_CALLBACK(onAxesParametersChange), (gpointer)axes);
      priv->widthHeight_signal =
        g_signal_connect_swapped(G_OBJECT(view), "WidthHeightChanged",
                                 G_CALLBACK(onAxesParametersChange), (gpointer)axes);
    }
  priv->view = view;
  g_object_notify_by_pspec(G_OBJECT(axes), properties[VIEW_PROP]);

  return TRUE;
}

/* Get methods. */
static float* _getRGB(const VisuGlExtLined *axes)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_AXES(axes), rgbDefault);
  
  return ((VisuGlExtAxes*)axes)->priv->rgb;
}
static float _getLineWidth(const VisuGlExtLined *axes)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_AXES(axes), LINE_WIDTH_DEFAULT);
  
  return ((VisuGlExtAxes*)axes)->priv->lineWidth;
}
static guint16 _getLineStipple(const VisuGlExtLined *axes)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_AXES(axes), LINE_STIPPLE_DEFAULT);
  
  return ((VisuGlExtAxes*)axes)->priv->lineStipple;
}
/**
 * visu_gl_ext_axes_getPosition:
 * @axes: the #VisuGlExtAxes object to inquire.
 * @xpos: (out) (allow-none): a location to store the x position.
 * @ypos: (out) (allow-none): a location to store the y position.
 *
 * Inquire the position of the representation of tha axes.
 *
 * Since: 3.7
 **/
void visu_gl_ext_axes_getPosition(VisuGlExtAxes *axes, float *xpos, float *ypos)
{
  g_return_if_fail(VISU_IS_GL_EXT_AXES(axes));

  if (xpos)
    *xpos = axes->priv->xpos;
  if (ypos)
    *ypos = axes->priv->ypos;
}
/**
 * visu_gl_ext_axes_setLengthFactor:
 * @axes: a #VisuGlExtAxes object.
 * @factor: a floating point value between 0. and 10.
 *
 * Change the scaling factor to draw the axis.
 *
 * Since: 3.8
 *
 * Returns: TRUE if value is indeed changed.
 **/
gboolean visu_gl_ext_axes_setLengthFactor(VisuGlExtAxes *axes, float factor)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_AXES(axes), FALSE);

  if (axes->priv->lgFact == factor)
    return FALSE;
  axes->priv->lgFact = factor;

  g_object_notify_by_pspec(G_OBJECT(axes), properties[SIZE_PROP]);
  visu_gl_ext_setDirty(VISU_GL_EXT(axes), VISU_GL_RENDER_REQUIRED);

  return TRUE;
}
/**
 * visu_gl_ext_axes_getLengthFactor:
 * @axes: a #VisuGlExtAxes object.
 *
 * Retrieve the scaling factor used to draw axis.
 *
 * Since: 3.8
 *
 * Returns: the scaling factor.
 **/
float visu_gl_ext_axes_getLengthFactor(VisuGlExtAxes *axes)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_AXES(axes), 1.f);

  return axes->priv->lgFact;
}

/**
 * visu_gl_ext_axes_getLabel:
 * @axes: a #VisuGlExtAxes object.
 * @dir: a direction.
 *
 * Retrieve the label used for direction @dir.
 *
 * Since: 3.9
 *
 * Returns: the label used for the direction.
 */
const gchar* visu_gl_ext_axes_getLabel(const VisuGlExtAxes *axes, ToolXyzDir dir)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_AXES(axes), (gchar*)0);

  return axes->priv->lbl[dir];
}

/****************/
/* Private part */
/****************/
static void onAxesParametersChange(VisuGlExtAxes *axes)
{
  g_debug("Extension Axes: caught change on view.");
  visu_gl_ext_setDirty(VISU_GL_EXT(axes), VISU_GL_DRAW_REQUIRED);
}
static void onBoxChange(VisuBox *box, float extens _U_, gpointer data)
{
  VisuGlExtAxes *axes = VISU_GL_EXT_AXES(data);
  double m[3][3];

  /* We copy the definition of the box. */
  visu_box_getCellMatrix(box, m);
  _setBasis(axes, m);
}
static void onEntryUsed(VisuGlExtAxes *axes, VisuConfigFileEntry *entry _U_, VisuConfigFile *obj _U_)
{
  visu_gl_ext_setActive(VISU_GL_EXT(axes), RESOURCE_AXES_USED_DEFAULT);
}
static void onEntryColor(VisuGlExtAxes *axes, VisuConfigFileEntry *entry _U_, VisuConfigFile *obj _U_)
{
  visu_gl_ext_lined_setRGBA(VISU_GL_EXT_LINED(axes), rgbDefault, TOOL_COLOR_MASK_RGBA);
}
static void onEntryWidth(VisuGlExtAxes *axes, VisuConfigFileEntry *entry _U_, VisuConfigFile *obj _U_)
{
  visu_gl_ext_lined_setWidth(VISU_GL_EXT_LINED(axes), LINE_WIDTH_DEFAULT);
}
static void onEntryStipple(VisuGlExtAxes *axes, VisuConfigFileEntry *entry _U_, VisuConfigFile *obj _U_)
{
  visu_gl_ext_lined_setStipple(VISU_GL_EXT_LINED(axes), LINE_STIPPLE_DEFAULT);
}
static void onEntryPosition(VisuGlExtAxes *axes, VisuConfigFileEntry *entry _U_, VisuConfigFile *obj _U_)
{
  visu_gl_ext_axes_setPosition(axes, POSITION_DEFAULT[0], POSITION_DEFAULT[1]);
}
static void onEntryLabel(VisuGlExtAxes *axes, VisuConfigFileEntry *entry, VisuConfigFile *obj _U_)
{
  if (!g_strcmp0(visu_config_file_entry_getKey(entry), FLAG_RESOURCE_AXES_LABEL_X))
    g_object_set(G_OBJECT(axes), "x-label", LABEL_DEFAULT[TOOL_XYZ_X], NULL);
  else if (!g_strcmp0(visu_config_file_entry_getKey(entry), FLAG_RESOURCE_AXES_LABEL_Y))
    g_object_set(G_OBJECT(axes), "y-label", LABEL_DEFAULT[TOOL_XYZ_Y], NULL);
  else if (!g_strcmp0(visu_config_file_entry_getKey(entry), FLAG_RESOURCE_AXES_LABEL_Z))
    g_object_set(G_OBJECT(axes), "z-label", LABEL_DEFAULT[TOOL_XYZ_Z], NULL);
}
static void onEntryFactor(VisuGlExtAxes *axes, VisuConfigFileEntry *entry _U_, VisuConfigFile *obj _U_)
{
  visu_gl_ext_axes_setLengthFactor(axes, SIZE_DEFAULT);
}

static void _buildVCone(void)
{
  float hsv[3];
  int i, j;
  double theta3;

  for (j = 0;j < nEdges / 2; j++) 
    for (i = 0; i <= nEdges; i++) 
      {
        theta3 = i * 2 * G_PI / nEdges;
	  
        hsv[0] = /*1-*/(float)i/(float)nEdges;
        hsv[1] = MIN(2*(float)(1+j)/(float)(nEdges/2), 1);
        hsv[2] = MIN(2-2*(float)(1+j)/(float)(nEdges/2), 1); 

        vCone[j][2*i][VERTEX][0] = hsv[1]*hsv[2]* cos(theta3);
        vCone[j][2*i][VERTEX][1] = /*r * e_y*/(hsv[1] - hsv[2]);
        vCone[j][2*i][VERTEX][2] = hsv[1]*hsv[2]* sin(theta3);
        tool_color_convertHSVtoRGB(vCone[j][2*i][COLOR], hsv);
        vCone[j][2*i][COLOR][3] = 1.f;
	  
        hsv[0] = /*1-*/(float)i/(float)nEdges;
        hsv[1] = MIN(2*(float)j/(float)(nEdges/2), 1);
        hsv[2] = MIN(2-2*(float)j/(float)(nEdges/2), 1); 

        vCone[j][2*i+1][VERTEX][0] = hsv[1]*hsv[2]* cos(theta3);
        vCone[j][2*i+1][VERTEX][1] = /*r * e_y*/(hsv[1] - hsv[2]);
        vCone[j][2*i+1][VERTEX][2] = hsv[1]*hsv[2]* sin(theta3);
        tool_color_convertHSVtoRGB(vCone[j][2*i+1][COLOR], hsv);
        vCone[j][2*i+1][COLOR][3] = 1.f;
      }
}

static void visu_gl_ext_axes_rebuild(VisuGlExt *ext)
{
  ToolWriter *writer = tool_writer_getStatic();
  GArray *buf;
  guint width, height;
  GError *error;
  const VisuGlExtAttrib coneAttribs[] =
    {{"position", 3, GL_FLOAT, 4 * N_STORE * sizeof(GLfloat), GINT_TO_POINTER(VERTEX * 4 * sizeof(GLfloat))},
     {"color", 4, GL_FLOAT, 4 * N_STORE * sizeof(GLfloat), GINT_TO_POINTER(COLOR * 4 * sizeof(GLfloat))},
     VISU_GL_EXT_NULL_ATTRIB};
  GArray *data, cone;

  error = (GError*)0;
  if (!visu_gl_ext_setShaderById(ext, AXES_SHADER, VISU_GL_SHADER_LINE, &error))
    {
      g_warning("Cannot create axes shaders: %s", error->message);
      g_clear_error(&error);
    }

  data = visu_gl_ext_startBuffer(ext, AXES_SHADER, AXES_ARRAY,
                                 VISU_GL_XYZ, VISU_GL_LINES);
  data->len = sizeof(VISU_GL_EXT_AXES(ext)->priv->matrix);
  visu_gl_ext_takeBuffer(ext, AXES_ARRAY, data);
  
  error = (GError*)0;
  if (!visu_gl_ext_setShaderById(ext, CONE_SHADER, VISU_GL_SHADER_SIMPLE, &error))
    {
      g_warning("Cannot create axes shaders: %s", error->message);
      g_clear_error(&error);
    }

  cone.len = sizeof(vCone);
  cone.data = (char*)vCone;
  visu_gl_ext_setBufferWithAttrib(ext, CONE_ARRAY, &cone, CONE_SHADER, coneAttribs);

  tool_writer_setSize(writer, TOOL_WRITER_FONT_NORMAL);
  buf = tool_writer_layout(writer, _("front"), &width, &height);
  visu_gl_ext_setTexture2D(ext, LBL_FRONT, (const unsigned char*)buf->data, width, height);
  g_array_unref(buf);
  buf = tool_writer_layout(writer, _("back"), &width, &height);
  visu_gl_ext_setTexture2D(ext, LBL_BACK, (const unsigned char*)buf->data, width, height);
  g_array_unref(buf);
  g_object_unref(writer);
}

/**
 * visu_gl_ext_axes_getGlView:
 * @axes: a #VisuGlExtAxes object.
 * @x: (out caller-allocates): a location for a position
 * @y: (out caller-allocates): a location for a position
 *
 * Creates a #VisuGlView corresponding to the one that is used to draw
 * the axes. Axes are then position with respect to (@x, @y) in the
 * main window.
 *
 * Since: 3.9
 *
 * Returns: (transfer full): a newly created #VisuGlView as used by
 * the @axes extension to display the axes.
 */
VisuGlView* visu_gl_ext_axes_getGlView(const VisuGlExtAxes *axes, guint *x, guint *y)
{
  VisuGlView *view;
  guint w;
  ToolGlCamera camera;

  g_return_val_if_fail(VISU_IS_GL_EXT_AXES(axes), (VisuGlView*)0);

  w = axes->priv->lgFact * MIN(visu_gl_view_getWidth(axes->priv->view),
                               visu_gl_view_getHeight(axes->priv->view));
  if (axes->priv->displayOrientation)
    w = 1.333f;

  tool_gl_camera_copy(&camera, &axes->priv->view->camera);
  camera.xs = 0.5f;
  camera.ys = 0.5f;
  camera.gross = 1.f;
  camera.length0 = 1.5f;
  camera.extens = 1.5f;

  *x = (visu_gl_view_getWidth(axes->priv->view) - w) * axes->priv->xpos;
  *y = (visu_gl_view_getHeight(axes->priv->view) - w) * axes->priv->ypos;

  view = visu_gl_view_new_full(w, w, &camera);
  return view;
}

static void _setMVP(const VisuGlExtAxes *axes, ToolGlMatrix *MVP, GLsizei *w, GLsizei *h)
{
  ToolGlCamera camera;
  ToolGlMatrix MV, P;

  *w = axes->priv->lgFact * MIN(visu_gl_view_getWidth(axes->priv->view),
                                visu_gl_view_getHeight(axes->priv->view));
  if (axes->priv->displayOrientation)
    *w *= 1.333f;
  *h = *w;

  tool_gl_camera_copy(&camera, &axes->priv->view->camera);
  camera.xs = 0.5f;
  camera.ys = 0.5f;
  camera.gross = 1.f;
  camera.length0 = 1.5f;
  camera.extens = 1.5f;

  tool_gl_matrix_project(&P, &camera, *w, *h);
  tool_gl_matrix_modelize(&MV, &camera);
  tool_gl_matrix_matrixProd(MVP, &P, &MV);
}

static void visu_gl_ext_axes_render(const VisuGlExt *ext)
{
  VisuGlExtAxes *axes;
  gint i;
  GLint x, y;
  GLsizei w, h;
  ToolGlMatrix MVP, coneMVP;
  const GLint pos[nEdges / 2] = {0,
                                 1 * 2 * (nEdges + 1),
                                 2 * 2 * (nEdges + 1),
                                 3 * 2 * (nEdges + 1),
                                 4 * 2 * (nEdges + 1),
                                 5 * 2 * (nEdges + 1),
                                 6 * 2 * (nEdges + 1),
                                 7 * 2 * (nEdges + 1)};
  const GLsizei count[nEdges / 2] = {2 * (nEdges + 1),
                                     2 * (nEdges + 1),
                                     2 * (nEdges + 1),
                                     2 * (nEdges + 1),
                                     2 * (nEdges + 1),
                                     2 * (nEdges + 1),
                                     2 * (nEdges + 1),
                                     2 * (nEdges + 1)};
  const gfloat coneScale[] = {0.667f, 0.8f, 0.667f};

  axes = VISU_GL_EXT_AXES(ext);
  if (!axes->priv->view)
    return;
  
  if (axes->priv->displayOrientation)
    {
      /* Resetting the depth buffer. */
      glClear(GL_DEPTH_BUFFER_BIT);
      glEnable(GL_DEPTH_TEST);
    }
  else
    glDisable(GL_DEPTH_TEST);

  glLineWidth(axes->priv->lineWidth);

  _setMVP(axes, &MVP, &w, &h);
  if (axes->priv->displayOrientation)
    {
      coneMVP = MVP;
      tool_gl_matrix_rotate(&coneMVP, axes->priv->orientation[1], TOOL_XYZ_Z);
      tool_gl_matrix_rotate(&coneMVP, axes->priv->orientation[0], TOOL_XYZ_Y);
      tool_gl_matrix_rotate(&coneMVP, axes->priv->orientation[2], TOOL_XYZ_Z);
      tool_gl_matrix_rotate(&coneMVP, -90, TOOL_XYZ_X);
      tool_gl_matrix_scale(&coneMVP, coneScale);
    }
  x = (visu_gl_view_getWidth(axes->priv->view) - w) * axes->priv->xpos;
  y = (visu_gl_view_getHeight(axes->priv->view) - h) * (1.f - axes->priv->ypos);
  for (i = 0; i < (axes->priv->displayOrientation ? 2 : 1); i++)
    {
      glViewport(x, y + (i == 1 ? h : 0), w, h);

      if (axes->priv->displayOrientation)
        {
          glEnable(GL_CULL_FACE);
          if (i == 0)
            glCullFace(GL_BACK);
          else
            glCullFace(GL_FRONT);
          glFrontFace(GL_CW);
          visu_gl_ext_startRenderShader(ext, CONE_SHADER, CONE_ARRAY);
          visu_gl_ext_setUniformMVP(ext, CONE_SHADER, &coneMVP);
          glMultiDrawArrays(GL_QUAD_STRIP, pos, count, nEdges / 2);
          visu_gl_ext_stopRenderShader(ext, CONE_SHADER, CONE_ARRAY);
          glFrontFace(GL_CCW);
        }

      visu_gl_ext_startRenderShader(ext, AXES_SHADER, AXES_ARRAY);
      visu_gl_ext_setUniformMVP(ext, AXES_SHADER, &MVP);
      visu_gl_ext_setUniformRGBA(ext, axes->priv->rgb);
      visu_gl_ext_setUniformStipple(ext, axes->priv->lineStipple);
      glDrawArrays(GL_LINES, 0, nVertices);
      visu_gl_ext_stopRenderShader(ext, AXES_SHADER, AXES_ARRAY);
    }

  glViewport(0, 0, visu_gl_view_getWidth(axes->priv->view),
             visu_gl_view_getHeight(axes->priv->view));
  glCullFace(GL_BACK);
}

static void visu_gl_ext_axes_renderText(const VisuGlExt *ext)
{
  VisuGlExtAxes *axes;
  gint i;
  GLint x, y;
  GLsizei w, h;
  ToolGlMatrix MVP;
  const gfloat zero[3] = {0.f, 0.f, 0.f};

  axes = VISU_GL_EXT_AXES(ext);
  if (!axes->priv->view)
    return;
  
  if (axes->priv->displayOrientation)
    glEnable(GL_DEPTH_TEST);
  else
    glDisable(GL_DEPTH_TEST);
  _setMVP(axes, &MVP, &w, &h);
  visu_gl_ext_setUniformTexMVP(ext, &MVP);
  visu_gl_ext_setUniformTexRGBA(ext, axes->priv->rgb);
  visu_gl_ext_setUniformTexResolution(ext, w, h);
  x = (visu_gl_view_getWidth(axes->priv->view) - w) * axes->priv->xpos;
  y = (visu_gl_view_getHeight(axes->priv->view) - h) * (1.f - axes->priv->ypos);
  for (i = 0; i < (axes->priv->displayOrientation ? 2 : 1); i++)
    {
      g_debug("Extension Axes: render at %dx%d (%dx%d).",
              x, y + (i == 1 ? h : 0), w, h);
      glViewport(x, y + (i == 1 ? h : 0), w, h);

      visu_gl_ext_blitTextureAt(ext, LBL_X, axes->priv->matrix[1], zero);
      visu_gl_ext_blitTextureAt(ext, LBL_Y, axes->priv->matrix[3], zero);
      visu_gl_ext_blitTextureAt(ext, LBL_Z, axes->priv->matrix[5], zero);

      if (axes->priv->displayOrientation)
        {
          const gfloat at[2] = {0.5f, 0.f};
          visu_gl_ext_blitTextureOnScreen(ext, i == 0 ? LBL_FRONT : LBL_BACK, at,
                                          VISU_GL_CENTER, VISU_GL_CENTER);
        }
    }
  g_debug("Extension Axes: put back the viewport to %dx%d.",
          visu_gl_view_getWidth(axes->priv->view),
          visu_gl_view_getHeight(axes->priv->view));
  glViewport(0, 0, visu_gl_view_getWidth(axes->priv->view),
             visu_gl_view_getHeight(axes->priv->view));
}

static void visu_gl_ext_axes_draw(VisuGlExt *ext)
{
  VisuGlExtAxes *axes;
  ToolWriter *writer;
  GArray *buf, matrix;
  guint width, height;

  g_return_if_fail(VISU_IS_GL_EXT_AXES(ext));
  axes = VISU_GL_EXT_AXES(ext);

  if (axes->priv->displayOrientation)
    {
      axes->priv->matrix[0][0] = -axes->priv->matrix[1][0];
      axes->priv->matrix[0][1] = -axes->priv->matrix[1][1];
      axes->priv->matrix[0][2] = -axes->priv->matrix[1][2];
      axes->priv->matrix[2][0] = -axes->priv->matrix[3][0];
      axes->priv->matrix[2][1] = -axes->priv->matrix[3][1];
      axes->priv->matrix[2][2] = -axes->priv->matrix[3][2];
      axes->priv->matrix[4][0] = -axes->priv->matrix[5][0];
      axes->priv->matrix[4][1] = -axes->priv->matrix[5][1];
      axes->priv->matrix[4][2] = -axes->priv->matrix[5][2];
    }
  else
    {
      axes->priv->matrix[0][0] = 0.f;
      axes->priv->matrix[0][1] = 0.f;
      axes->priv->matrix[0][2] = 0.f;
      axes->priv->matrix[2][0] = 0.f;
      axes->priv->matrix[2][1] = 0.f;
      axes->priv->matrix[2][2] = 0.f;
      axes->priv->matrix[4][0] = 0.f;
      axes->priv->matrix[4][1] = 0.f;
      axes->priv->matrix[4][2] = 0.f;
    }
  matrix.len = sizeof(axes->priv->matrix);
  matrix.data = (char*)axes->priv->matrix;
  visu_gl_ext_transferBuffer(ext, AXES_ARRAY, &matrix, 0);

  writer = tool_writer_getStatic();
  tool_writer_setSize(writer, TOOL_WRITER_FONT_NORMAL);
  buf = tool_writer_layout(writer, axes->priv->lbl[0], &width, &height);
  visu_gl_ext_setTexture2D(ext, LBL_X, (const unsigned char*)buf->data, width, height);
  g_array_unref(buf);
  buf = tool_writer_layout(writer, axes->priv->lbl[1], &width, &height);
  visu_gl_ext_setTexture2D(ext, LBL_Y, (const unsigned char*)buf->data, width, height);
  g_array_unref(buf);
  buf = tool_writer_layout(writer, axes->priv->lbl[2], &width, &height);
  visu_gl_ext_setTexture2D(ext, LBL_Z, (const unsigned char*)buf->data, width, height);
  g_array_unref(buf);
  g_object_unref(writer);
}

/*************************/
/* Parameters & resources*/
/*************************/
/* Export function that is called by visu_module to write the
   values of resources to a file. */
static void exportResources(GString *data, VisuData *dataObj _U_)
{
  if (!defaultAxes)
    return;

  visu_config_file_exportComment(data, DESC_RESOURCE_AXES_USED);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_AXES_USED, NULL,
                               "%d", visu_gl_ext_getActive(VISU_GL_EXT(defaultAxes)));

  visu_config_file_exportComment(data, DESC_RESOURCE_AXES_COLOR);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_AXES_COLOR, NULL,
                               "%4.3f %4.3f %4.3f", defaultAxes->priv->rgb[0],
                               defaultAxes->priv->rgb[1], defaultAxes->priv->rgb[2]);

  visu_config_file_exportComment(data, DESC_RESOURCE_AXES_LINE);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_AXES_LINE, NULL,
                               "%4.0f", defaultAxes->priv->lineWidth);

  visu_config_file_exportComment(data, DESC_RESOURCE_AXES_STIPPLE);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_AXES_STIPPLE, NULL,
                               "%d", defaultAxes->priv->lineStipple);

  visu_config_file_exportComment(data, DESC_RESOURCE_AXES_POSITION);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_AXES_POSITION, NULL,
                               "%4.3f %4.3f", defaultAxes->priv->xpos,
                               defaultAxes->priv->ypos);

  visu_config_file_exportComment(data, DESC_RESOURCE_AXES_LABEL);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_AXES_LABEL_X, NULL,
                               "%s", defaultAxes->priv->lbl[TOOL_XYZ_X]);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_AXES_LABEL_Y, NULL,
                               "%s", defaultAxes->priv->lbl[TOOL_XYZ_Y]);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_AXES_LABEL_Z, NULL,
                               "%s", defaultAxes->priv->lbl[TOOL_XYZ_Z]);

  visu_config_file_exportEntry(data, FLAG_RESOURCE_AXES_FACTOR, NULL,
                               "%4.3f", defaultAxes->priv->lgFact);

  visu_config_file_exportComment(data, "");
}
