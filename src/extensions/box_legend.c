/* -*- mode: C; c-basic-offset: 2 -*- */
/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include "box_legend.h"

#include <visu_configFile.h>

#include <math.h>
#include <epoxy/gl.h>

/**
 * SECTION:box_legend
 * @short_description: Draw a legend describing the box.
 *
 * <para>This extension allows V_Sim to draw a small box with box
 * information like the box size.</para>
 */

/* A resource to control the position to render the box lengths. */
#define FLAG_RESOURCE_LEGEND_POSITION   "box_legend_position"
#define DESC_RESOURCE_LEGEND_POSITION   "Position of the legend area for the box ; two floating point values (0. <= v <= 1.)"
static float POSITION_DEFAULT[2] = {.5f, 0.f};

/* A resource to control if the box lengths are printed. */
#define FLAG_RESOURCE_BOX_LENGTHS   "box_show_lengths"
#define DESC_RESOURCE_BOX_LENGTHS   "Print the box lengths ; boolean (0 or 1)"
static gboolean WITH_LENGTHS_DEFAULT = FALSE;
static void exportResourcesBoxLegend(GString *data, VisuData *dataObj);

/**
 * VisuGlExtBoxLegendClass:
 * @parent: the parent class;
 *
 * A short way to identify #_VisuGlExtBoxLegendClass structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtBoxLegend:
 *
 * An opaque structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtBoxLegendPrivate:
 *
 * Private fields for #VisuGlExtBoxLegend objects.
 *
 * Since: 3.7
 */
struct _VisuGlExtBoxLegendPrivate
{
  gboolean dispose_has_run;

  /* Box definition. */
  VisuBox *box;
  gulong size_sig, unit_sig;
};
static VisuGlExtBoxLegend* defaultBoxLegend;

static GObject* visu_gl_ext_box_legend_constructor(GType gtype, guint nprops, GObjectConstructParam *props);
static void visu_gl_ext_box_legend_dispose(GObject* obj);
static void visu_gl_ext_box_legend_draw(VisuGlExt *frame);
static void visu_gl_ext_box_legend_render_text(const VisuGlExtFrame *frame);
static gboolean visu_gl_ext_box_legend_isValid(const VisuGlExtFrame *frame);

/* Callbacks. */
static void _setDirty(VisuGlExt *ext);
static void onEntryLgUsed(VisuGlExtBoxLegend *lg, VisuConfigFileEntry *entry, VisuConfigFile *obj);
static void onEntryLgPosition(VisuGlExtBoxLegend *lg, VisuConfigFileEntry *entry, VisuConfigFile *obj);

G_DEFINE_TYPE_WITH_CODE(VisuGlExtBoxLegend, visu_gl_ext_box_legend, VISU_TYPE_GL_EXT_FRAME,
                        G_ADD_PRIVATE(VisuGlExtBoxLegend))

static void visu_gl_ext_box_legend_class_init(VisuGlExtBoxLegendClass *klass)
{
  float rgPos[2] = {0.f, 1.f};
  VisuConfigFileEntry *resourceEntry;

  g_debug("Extension Box: creating the class of the object (legend).");
  /* g_debug("                - adding new signals ;"); */

  g_debug("                - adding new resources ;");
  resourceEntry = visu_config_file_addBooleanEntry(VISU_CONFIG_FILE_RESOURCE,
                                                   FLAG_RESOURCE_BOX_LENGTHS,
                                                   DESC_RESOURCE_BOX_LENGTHS,
                                                   &WITH_LENGTHS_DEFAULT, FALSE);
  visu_config_file_entry_setVersion(resourceEntry, 3.6f);
  resourceEntry = visu_config_file_addFloatArrayEntry(VISU_CONFIG_FILE_RESOURCE,
                                                      FLAG_RESOURCE_LEGEND_POSITION,
                                                      DESC_RESOURCE_LEGEND_POSITION,
                                                      2, POSITION_DEFAULT, rgPos, FALSE);
  visu_config_file_entry_setVersion(resourceEntry, 3.7f);
  visu_config_file_addExportFunction(VISU_CONFIG_FILE_RESOURCE,
                                     exportResourcesBoxLegend);

  defaultBoxLegend = (VisuGlExtBoxLegend*)0;

  /* Connect the overloading methods. */
  G_OBJECT_CLASS(klass)->constructor = visu_gl_ext_box_legend_constructor;
  G_OBJECT_CLASS(klass)->dispose = visu_gl_ext_box_legend_dispose;
  VISU_GL_EXT_CLASS(klass)->draw = visu_gl_ext_box_legend_draw;
  VISU_GL_EXT_FRAME_CLASS(klass)->renderText = visu_gl_ext_box_legend_render_text;
  VISU_GL_EXT_FRAME_CLASS(klass)->isValid = visu_gl_ext_box_legend_isValid;
}

static GObject* visu_gl_ext_box_legend_constructor(GType gtype, guint nprops, GObjectConstructParam *props)
{
  guint i;

  for (i = 0; i < nprops; ++i)
    {
      if (!g_strcmp0(g_param_spec_get_name(props[i].pspec), "nGlTex"))
        g_value_set_uint(props[i].value, 3);
      else if (!g_strcmp0(g_param_spec_get_name(props[i].pspec), "priority"))
        g_value_set_uint(props[i].value, VISU_GL_EXT_PRIORITY_LAST);
    }

  return G_OBJECT_CLASS(visu_gl_ext_box_legend_parent_class)->constructor(gtype, nprops, props);
}

static void visu_gl_ext_box_legend_init(VisuGlExtBoxLegend *obj)
{
  g_debug("Extension Box: initializing a new legend object (%p).",
	      (gpointer)obj);
  
  obj->priv = visu_gl_ext_box_legend_get_instance_private(obj);
  obj->priv->dispose_has_run = FALSE;

  /* Private data. */
  obj->priv->box      = (VisuBox*)0;
  obj->priv->size_sig = 0;
  obj->priv->unit_sig = 0;

  g_signal_connect_object(VISU_CONFIG_FILE_RESOURCE, "parsed::" FLAG_RESOURCE_BOX_LENGTHS,
                          G_CALLBACK(onEntryLgUsed), (gpointer)obj, G_CONNECT_SWAPPED);
  g_signal_connect_object(VISU_CONFIG_FILE_RESOURCE, "parsed::" FLAG_RESOURCE_LEGEND_POSITION,
                          G_CALLBACK(onEntryLgPosition), (gpointer)obj, G_CONNECT_SWAPPED);

  if (!defaultBoxLegend)
    defaultBoxLegend = obj;
}
static void visu_gl_ext_box_legend_dispose(GObject* obj)
{
  VisuGlExtBoxLegend *legend;

  g_debug("Extension Box: dispose legend object %p.", (gpointer)obj);

  legend = VISU_GL_EXT_BOX_LEGEND(obj);
  if (legend->priv->dispose_has_run)
    return;
  legend->priv->dispose_has_run = TRUE;

  /* Disconnect signals. */
  visu_gl_ext_box_legend_setBox(legend, (VisuBox*)0);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_gl_ext_box_legend_parent_class)->dispose(obj);
}

/**
 * visu_gl_ext_box_legend_new:
 * @name: (allow-none): the name to give to the extension.
 *
 * Creates a new #VisuGlExt to draw a legend with the box size.
 *
 * Since: 3.7
 *
 * Returns: a pointer to the #VisuGlExt it created or
 * NULL otherwise.
 */
VisuGlExtBoxLegend* visu_gl_ext_box_legend_new(const gchar *name)
{
  char *name_ = VISU_GL_EXT_BOX_LEGEND_ID;
  char *description = _("Draw informations related to the box.");
#define BOX_WIDTH   100
#define BOX_HEIGHT   55

  g_debug("Extension Box: new legend object.");
  
  return VISU_GL_EXT_BOX_LEGEND(g_object_new(VISU_TYPE_GL_EXT_BOX_LEGEND,
                                             "active", WITH_LENGTHS_DEFAULT,
                                             "name", (name)?name:name_, "label", _(name),
                                             "description", description,
                                             "title", _("Box lengths"),
                                             "x-pos", POSITION_DEFAULT[0],
                                             "y-pos", POSITION_DEFAULT[1],
                                             "x-requisition", BOX_WIDTH,
                                             "y-requisition", BOX_HEIGHT, NULL));
}
/**
 * visu_gl_ext_box_legend_setBox:
 * @legend: The #VisuGlExtBoxLegend to attached to.
 * @boxObj: the box to get the size of.
 *
 * Attach an #VisuGlView to render to and setup the box to get the
 * size of also.
 *
 * Since: 3.7
 *
 * Returns: TRUE if model is changed.
 **/
gboolean visu_gl_ext_box_legend_setBox(VisuGlExtBoxLegend *legend, VisuBox *boxObj)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_BOX_LEGEND(legend), FALSE);

  g_debug("Extension Box Legend: set box %p.", (gpointer)boxObj);

  if (legend->priv->box == boxObj)
    return FALSE;

  if (legend->priv->box)
    {
      g_signal_handler_disconnect(G_OBJECT(legend->priv->box), legend->priv->size_sig);
      g_signal_handler_disconnect(G_OBJECT(legend->priv->box), legend->priv->unit_sig);
      g_object_unref(legend->priv->box);
    }
  if (boxObj)
    {
      g_object_ref(boxObj);
      legend->priv->size_sig =
        g_signal_connect_swapped(G_OBJECT(boxObj), "SizeChanged",
                                 G_CALLBACK(_setDirty), (gpointer)legend);
      legend->priv->unit_sig =
        g_signal_connect_swapped(G_OBJECT(boxObj), "unit-changed",
                                 G_CALLBACK(_setDirty), (gpointer)legend);
    }
  else
    {
      legend->priv->size_sig = 0;
      legend->priv->unit_sig = 0;
    }
  legend->priv->box = boxObj;

  visu_gl_ext_setDirty(VISU_GL_EXT(legend), VISU_GL_DRAW_REQUIRED);
  return TRUE;
}


/****************/
/* Private part */
/****************/
static void _setDirty(VisuGlExt *ext)
{
  g_debug("Extension Box: caught the 'SizeChanged' or 'unit-changed' signal.");
  visu_gl_ext_setDirty(ext, VISU_GL_DRAW_REQUIRED);
}
static void onEntryLgUsed(VisuGlExtBoxLegend *lg, VisuConfigFileEntry *entry _U_, VisuConfigFile *obj _U_)
{
  visu_gl_ext_setActive(VISU_GL_EXT(lg), WITH_LENGTHS_DEFAULT);
}
static void onEntryLgPosition(VisuGlExtBoxLegend *lg, VisuConfigFileEntry *entry _U_, VisuConfigFile *obj _U_)
{
  visu_gl_ext_frame_setPosition(VISU_GL_EXT_FRAME(lg),
                                POSITION_DEFAULT[0], POSITION_DEFAULT[1]);
}
static gboolean visu_gl_ext_box_legend_isValid(const VisuGlExtFrame *frame)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_BOX_LEGEND(frame), FALSE);

  /* Nothing to draw if no data is associated to
     the current rendering window. */
  return (VISU_GL_EXT_BOX_LEGEND(frame)->priv->box != (VisuBox*)0);
}
static void visu_gl_ext_box_legend_draw(VisuGlExt *self)
{
  float vertices[8][3], box[3];
  ToolUnits units;
  VisuGlExtBoxLegend *legend;
  VisuGlExtFrame *frame;
  VisuGlExtLabel lbl;
  GArray *labels;

  g_return_if_fail(VISU_IS_GL_EXT_BOX_LEGEND(self));
  legend = VISU_GL_EXT_BOX_LEGEND(self);
  frame = VISU_GL_EXT_FRAME(self);

  VISU_GL_EXT_CLASS(visu_gl_ext_box_legend_parent_class)->draw(self);

  if (!legend->priv->box)
    return;

  visu_box_getVertices(legend->priv->box, vertices, FALSE);
  box[0] = sqrt((vertices[1][0] - vertices[0][0]) *
		(vertices[1][0] - vertices[0][0]) +
		(vertices[1][1] - vertices[0][1]) *
		(vertices[1][1] - vertices[0][1]) +
		(vertices[1][2] - vertices[0][2]) *
		(vertices[1][2] - vertices[0][2]));
  box[1] = sqrt((vertices[3][0] - vertices[0][0]) *
		(vertices[3][0] - vertices[0][0]) +
		(vertices[3][1] - vertices[0][1]) *
		(vertices[3][1] - vertices[0][1]) +
		(vertices[3][2] - vertices[0][2]) *
		(vertices[3][2] - vertices[0][2]));
  box[2] = sqrt((vertices[4][0] - vertices[0][0]) *
		(vertices[4][0] - vertices[0][0]) +
		(vertices[4][1] - vertices[0][1]) *
		(vertices[4][1] - vertices[0][1]) +
		(vertices[4][2] - vertices[0][2]) *
		(vertices[4][2] - vertices[0][2]));
  units = visu_box_getUnit(legend->priv->box);

  labels = g_array_new(FALSE, FALSE, sizeof(VisuGlExtLabel));
  lbl.xyz[0] = 0.5f;
  lbl.xyz[1] = 2.f / 3.f;
  lbl.rgba[0] = frame->fontRGB[0];
  lbl.rgba[1] = frame->fontRGB[1];
  lbl.rgba[2] = frame->fontRGB[2];
  lbl.rgba[3] = 1.f;
  lbl.align[0] = VISU_GL_CENTER;
  lbl.align[1] = VISU_GL_LEFT;
  sprintf(lbl.lbl, "%s: %7.3f %s", "x", box[0], tool_physic_getUnitLabel(units));
  g_array_append_val(labels, lbl);
  lbl.xyz[1] = 1.f / 3.f;
  sprintf(lbl.lbl, "%s: %7.3f %s", "y", box[1], tool_physic_getUnitLabel(units));
  g_array_append_val(labels, lbl);
  lbl.xyz[1] = 0.f;
  sprintf(lbl.lbl, "%s: %7.3f %s", "z", box[2], tool_physic_getUnitLabel(units));
  g_array_append_val(labels, lbl);

  visu_gl_ext_setLabels(VISU_GL_EXT(frame), labels, TOOL_WRITER_FONT_SMALL, FALSE);
  g_array_unref(labels);
}
static void visu_gl_ext_box_legend_render_text(const VisuGlExtFrame *frame)
{
  visu_gl_ext_blitLabelsOnScreen(VISU_GL_EXT(frame));
}

/* Parameters & resources*/
/* Export function that is called by visu_module to write the
   values of resources to a file. */
static void exportResourcesBoxLegend(GString *data, VisuData *dataObj _U_)
{
  float xpos, ypos;

  if (!defaultBoxLegend)
    return;

  visu_config_file_exportComment(data, DESC_RESOURCE_BOX_LENGTHS);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_BOX_LENGTHS, NULL,
                               "%d", visu_gl_ext_getActive(VISU_GL_EXT(defaultBoxLegend)));

  visu_gl_ext_frame_getPosition(VISU_GL_EXT_FRAME(defaultBoxLegend), &xpos, &ypos);
  visu_config_file_exportComment(data, DESC_RESOURCE_LEGEND_POSITION);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_LEGEND_POSITION, NULL,
                               "%4.3f %4.3f", xpos, ypos);

  visu_config_file_exportComment(data, "");
}
