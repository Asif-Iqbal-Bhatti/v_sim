/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "fogAndBGColor.h"

#include <visu_tools.h>
#include <visu_configFile.h>
#include <coreTools/toolColor.h>

/* For the GdkPixbuf, to be removed later. */
#include <gtk/gtk.h>

#include <epoxy/gl.h>

/**
 * SECTION:fogAndBGColor
 * @short_description: Handle the background colour and the fog.
 *
 * This module is used to support a background colour and to tune the
 * fog. This last one can be turn on or off and its colour can be
 * either a user defined one or the one of the background. The fog is
 * a linear blending into the fog colour. It starts at a given z
 * position (in the camera basis set) and ends at a lower z.
 */

/**
 * VisuGlExtBgClass:
 * @parent: the parent class;
 *
 * A short way to identify #_VisuGlExtBgClass structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtBg:
 *
 * An opaque structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtBgPrivate:
 *
 * Private fields for #VisuGlExtBg objects.
 *
 * Since: 3.7
 */
struct _VisuGlExtBgPrivate
{
  gboolean dispose_has_run;

  /* Handling the background image. */
  guchar *bgImage;
  gboolean bgImageAlpha, bgImageFit, bgImageFollowZoom;
  guint bgImageW, bgImageH;
  gchar *bgImageTitle;
  float bgImageZoomInit, bgImageZoom, bgImageZoomRatioInit;
  float bgImageXsInit, bgImageXs, bgImageXs0;
  float bgImageYsInit, bgImageYs, bgImageYs0;

  gchar *bgFile;
  gboolean withLabel;

  /* Signals for the current view. */
  VisuGlView *view;
  gulong widthHeight_signal;
  gulong transx_signal, transy_signal, gross_signal;
};

enum
  {
    PROP_0,
    BG_IMAGE_PROP,
    BG_LABEL_PROP,
    N_PROP,
  };
static GParamSpec *_properties[N_PROP];

static GObject* visu_gl_ext_bg_constructor(GType gtype, guint nprops, GObjectConstructParam *props);
static void visu_gl_ext_bg_finalize(GObject* obj);
static void visu_gl_ext_bg_dispose(GObject* obj);
static void visu_gl_ext_bg_get_property(GObject* obj, guint property_id,
                                        GValue *value, GParamSpec *pspec);
static void visu_gl_ext_bg_set_property(GObject* obj, guint property_id,
                                        const GValue *value, GParamSpec *pspec);
static void visu_gl_ext_bg_draw(VisuGlExt *bg);
static void visu_gl_ext_bg_render(const VisuGlExt *bg);
static gboolean visu_gl_ext_bg_setGlView(VisuGlExt *bg, VisuGlView *view);

/* Local callbacks */
static void onBgImageRescale(VisuGlView *view, gpointer data);
static void onCameraChange(VisuGlView *view, GParamSpec *pspec, gpointer data);

G_DEFINE_TYPE_WITH_CODE(VisuGlExtBg, visu_gl_ext_bg, VISU_TYPE_GL_EXT,
                        G_ADD_PRIVATE(VisuGlExtBg))

static void visu_gl_ext_bg_class_init(VisuGlExtBgClass *klass)
{
  g_debug("Extension Bg: creating the class of the object.");
  /* g_debug("                - adding new signals ;"); */

  /* Connect the overloading methods. */
  G_OBJECT_CLASS(klass)->constructor = visu_gl_ext_bg_constructor;
  G_OBJECT_CLASS(klass)->dispose  = visu_gl_ext_bg_dispose;
  G_OBJECT_CLASS(klass)->finalize = visu_gl_ext_bg_finalize;
  G_OBJECT_CLASS(klass)->set_property = visu_gl_ext_bg_set_property;
  G_OBJECT_CLASS(klass)->get_property = visu_gl_ext_bg_get_property;
  VISU_GL_EXT_CLASS(klass)->draw = visu_gl_ext_bg_draw;
  VISU_GL_EXT_CLASS(klass)->render = visu_gl_ext_bg_render;
  VISU_GL_EXT_CLASS(klass)->setGlView = visu_gl_ext_bg_setGlView;

  /**
   * VisuGlNodeScene::background-file:
   *
   * Path to the background image.
   *
   * Since: 3.8
   */
  _properties[BG_IMAGE_PROP] =
    g_param_spec_string("background-file", "Background file",
                        "path to the background image.",
                        "", G_PARAM_READWRITE);
  /**
   * VisuGlNodeScene::display-background-filename:
   *
   * Display or not the background filename.
   *
   * Since: 3.8
   */
  _properties[BG_LABEL_PROP] =
    g_param_spec_boolean("display-background-filename", "Display background filename",
                         "display or not the background filename.",
                         FALSE, G_PARAM_READWRITE);

  g_object_class_install_properties(G_OBJECT_CLASS(klass), N_PROP, _properties);
}

static GObject* visu_gl_ext_bg_constructor(GType gtype, guint nprops, GObjectConstructParam *props)
{
  guint i;

  for (i = 0; i < nprops; ++i)
    {
      if (!g_strcmp0(g_param_spec_get_name(props[i].pspec), "nGlTex"))
        g_value_set_uint(props[i].value, 2);
      if (!g_strcmp0(g_param_spec_get_name(props[i].pspec), "priority"))
        g_value_set_uint(props[i].value, VISU_GL_EXT_PRIORITY_BACKGROUND);
    }

  return G_OBJECT_CLASS(visu_gl_ext_bg_parent_class)->constructor(gtype, nprops, props);
}

static void visu_gl_ext_bg_init(VisuGlExtBg *obj)
{
  g_debug("Extension Bg: initializing a new object (%p).",
	      (gpointer)obj);
  
  obj->priv = visu_gl_ext_bg_get_instance_private(obj);
  obj->priv->dispose_has_run = FALSE;

  /* Private data. */
  obj->priv->bgImage      = (guchar*)0;
  obj->priv->bgImageTitle = (gchar*)0;
  obj->priv->bgImageFollowZoom = FALSE;
  obj->priv->bgImageZoomInit = obj->priv->bgImageZoom = -1.f;
  obj->priv->bgImageZoomRatioInit = 1.f;
  obj->priv->bgImageXsInit = obj->priv->bgImageXs = 0.5f;
  obj->priv->bgImageXs0 = 0.f;
  obj->priv->bgImageYsInit = obj->priv->bgImageYs = 0.5f;
  obj->priv->bgImageYs0 = 0.f;
  obj->priv->view               = (VisuGlView*)0;
  obj->priv->widthHeight_signal = 0;
  obj->priv->bgFile = (gchar*)0;
  obj->priv->withLabel = FALSE;
}
static void visu_gl_ext_bg_dispose(GObject* obj)
{
  VisuGlExtBg *bg;

  g_debug("Extension Bg: dispose object %p.", (gpointer)obj);

  bg = VISU_GL_EXT_BG(obj);
  if (bg->priv->dispose_has_run)
    return;
  bg->priv->dispose_has_run = TRUE;

  /* Disconnect signals. */
  visu_gl_ext_bg_setGlView(VISU_GL_EXT(bg), (VisuGlView*)0);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_gl_ext_bg_parent_class)->dispose(obj);
}
static void visu_gl_ext_bg_finalize(GObject* obj)
{
  VisuGlExtBg *bg;

  g_return_if_fail(obj);

  g_debug("Extension Bg: finalize object %p.", (gpointer)obj);

  bg = VISU_GL_EXT_BG(obj);

  /* Free privs elements. */
  g_debug("Extension Bg: free private bg.");
  g_free(bg->priv->bgImage);
  g_free(bg->priv->bgImageTitle);
  g_free(bg->priv->bgFile);

  /* Chain up to the parent class */
  g_debug("Extension Bg: chain to parent.");
  G_OBJECT_CLASS(visu_gl_ext_bg_parent_class)->finalize(obj);
  g_debug("Extension Bg: freeing ... OK.");
}
static void visu_gl_ext_bg_get_property(GObject* obj, guint property_id,
                                        GValue *value, GParamSpec *pspec)
{
  VisuGlExtBg *self = VISU_GL_EXT_BG(obj);

  g_debug("Extension Bg: get property '%s' -> ",
	      g_param_spec_get_name(pspec));
  switch (property_id)
    {
    case BG_IMAGE_PROP:
      g_value_set_string(value, self->priv->bgFile);
      g_debug("%s.", g_value_get_string(value));
      break;
    case BG_LABEL_PROP:
      g_value_set_boolean(value, self->priv->withLabel);
      g_debug("%d.", g_value_get_boolean(value));
      break;
    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
      break;
    }
}
static void visu_gl_ext_bg_set_property(GObject* obj, guint property_id,
                                        const GValue *value, GParamSpec *pspec)
{
  VisuGlExtBg *self = VISU_GL_EXT_BG(obj);
  GError *error;
  gchar *path;

  g_debug("Extension Bg: set property '%s' -> ",
	      g_param_spec_get_name(pspec));
  switch (property_id)
    {
    case BG_IMAGE_PROP:
      g_debug("%s.", g_value_get_string(value));
      error = (GError*)0;
      visu_gl_ext_bg_setFile(self, g_value_get_string(value), &error);
      if (error)
        {
          g_warning("%s", error->message);
          g_error_free(error);
        }
      break;
    case BG_LABEL_PROP:
      g_debug("%d.", g_value_get_boolean(value));
      self->priv->withLabel = g_value_get_boolean(value);
      path = self->priv->bgFile;
      self->priv->bgFile = (gchar*)0;
      error = (GError*)0;
      visu_gl_ext_bg_setFile(self, path, &error);
      if (error)
        {
          g_warning("%s", error->message);
          g_error_free(error);
        }
      g_free(path);
      break;
    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
      break;
    }
}
/**
 * visu_gl_ext_bg_new:
 * @name: (allow-none): the name to give to the extension (default is #VISU_GL_EXT_BG_ID).
 *
 * Creates a new #VisuGlExt to draw bg.
 *
 * Since: 3.7
 *
 * Returns: a pointer to the #VisuGlExt it created or
 * NULL otherwise.
 */
VisuGlExtBg* visu_gl_ext_bg_new(const gchar *name)
{
  char *name_ = VISU_GL_EXT_BG_ID;
  char *description = _("Set an image as background.");
  VisuGlExt *extensionBg;

  g_debug("Extension Bg: new object.");
  
  extensionBg = VISU_GL_EXT(g_object_new(VISU_TYPE_GL_EXT_BG,
                                              "name", (name)?name:name_, "label", _(name),
                                              "description", description, NULL));

  return VISU_GL_EXT_BG(extensionBg);
}

static gboolean visu_gl_ext_bg_setGlView(VisuGlExt *bg, VisuGlView *view)
{
  VisuGlExtBgPrivate *priv = ((VisuGlExtBg*)bg)->priv;

  /* No change to be done. */
  if (view == priv->view)
    return FALSE;

  if (priv->view)
    {
      g_signal_handler_disconnect(G_OBJECT(priv->view), priv->widthHeight_signal);
      g_signal_handler_disconnect(G_OBJECT(priv->view), priv->transx_signal);
      g_signal_handler_disconnect(G_OBJECT(priv->view), priv->transy_signal);
      g_signal_handler_disconnect(G_OBJECT(priv->view), priv->gross_signal);
      g_object_unref(priv->view);
    }
  if (view)
    {
      g_object_ref(view);
      priv->widthHeight_signal =
        g_signal_connect(G_OBJECT(view), "WidthHeightChanged",
                         G_CALLBACK(onBgImageRescale), (gpointer)bg);
      priv->transx_signal =
        g_signal_connect(G_OBJECT(view), "notify::trans-x",
                         G_CALLBACK(onCameraChange), (gpointer)bg);
      priv->transy_signal =
        g_signal_connect(G_OBJECT(view), "notify::trans-y",
                         G_CALLBACK(onCameraChange), (gpointer)bg);
      priv->gross_signal =
        g_signal_connect(G_OBJECT(view), "notify::zoom",
                         G_CALLBACK(onCameraChange), (gpointer)bg);
    }
  else
    {
      priv->widthHeight_signal = 0;
    }
  priv->view = view;

  visu_gl_ext_setDirty(bg, VISU_GL_DRAW_REQUIRED);
  return TRUE;
}

/**
 * visu_gl_ext_bg_setFile:
 * @bg: a #VisuGlExtBg object.
 * @path: a file path
 * @error: an error location.
 *
 * Loads @path and store it as a background image for the scene, see
 * visu_gl_ext_bg_setImage().
 *
 * Since: 3.8
 *
 * Returns: FALSE if an error occured.
 **/
gboolean visu_gl_ext_bg_setFile(VisuGlExtBg *bg, const gchar *path,
                                GError **error)
{
  GdkPixbuf *pixbuf;
  gchar *title;
  gboolean fit;

  g_return_val_if_fail(VISU_IS_GL_EXT_BG(bg), FALSE);

  if (!g_strcmp0(bg->priv->bgFile, path))
    return FALSE;

  g_free(bg->priv->bgFile);
  bg->priv->bgFile = (gchar*)0;

  if (!path)
    {
      visu_gl_ext_bg_setImage(bg, (guchar*)0, 0, 0, FALSE, (const gchar*)0, TRUE);
      g_object_notify_by_pspec(G_OBJECT(bg), _properties[BG_IMAGE_PROP]);
      return TRUE;
    }
  
  pixbuf = gdk_pixbuf_new_from_file(path, error);
  if (!pixbuf)
    {
      visu_gl_ext_bg_setImage(bg, (guchar*)0, 0, 0, FALSE, (const gchar*)0, TRUE);
      g_object_notify_by_pspec(G_OBJECT(bg), _properties[BG_IMAGE_PROP]);
      return TRUE;
    }

  fit = TRUE;
  title = g_path_get_basename(path);
  if (!g_strcmp0(title, "logo_grey.png"))
    {
      fit = FALSE;
      g_free(title);
      title = (gchar*)0;
    }
  visu_gl_ext_bg_setImage(bg,
                          gdk_pixbuf_get_pixels(pixbuf),
                          gdk_pixbuf_get_width(pixbuf),
                          gdk_pixbuf_get_height(pixbuf),
                          gdk_pixbuf_get_has_alpha(pixbuf),
                          bg->priv->withLabel ? title : (const gchar*)0, fit);
  g_object_unref(pixbuf);
  g_free(title);
  bg->priv->bgFile = g_strdup(path);
  g_object_notify_by_pspec(G_OBJECT(bg), _properties[BG_IMAGE_PROP]);
  return TRUE;
}
/**
 * visu_gl_ext_bg_setImage:
 * @bg: a #VisuGlExtBg object.
 * @imageData: (allow-none): raw image data in RGB or RGBA format ;
 * @width: the width ;
 * @height: the height ;
 * @alpha: TRUE if the image is RGBA ;
 * @title: (allow-none): an optional title (can be NULL).
 * @fit: a boolean (default is TRUE).
 *
 * Draw the @imageData on the background. The image is scaled to the
 * viewport dimensions, keeping the width/height ratio, if @fit is set
 * to TRUE. If @title is not NULL, the title is also printed on the
 * background. The image data are copied and can be free after this
 * call.
 */
void visu_gl_ext_bg_setImage(VisuGlExtBg *bg,
                             const guchar *imageData, guint width, guint height,
                             gboolean alpha, const gchar *title, gboolean fit)
{
  guint n;

  g_return_if_fail(VISU_IS_GL_EXT_BG(bg));

  g_free(bg->priv->bgImage);
  bg->priv->bgImage = (guchar*)0;
  g_free(bg->priv->bgImageTitle);
  bg->priv->bgImageTitle = (gchar*)0;

  visu_gl_ext_setDirty(VISU_GL_EXT(bg), VISU_GL_DRAW_REQUIRED);

  if (!imageData)
    return;

  g_debug("Extension bg: copy image to memory buffer.");
  /* We copy the image to some correct size buffer. */
  bg->priv->bgImageW = width;
  bg->priv->bgImageH = height;
  n = (alpha)?4:3;
  bg->priv->bgImage = g_memdup2(imageData, sizeof(guchar) * bg->priv->bgImageW *
                                bg->priv->bgImageH * n);
  bg->priv->bgImageAlpha = alpha;
  if (title)
    bg->priv->bgImageTitle = g_strdup(title);
  bg->priv->bgImageFit = fit;
  bg->priv->bgImageZoomInit = bg->priv->bgImageZoom = -1.f;
  bg->priv->bgImageZoomRatioInit = 1.f;
  bg->priv->bgImageXsInit = bg->priv->bgImageXs = 0.5f;
  bg->priv->bgImageXs0 = 0.f;
  bg->priv->bgImageYsInit = bg->priv->bgImageYs = 0.5f;
  bg->priv->bgImageYs0 = 0.f;
}
/**
 * visu_gl_ext_bg_setFollowCamera:
 * @bg: a #VisuGlExtBg object.
 * @follow: a boolean.
 * @zoomInit: a floating point value.
 * @xs: a floating point value.
 * @ys: a floating point value.
 *
 * When @follow is TRUE, the size and the position of the background
 * image is adjusted with every camera change.
 *
 * Since: 3.7
 *
 * Returns: TRUE if the following status has been changed.
 */
gboolean visu_gl_ext_bg_setFollowCamera(VisuGlExtBg *bg, gboolean follow, float zoomInit,
                                        float xs, float ys)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_BG(bg), FALSE);

  if (follow == bg->priv->bgImageFollowZoom)
    return FALSE;

  bg->priv->bgImageFollowZoom = follow;
  if (follow)
    {
      bg->priv->bgImageZoomInit = bg->priv->bgImageZoom = zoomInit;
      bg->priv->bgImageXsInit = bg->priv->bgImageXs = xs;
      bg->priv->bgImageYsInit = bg->priv->bgImageYs = ys;
    }
  else
    {
      bg->priv->bgImageZoomRatioInit *= bg->priv->bgImageZoom / bg->priv->bgImageZoomInit;
      bg->priv->bgImageXs0 -= bg->priv->bgImageXs - bg->priv->bgImageXsInit;
      bg->priv->bgImageYs0 -= bg->priv->bgImageYs - bg->priv->bgImageYsInit;
    }

  visu_gl_ext_setDirty(VISU_GL_EXT(bg), VISU_GL_DRAW_REQUIRED);
  return TRUE;
}
/**
 * visu_gl_ext_bg_setCamera:
 * @bg: a #VisuGlExtBg object.
 * @zoom: a floating point value.
 * @xs: a floating point value.
 * @ys: a floating point value.
 *
 * If the background image is in follow mode, see
 * visu_gl_ext_bg_setFollowCamera(), this routine is used to update
 * the current camera settings of the background image.
 *
 * Since: 3.7
 *
 * Returns: TRUE if the settings are indeed changed.
 */
gboolean visu_gl_ext_bg_setCamera(VisuGlExtBg *bg, float zoom, float xs, float ys)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_BG(bg), FALSE);

  if (zoom == bg->priv->bgImageZoom && xs == bg->priv->bgImageXs && ys == bg->priv->bgImageYs)
    return FALSE;

  if (bg->priv->bgImageFollowZoom)
    {
      bg->priv->bgImageZoom = zoom;
      bg->priv->bgImageXs = xs;
      bg->priv->bgImageYs = ys;
    }

  return bg->priv->bgImageFollowZoom;
}

/****************/
/* Private part */
/****************/
static void visu_gl_ext_bg_draw(VisuGlExt *ext)
{
  VisuGlExtBg *bg = VISU_GL_EXT_BG(ext);

  /* Nothing to draw; */
  if(!bg->priv->view || !bg->priv->bgImage) return;

  g_debug("Extension bg: set background image.");
  visu_gl_ext_setImage(ext, 1, bg->priv->bgImage,
                       bg->priv->bgImageW, bg->priv->bgImageH, bg->priv->bgImageAlpha);
  
  if (bg->priv->bgImageTitle)
    {
      VisuGlExtLabel lbl;
      GArray *labels;
      sprintf(lbl.lbl, "%s", bg->priv->bgImageTitle);
      lbl.xyz[0] = 0.f;
      lbl.xyz[1] = 0.f;
      lbl.rgba[0] = .5f;
      lbl.rgba[1] = .5f;
      lbl.rgba[2] = .5f;
      lbl.rgba[3] = 1.f;
      lbl.align[0] = VISU_GL_LEFT;
      lbl.align[1] = VISU_GL_LEFT;
      labels = g_array_new(FALSE, FALSE, sizeof(VisuGlExtLabel));
      g_array_append_val(labels, lbl);
      visu_gl_ext_setLabels(ext, labels, TOOL_WRITER_FONT_NORMAL, TRUE);
      g_array_unref(labels);
    }
}
static void visu_gl_ext_bg_render(const VisuGlExt *ext)
{
  int viewport[4];
  float zoom, x, y;
  VisuGlExtBg *bg = VISU_GL_EXT_BG(ext);
  gfloat at[2];

  if(!bg->priv->view || !bg->priv->bgImage) return;

  glGetIntegerv(GL_VIEWPORT, viewport);
  if (bg->priv->bgImageFit)
    {
      x = (float)viewport[2] / (float)bg->priv->bgImageW;
      y = (float)viewport[3] / (float)bg->priv->bgImageH;
    }
  else
    {
      x = y = 1.f;
    }
  g_debug("Extension bg: use follow zoom %d (%g / %g).",
          bg->priv->bgImageFollowZoom, bg->priv->bgImageZoom, bg->priv->bgImageZoomInit);
  zoom = MIN(x, y) * bg->priv->bgImageZoomRatioInit * bg->priv->bgImageZoom / bg->priv->bgImageZoomInit;
  at[0]  = (1.f - zoom * (float)bg->priv->bgImageW / (float)viewport[2]) / 2.f;
  at[0] += bg->priv->bgImageXs - bg->priv->bgImageXsInit - bg->priv->bgImageXs0;
  at[1]  = (1.f - zoom * (float)bg->priv->bgImageH / (float)viewport[3]) / 2.f;
  at[1] += bg->priv->bgImageYs - bg->priv->bgImageYsInit - bg->priv->bgImageYs0;
  visu_gl_ext_adjustTextureDimensions(ext, 1, zoom * bg->priv->bgImageW, zoom * bg->priv->bgImageH);
  
  glDisable(GL_LIGHTING);
  glEnable(GL_TEXTURE_2D);
  visu_gl_ext_blitTextureOnScreen(ext, 1, at, VISU_GL_LEFT, VISU_GL_LEFT);
  glDisable(GL_TEXTURE_2D);

  if (bg->priv->bgImageTitle)
    visu_gl_ext_blitLabelsOnScreen(ext);
}

static void onBgImageRescale(VisuGlView *view _U_, gpointer data)
{
  g_debug("Extension Bg: caught the 'WidthHeightChanged' signal.");

  if (VISU_GL_EXT_BG(data)->priv->bgImage)
    visu_gl_ext_setDirty(VISU_GL_EXT(data), TRUE);
}
static void onCameraChange(VisuGlView *view, GParamSpec *pspec _U_, gpointer data)
{
  if (visu_gl_ext_bg_setCamera(VISU_GL_EXT_BG(data),
                               view->camera.gross, view->camera.xs, view->camera.ys))
    visu_gl_ext_setDirty(VISU_GL_EXT(data), TRUE);
}
