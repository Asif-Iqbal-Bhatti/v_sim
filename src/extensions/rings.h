/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2001-2009)
        David WAROQUIERS, PCPM UC Louvain la Neuve (2009)
  
	Adresse mèl :
	WAROQUIERS, david P waroquiers AT uclouvain P be
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2001-2009)
        David WAROQUIERS, PCPM UC Louvain la Neuve (2009)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr
	WAROQUIERS, david P waroquiers AT uclouvain P be

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at COPYING.
*/
#ifndef RINGS_H
#define RINGS_H

#include <visu_extension.h>
#include <visu_data.h>

/**
 * EXT_RINGS_ID:
 * 
 * The id of the ring extension.
 */
#define EXT_RINGS_ID "Rings"

VisuGlExt* initExtRings();
gboolean extRingsSet_isOn(gboolean value);
gboolean extRingsGet_isOn();

/**
 * setVisuPlaneFromBoxChange:
 * @dataObj: a #VisuData object ;
 * @boxChange: the boxchange associated to the plane ;
 * @point: a point through which the plane passes ;
 * @plane: a VisuPlane declared in which to store the plane properties.
 *
 * A method to set a plane corresponding to a box change
 */
/*
void setVisuPlaneFromBoxChange(VisuData *dataObj, float boxChange[3], float point[3], VisuPlane *plane);
*/
void extRingsDraw(VisuData *dataObj);

#endif
