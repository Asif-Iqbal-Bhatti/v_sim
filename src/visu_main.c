/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include "visu_basic.h"
#include "visu_commandLine.h"
#include "visu_tools.h"
#include "visu_basic.h"
#include "visu_plugins.h"
#include "visu_gtk.h"
#include "gtk_main.h"
#include "visu_dataloadable.h"
#include "extensions/scale.h"
#include "gtk_renderingWindowWidget.h"
#include "coreTools/toolWriter.h"

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>
#include <glib.h>

#ifdef HAVE_GTKGLEXT
#include <gdk/gdkgl.h>
#endif

#define VISU_MEM_CHECK 0

int main (int argc, char *argv[])
{
  int res;
  char *arg;
  VisuDataLoadable *newData;
  gchar *dirname, *normDir;
  GError *error;
  gboolean GUIerror;
  VisuUiRenderingWindow *window;
  ToolWriter *writer;

#if DEBUG == 1 && VISU_MEM_CHECK == 1
  g_mem_set_vtable(glib_mem_profiler_table);
#endif

  g_debug("--- Get the default path ---");
  visu_basic_setExePath(argv[0]);

#ifdef G_THREADS_ENABLED
  g_debug("--- Initialise threads ---");
#endif

#ifdef ENABLE_NLS
  setlocale (LC_ALL, "");
  bindtextdomain (GETTEXT_PACKAGE, V_SIM_LOCALE_DIR);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
  textdomain (GETTEXT_PACKAGE);
#endif

  g_debug("Visu Main: initialise Gtk.");
  GUIerror = gtk_init_check(&argc, &argv);

  g_debug("--- Parse the command line ---");
  res = commandLineParse(argc, argv);
  if (res)
    exit(1);
  /* 5444o still allocated by GLIB. */

  /* if arg is not null, v_sim is in export mode. */
  arg = commandLineGet_ExportFileName();
  if (arg)
    {
#ifdef HAVE_GTKGLEXT
      g_debug("Visu Main: initialise Gtk.");
      gtk_init(&argc, &argv);

      g_debug("Visu Main: initialise GtkGlExt.");
      gdk_gl_init(&argc, &argv);
#endif

      g_debug("--- Initialising plugins ---");
      error = (GError*)0;
      visu_plugins_init(&error);
      if (error)
        {
          g_message("Plugin error: %s", error->message);
          g_error_free(error);
        }

      g_debug("Visu Main: test for help message.");
      if (visu_basic_showOptionHelp(FALSE))
        exit(0);

      g_debug("Visu Main: V_Sim has been called in an export session,"
		  " with parameter '%s'.", arg);
      res = visu_basic_mainExport();

      return res;
    }
  g_return_val_if_fail(GUIerror, 1);

  gtk_window_set_default_icon_name("v_sim");
  gtk_icon_theme_append_search_path(gtk_icon_theme_get_default(),
                                    V_SIM_ICONS_DIR);
  g_set_application_name(_("Visualise atomic simulations"));

#ifdef HAVE_GTKGLEXT
  g_debug("Visu Main: initialise GtkGlExt.");
  gdk_gl_init(&argc, &argv);
#endif
  /* 38713o still allocated by GLIB and GTK. */

  visu_basic_init();

  /* the default is V_Sim with gtk interface. */
  if (!g_strcmp0(commandLineGet_windowMode(), "renderOnly"))
    visu_ui_mainCreate(visu_ui_createInterface);
  else
    visu_ui_mainCreate(visu_ui_main_class_createMain);
  window = VISU_UI_RENDERING_WINDOW(visu_ui_getRenderWidget());

  g_debug("Visu Main: test for help message.");
  if (visu_basic_showOptionHelp(FALSE))
    exit(0);

  newData = visu_data_loadable_new_fromCLI();
  if (newData)
    {
      dirname = g_path_get_dirname(visu_data_loadable_getFilename(newData, 0));
      normDir = tool_path_normalize(dirname);
      visu_ui_main_setLastOpenDirectory(visu_ui_main_class_getCurrentPanel(),
                                        (char*)normDir, VISU_UI_DIR_FILE);
      g_free(dirname);
      g_free(normDir);

      visu_ui_rendering_window_loadFile(window, newData, commandLineGet_iSet());

      /* Run the command line. */
      if (!g_strcmp0(commandLineGet_windowMode(), "renderOnly"))
	g_idle_add(visu_ui_runCommandLine, (gpointer)0);
      else
	{
	  g_idle_add(visu_ui_main_initPanels, visu_ui_main_class_getCurrentPanel());
	  g_idle_add(visu_ui_main_runCommandLine, (gpointer)visu_ui_getPanel());
	}
    }
  else
    {
      visu_gl_node_scene_setData(visu_ui_rendering_window_getGlScene(window), (VisuData*)0);

      /* Run the command line. */
      if (g_strcmp0(commandLineGet_windowMode(), "renderOnly"))
	g_idle_add(visu_ui_main_initPanels, visu_ui_main_class_getCurrentPanel());
    }

  writer = tool_writer_getStatic();

  g_debug("Visu Main: starting main GTK loop.");
  gtk_main();
  g_debug("Visu Main: quit main GTK loop.");

  g_object_unref(writer);
  
  visu_basic_freeAll();
  commandLineFree_all();

  return 0;
}
