/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2016)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2016)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef ELEMENT_SPIN_H
#define ELEMENT_SPIN_H

#include <glib-object.h>

#include "elementAtomic.h"

G_BEGIN_DECLS

/**
 * VisuElementSpinShapeId:
 * @VISU_ELEMENT_SPIN_ARROW_SMOOTH: the shape is smooth and rounded ;
 * @VISU_ELEMENT_SPIN_ARROW_SHARP: the shape is built on squares ;
 * @VISU_ELEMENT_SPIN_ELLIPSOID: the shape is an ellipsoid ;
 * @VISU_ELEMENT_SPIN_TORUS: the shape is a torus (direction of the arrow is
 * normal to the torus plane).
 * @VISU_ELEMENT_SPIN_N_SHAPES: private.
 *
 * An identifier for the different shapes to draw elements.
 */
typedef enum
  {
    VISU_ELEMENT_SPIN_ARROW_SMOOTH,
    VISU_ELEMENT_SPIN_ARROW_SHARP,
    VISU_ELEMENT_SPIN_ELLIPSOID,
    VISU_ELEMENT_SPIN_TORUS,
    /*< private >*/
    VISU_ELEMENT_SPIN_N_SHAPES
  } VisuElementSpinShapeId;


/**
 * VISU_TYPE_ELEMENT_SPIN:
 *
 * return the type of #VisuElementSpin.
 */
#define VISU_TYPE_ELEMENT_SPIN	     (visu_element_spin_get_type ())
/**
 * VISU_ELEMENT_SPIN:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuElementSpin type.
 */
#define VISU_ELEMENT_SPIN(obj)	        (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_ELEMENT_SPIN, VisuElementSpin))
/**
 * VISU_ELEMENT_SPIN_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuElementSpinClass.
 */
#define VISU_ELEMENT_SPIN_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_ELEMENT_SPIN, VisuElementSpinClass))
/**
 * VISU_IS_ELEMENT_SPIN:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuElementSpin object.
 */
#define VISU_IS_ELEMENT_SPIN(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_ELEMENT_SPIN))
/**
 * VISU_IS_ELEMENT_SPIN_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuElementSpinClass class.
 */
#define VISU_IS_ELEMENT_SPIN_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_ELEMENT_SPIN))
/**
 * VISU_ELEMENT_SPIN_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_ELEMENT_SPIN_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_ELEMENT_SPIN, VisuElementSpinClass))

typedef struct _VisuElementSpinClass VisuElementSpinClass;
typedef struct _VisuElementSpin VisuElementSpin;
typedef struct _VisuElementSpinPrivate VisuElementSpinPrivate;

/**
 * visu_element_spin_get_type:
 *
 * This method returns the type of #VisuElementSpin, use VISU_TYPE_ELEMENT_SPIN instead.
 *
 * Returns: the type of #VisuElementSpin.
 */
GType visu_element_spin_get_type(void);

/**
 * _VisuElementSpin:
 * @parent: parent.
 * @priv: private data.
 *
 * This structure describes a set of pairs.
 */

struct _VisuElementSpin
{
  VisuElementAtomic parent;

  VisuElementSpinPrivate *priv;
};

struct _VisuElementSpinClass
{
  VisuElementAtomicClass parent;
};

VisuElementSpin* visu_element_spin_new(VisuElement *element);

VisuElementSpin* visu_element_spin_getFromPool(VisuElement *element);
void visu_element_spin_bindToPool(VisuElementSpin *spin);

VisuElementSpinShapeId visu_element_spin_getShape(const VisuElementSpin *self);

const gchar ** visu_element_spin_getShapeNames(gboolean asLabel);

void visu_element_spin_pool_finalize(void);

G_END_DECLS

#endif
