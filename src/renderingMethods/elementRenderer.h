/* -*- mode: C; c-basic-offset: 2 -*- */
/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2016)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2016)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef ELEMENT_RENDERER_H
#define ELEMENT_RENDERER_H

#include <glib.h>
#include <glib-object.h>

#include <visu_data.h>
#include <visu_extension.h>
#include <extraFunctions/colorizer.h>
#include <openGLFunctions/view.h>
#include <coreTools/toolColor.h>

G_BEGIN_DECLS

/**
 * VisuElementRendererEffects:
 * @VISU_ELEMENT_RENDERER_NO_EFFECT: no effect (apply pristine element
 * color and material).
 * @VISU_ELEMENT_RENDERER_INVERT: invert colour.
 * @VISU_ELEMENT_RENDERER_HIGHLIGHT: highlight colour (same material).
 * @VISU_ELEMENT_RENDERER_HIGHLIGHT_SEMI: semi-transparent highlight
 * colour with neutral material.
 * @VISU_ELEMENT_RENDERER_DESATURATE: desaturate colour.
 * @VISU_ELEMENT_RENDERER_SATURATE: saturate colour.
 * @VISU_ELEMENT_RENDERER_DARKEN: darken colour.
 * @VISU_ELEMENT_RENDERER_LIGHTEN: lighten colour.
 * @VISU_ELEMENT_RENDERER_FLATTEN_DARK: render darker without light efect.
 * @VISU_ELEMENT_RENDERER_FLATTEN: render without light efect.
 * @VISU_ELEMENT_RENDERER_FLATTEN_LIGHT: render lighter without light efect.
 *
 * The rendering done by #VisuGlExtNodes can alter the color and
 * material of rendered nodes.
 *
 * Since: 3.7
 */
typedef enum
  {
    VISU_ELEMENT_RENDERER_NO_EFFECT,
    VISU_ELEMENT_RENDERER_INVERT,
    VISU_ELEMENT_RENDERER_HIGHLIGHT,
    VISU_ELEMENT_RENDERER_HIGHLIGHT_SEMI,
    VISU_ELEMENT_RENDERER_DESATURATE,
    VISU_ELEMENT_RENDERER_SATURATE,
    VISU_ELEMENT_RENDERER_DARKEN,
    VISU_ELEMENT_RENDERER_LIGHTEN,
    VISU_ELEMENT_RENDERER_FLATTEN_DARK,
    VISU_ELEMENT_RENDERER_FLATTEN,
    VISU_ELEMENT_RENDERER_FLATTEN_LIGHT
  } VisuElementRendererEffects;

/* ElementRenderer interface. */
#define VISU_TYPE_ELEMENT_RENDERER                (visu_element_renderer_get_type())
#define VISU_ELEMENT_RENDERER(obj)                (G_TYPE_CHECK_INSTANCE_CAST((obj), VISU_TYPE_ELEMENT_RENDERER, VisuElementRenderer))
#define VISU_ELEMENT_RENDERER_CLASS(klass)          (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_ELEMENT_RENDERER, VisuElementRendererClass))
#define VISU_IS_ELEMENT_RENDERER(obj)             (G_TYPE_CHECK_INSTANCE_TYPE((obj), VISU_TYPE_ELEMENT_RENDERER))
#define VISU_IS_ELEMENT_RENDERER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_ELEMENT_RENDERER))
#define VISU_ELEMENT_RENDERER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_ELEMENT_RENDERER, VisuElementRendererClass))

typedef struct _VisuElementRendererClass VisuElementRendererClass;
typedef struct _VisuElementRenderer VisuElementRenderer;
typedef struct _VisuElementRendererPrivate VisuElementRendererPrivate;

/**
 * visu_element_renderer_get_type:
 *
 * This method returns the type of #VisuElementRenderer, use VISU_TYPE_ELEMENT_RENDERER instead.
 *
 * Returns: the type of #VisuElementRenderer.
 */
GType visu_element_renderer_get_type(void);

struct _VisuElementRenderer
{
  VisuObject parent;

  VisuElementRendererPrivate *priv;
};

/**
 * VisuCommitVertices:
 * @vertices: (element-type float): vertices to be commited.
 * @id: a buffer id.
 * @rgba: the colour used for the vertices.
 * @material: the material for the vertices.
 * @data: some user defined data.
 *
 * Prototype of function used by the various #VisuElementRenderer objects
 * when they are drawing their shape.
 *
 * Since: 3.9
 */
typedef void (*VisuCommitVertices)(const GArray *vertices, guint id,
                                   const gfloat rgba[4], const gfloat material[5],
                                   gpointer data);

struct _VisuElementRendererClass
{
  VisuObjectClass parent;

  gfloat (*getExtent)(const VisuElementRenderer *element);

  void   (*getLayout)(const VisuElementRenderer *element,
                      guint id,
                      VisuGlExtPackingModels *packing,
                      VisuGlExtPrimitives *primitive);
  void   (*addVertices)(const VisuElementRenderer *element,
                        const VisuDataColorizer *colorizer,
                        const VisuData *dataObj, const VisuNode *node,
                        const gfloat xyz[3], gfloat scale,
                        GArray **vertices, VisuCommitVertices commitFunc, gpointer data);
};

VisuElement* visu_element_renderer_getElement(VisuElementRenderer *element);
const VisuElement* visu_element_renderer_getConstElement(const VisuElementRenderer *element);

const ToolColor* visu_element_renderer_getColor(const VisuElementRenderer *element);
gboolean visu_element_renderer_setColor(VisuElementRenderer* ele,
                                        const ToolColor *color);
gboolean visu_element_renderer_setRGBAValue(VisuElementRenderer* ele, gfloat value, guint id);

const gfloat* visu_element_renderer_getMaterial(const VisuElementRenderer *element);
void visu_element_renderer_copyMaterial(const VisuElementRenderer *element,
                                        gfloat material[TOOL_MATERIAL_N_VALUES]);
gboolean visu_element_renderer_setMaterial(VisuElementRenderer* ele,
                                           const gfloat material[TOOL_MATERIAL_N_VALUES]);
gboolean visu_element_renderer_setMaterialValue(VisuElementRenderer* ele,
                                                gfloat value, ToolMaterialIds id);

void visu_element_renderer_getColorization(const VisuElementRenderer *element,
                                           VisuElementRendererEffects effect,
                                           float rgba[4], float material[5]);
void visu_element_renderer_setGlView(VisuElementRenderer *element, VisuGlView *view);
VisuGlView* visu_element_renderer_getGlView(VisuElementRenderer *element);

guint visu_element_renderer_getVerticeBufferDim(const VisuElementRenderer *element);
void visu_element_renderer_getVerticeLayout(const VisuElementRenderer *element,
                                            guint id,
                                            VisuGlExtPackingModels *packing,
                                            VisuGlExtPrimitives *primitive);
void visu_element_renderer_addVertices(const VisuElementRenderer *element,
                                       const VisuDataColorizer *colorizer,
                                       const VisuData *dataObj, const VisuNode *node,
                                       GArray **vertices,
                                       VisuCommitVertices commitFunc, gpointer data);
void visu_element_renderer_addVerticesAt(const VisuElementRenderer *element,
                                         const gfloat xyz[3], gfloat scale,
                                         GArray **vertices,
                                         VisuCommitVertices commitFunc, gpointer data);

gfloat visu_element_renderer_getExtent(const VisuElementRenderer *element);
gboolean visu_element_renderer_featureMaterialCache(const VisuElementRenderer *element);

VisuElementRenderer* visu_element_renderer_getFromPool(VisuElement *element);
void visu_element_renderer_bindToPool(VisuElementRenderer *element);

void visu_element_renderer_pool_finalize(void);

G_END_DECLS

#endif
