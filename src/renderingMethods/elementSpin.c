/* -*- mode: C; c-basic-offset: 2 -*- */
/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2016-2021)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2016-2021)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "elementSpin.h"

#include <string.h>
#include <math.h>

#include <visu_configFile.h>
#include <visu_dataspin.h>
#include "spinMethod.h"
#include <coreTools/toolShape.h>

/**
 * SECTION:elementSpin
 * @short_description: a class implementing rendering for
 * #VisuDataSpin method.
 *
 * <para>This class implements the virtual method of
 * #VisuElementRenderer class to display nodes as 1D objects at a
 * given point in a given direction. The specific shape is defined by
 * #VisuElementSpinShapeId enumeration. The size and orientation of
 * the 1D object is given per node by a #VisuNodeValuesVector
 * property. </para>
 * <para>visu_element_spin_getFromPool() is a specific function to
 * associate a unique #VisuElementSpin to a given #VisuElement.</para>
 */

/**
 * VisuElementSpinClass:
 * @parent: its parent.
 *
 * Interface for class that can represent #VisuElement of the same
 * kind in the same way (usually spheres).
 *
 * Since: 3.8
 */

#define FLAG_RESOURCES_SPIN "spin_resources"
#define DESC_RESOURCES_SPIN "Global or element resource for rendering spin module"

#define FLAG_SPIN_SHAPE "spin_shape"
#define DESC_SPIN_SHAPE "shape used to render spin for one element."
static VisuElementSpinShapeId _shape = VISU_ELEMENT_SPIN_ARROW_SMOOTH;

/* Default values. */
#define SPIN_ELEMENT_HAT_RADIUS_DEFAULT  0.8
#define SPIN_ELEMENT_HAT_LENGTH_DEFAULT  2.0
#define SPIN_ELEMENT_TAIL_RADIUS_DEFAULT 0.33
#define SPIN_ELEMENT_TAIL_LENGTH_DEFAULT 0.8
#define SPIN_ELEMENT_HAT_COLOR_DEFAULT   FALSE
#define SPIN_ELEMENT_TAIL_COLOR_DEFAULT  FALSE

#define FLAG_SPIN_ARROW "spin_arrow"
#define DESC_SPIN_ARROW "arrow definition used to render spin for one element."
static gfloat _arrow[] = {SPIN_ELEMENT_HAT_LENGTH_DEFAULT, SPIN_ELEMENT_HAT_RADIUS_DEFAULT,
                          SPIN_ELEMENT_TAIL_LENGTH_DEFAULT, SPIN_ELEMENT_TAIL_RADIUS_DEFAULT,
                          SPIN_ELEMENT_HAT_COLOR_DEFAULT, SPIN_ELEMENT_TAIL_COLOR_DEFAULT};

#define SPIN_ELEMENT_AAXIS_DEFAULT       1.5
#define SPIN_ELEMENT_BAXIS_DEFAULT       0.6
#define SPIN_ELEMENT_ELIP_COLOR_DEFAULT  FALSE

#define FLAG_SPIN_ELIP "spin_elipsoid"
#define DESC_SPIN_ELIP "elipsoid definition used to render spin for one element."
static gfloat _elip[] = {SPIN_ELEMENT_AAXIS_DEFAULT, SPIN_ELEMENT_BAXIS_DEFAULT,
                         SPIN_ELEMENT_ELIP_COLOR_DEFAULT};

static const char* _shapeName[VISU_ELEMENT_SPIN_N_SHAPES + 1] =
  {"Rounded", "Edged", "Elipsoid", "Torus", (const char*)0};
static const char* _shapeNameI18n[VISU_ELEMENT_SPIN_N_SHAPES + 1] = {NULL};

/* Read routines for the config file. */

/**
 * VisuElementSpin:
 *
 * Structure used to define #VisuElementSpin objects.
 *
 * Since: 3.8
 */
struct _VisuElementSpinPrivate
{
  VisuElementAtomic *fromPool;

  /* Params for the arrow shapes. */
  /* The radius and the length of the hat. */
  float length, height;
  /* The radius and the length of the tail. */
  float u_length, u_height;
  /* The coloring pattern. */
  gboolean use_element_color, use_element_color_hat;

  /* Params for the elipsoid shapes. */
  /* The long and the short axis. */
  float aAxis, bAxis;
  /* The coloring pattern. */
  gboolean elipsoidColor;

  /* The shape used. */
  VisuElementSpinShapeId shape;
};

enum {
  PROP_0,
  PROP_H_LENGTH,
  PROP_H_RADIUS,
  PROP_T_LENGTH,
  PROP_T_RADIUS,
  PROP_H_USE,
  PROP_T_USE,
  PROP_A,
  PROP_B,
  PROP_USE,
  PROP_SHAPE,
  N_PROPS,
  PROP_CACHE,
  N_PROP_TOT
};
static GParamSpec *_properties[N_PROP_TOT];

static gboolean _shapeFromName(const gchar *name, VisuElementSpinShapeId *shape);

static GObject* visu_element_spin_constructor(GType gtype, guint nprops, GObjectConstructParam *props);
static void visu_element_spin_get_property(GObject* obj, guint property_id,
                                             GValue *value, GParamSpec *pspec);
static void visu_element_spin_set_property(GObject* obj, guint property_id,
                                             const GValue *value, GParamSpec *pspec);
static void _getLayout  (const VisuElementRenderer *element,
                         guint id,
                         VisuGlExtPackingModels *packing,
                         VisuGlExtPrimitives *primitive);
static void _addVertices(const VisuElementRenderer *element,
                         const VisuDataColorizer *colorizer,
                         const VisuData *dataobj, const VisuNode *node,
                         const gfloat xyz[3], gfloat scale,
                         GArray **vertices,
                         VisuCommitVertices commitFunc, gpointer data);

static gfloat _getExtent(const VisuElementRenderer *self);

static void onEntryShape(VisuConfigFile *obj, VisuConfigFileEntry *entry, gpointer data);
static void onEntryArrow(VisuConfigFile *obj, VisuConfigFileEntry *entry, gpointer data);
static void onEntryElip(VisuConfigFile *obj, VisuConfigFileEntry *entry, gpointer data);
static void exportSpin(GString *data, VisuData *dataObj);

static GList *_pool;

G_DEFINE_TYPE_WITH_CODE(VisuElementSpin, visu_element_spin, VISU_TYPE_ELEMENT_ATOMIC,
                        G_ADD_PRIVATE(VisuElementSpin))

static void visu_element_spin_class_init(VisuElementSpinClass *klass)
{
  VisuConfigFileEntry *resourceEntry;
  gfloat rgArrow[2] = {0.f, 999.f};

  /* Connect the overloading methods. */
  G_OBJECT_CLASS(klass)->constructor  = visu_element_spin_constructor;
  G_OBJECT_CLASS(klass)->set_property = visu_element_spin_set_property;
  G_OBJECT_CLASS(klass)->get_property = visu_element_spin_get_property;
  VISU_ELEMENT_RENDERER_CLASS(klass)->getExtent   = _getExtent;
  VISU_ELEMENT_RENDERER_CLASS(klass)->getLayout   = _getLayout;
  VISU_ELEMENT_RENDERER_CLASS(klass)->addVertices = _addVertices;

  /**
   * VisuElementSpin::spin-shape:
   *
   * The shape used to represent a given element.
   *
   * Since: 3.8
   */
  _properties[PROP_SHAPE] =
    g_param_spec_uint("spin-shape", "Spin shape", "spin shape",
                      0, VISU_ELEMENT_SPIN_N_SHAPES - 1, _shape,
                      G_PARAM_READWRITE);
  /**
   * VisuElementSpin::hat-length:
   *
   * .
   *
   * Since: 3.8
   */
  _properties[PROP_H_LENGTH] =
    g_param_spec_float("hat-length", "Hat length", "hat length",
                       0.f, G_MAXFLOAT, SPIN_ELEMENT_HAT_LENGTH_DEFAULT, G_PARAM_READWRITE);
  /**
   * VisuElementSpin::hat-radius:
   *
   * .
   *
   * Since: 3.8
   */
  _properties[PROP_H_RADIUS] =
    g_param_spec_float("hat-radius", "Hat radius", "hat radius",
                       0.f, G_MAXFLOAT, SPIN_ELEMENT_HAT_RADIUS_DEFAULT, G_PARAM_READWRITE);
  /**
   * VisuElementSpin::tail-length:
   *
   * .
   *
   * Since: 3.8
   */
  _properties[PROP_T_LENGTH] =
    g_param_spec_float("tail-length", "Tail length", "tail length",
                       0.f, G_MAXFLOAT, SPIN_ELEMENT_TAIL_LENGTH_DEFAULT, G_PARAM_READWRITE);
  /**
   * VisuElementSpin::tail-radius:
   *
   * .
   *
   * Since: 3.8
   */
  _properties[PROP_T_RADIUS] =
    g_param_spec_float("tail-radius", "Tail radius", "tail radius",
                       0.f, G_MAXFLOAT, SPIN_ELEMENT_TAIL_RADIUS_DEFAULT, G_PARAM_READWRITE);
  /**
   * VisuElementSpin::hat-spin-colored:
   *
   * .
   *
   * Since: 3.8
   */
  _properties[PROP_H_USE] =
    g_param_spec_boolean("hat-spin-colored", "Hat spin colored", "hat is colored by spin",
                         SPIN_ELEMENT_HAT_COLOR_DEFAULT, G_PARAM_READWRITE);
  /**
   * VisuElementSpin::tail-spin-colored:
   *
   * .
   *
   * Since: 3.8
   */
  _properties[PROP_T_USE] =
    g_param_spec_boolean("tail-spin-colored", "Tail spin colored", "tail is colored by spin",
                         SPIN_ELEMENT_TAIL_COLOR_DEFAULT, G_PARAM_READWRITE);
  /**
   * VisuElementSpin::a-axis:
   *
   * .
   *
   * Since: 3.8
   */
  _properties[PROP_A] =
    g_param_spec_float("a-axis", "A axis", "A axis length",
                       0.f, G_MAXFLOAT, SPIN_ELEMENT_AAXIS_DEFAULT, G_PARAM_READWRITE);
  /**
   * VisuElementSpin::b-axis:
   *
   * .
   *
   * Since: 3.8
   */
  _properties[PROP_B] =
    g_param_spec_float("b-axis", "B axis", "B axis length",
                       0.f, G_MAXFLOAT, SPIN_ELEMENT_BAXIS_DEFAULT, G_PARAM_READWRITE);
  /**
   * VisuElementSpin::spin-colored:
   *
   * .
   *
   * Since: 3.8
   */
  _properties[PROP_USE] =
    g_param_spec_boolean("spin-colored", "Spin colored", "shape is colored by spin",
                         SPIN_ELEMENT_ELIP_COLOR_DEFAULT, G_PARAM_READWRITE);

  g_object_class_install_properties(G_OBJECT_CLASS(klass), N_PROPS, _properties);

  g_object_class_override_property(G_OBJECT_CLASS(klass),
                                   PROP_CACHE, "cache-material");

  /* Dealing with config files. */
  resourceEntry = visu_config_file_addEnumEntry(VISU_CONFIG_FILE_RESOURCE,
                                                FLAG_SPIN_SHAPE, DESC_SPIN_SHAPE,
                                                &_shape, _shapeFromName, TRUE);
  visu_config_file_entry_setVersion(resourceEntry, 3.8f);
  g_signal_connect(VISU_CONFIG_FILE_RESOURCE, "parsed::" FLAG_SPIN_SHAPE,
                   G_CALLBACK(onEntryShape), (gpointer)0);
  resourceEntry = visu_config_file_addFloatArrayEntry(VISU_CONFIG_FILE_RESOURCE,
                                                      FLAG_SPIN_ARROW, DESC_SPIN_ARROW,
                                                      6, _arrow, rgArrow, TRUE);
  visu_config_file_entry_setVersion(resourceEntry, 3.8f);
  g_signal_connect(VISU_CONFIG_FILE_RESOURCE, "parsed::" FLAG_SPIN_ARROW,
                   G_CALLBACK(onEntryArrow), (gpointer)0);
  resourceEntry = visu_config_file_addFloatArrayEntry(VISU_CONFIG_FILE_RESOURCE,
                                                      FLAG_SPIN_ELIP, DESC_SPIN_ELIP,
                                                      3, _elip, rgArrow, TRUE);
  visu_config_file_entry_setVersion(resourceEntry, 3.8f);
  g_signal_connect(VISU_CONFIG_FILE_RESOURCE, "parsed::" FLAG_SPIN_ELIP,
                   G_CALLBACK(onEntryElip), (gpointer)0);
  visu_config_file_addExportFunction(VISU_CONFIG_FILE_RESOURCE,
                                     exportSpin);

  _pool = (GList*)0;
}
static GObject* visu_element_spin_constructor(GType gtype, guint nprops, GObjectConstructParam *props)
{
  guint i;

  for (i = 0; i < nprops; ++i)
    {
      if (!g_strcmp0(g_param_spec_get_name(props[i].pspec), "buffer-dimension"))
        g_value_set_uint(props[i].value, 2);
    }

  return G_OBJECT_CLASS(visu_element_spin_parent_class)->constructor(gtype, nprops, props);
}
static void visu_element_spin_init(VisuElementSpin *obj)
{
  g_debug("Visu Element Spin: initializing a new object (%p).",
	      (gpointer)obj);
  
  obj->priv = visu_element_spin_get_instance_private(obj);

  /* Private data. */
  obj->priv->fromPool = (VisuElementAtomic*)0;
  obj->priv->length = _arrow[1];
  obj->priv->height = _arrow[0];
  obj->priv->u_length = _arrow[3];
  obj->priv->u_height = _arrow[2];
  obj->priv->use_element_color = (_arrow[4] == TRUE);
  obj->priv->use_element_color_hat = (_arrow[5] == TRUE);
  obj->priv->aAxis = SPIN_ELEMENT_AAXIS_DEFAULT;
  obj->priv->bAxis = SPIN_ELEMENT_BAXIS_DEFAULT;
  obj->priv->elipsoidColor = SPIN_ELEMENT_ELIP_COLOR_DEFAULT;
  obj->priv->shape = _shape;
}
static void visu_element_spin_get_property(GObject* obj, guint property_id,
                                             GValue *value, GParamSpec *pspec)
{
  VisuElementSpin *self = VISU_ELEMENT_SPIN(obj);

  g_debug("Visu Element Spin: get property '%s'",
	      g_param_spec_get_name(pspec));
  switch (property_id)
    {
    case PROP_H_LENGTH:
      g_value_set_float(value, self->priv->height);
      break;
    case PROP_H_RADIUS:
      g_value_set_float(value, self->priv->length);
      break;
    case PROP_T_LENGTH:
      g_value_set_float(value, self->priv->u_height);
      break;
    case PROP_T_RADIUS:
      g_value_set_float(value, self->priv->u_length);
      break;
    case PROP_T_USE:
      g_value_set_boolean(value, self->priv->use_element_color);
      break;
    case PROP_H_USE:
      g_value_set_boolean(value, self->priv->use_element_color_hat);
      break;
    case PROP_A:
      g_value_set_float(value, self->priv->aAxis);
      break;
    case PROP_B:
      g_value_set_float(value, self->priv->bAxis);
      break;
    case PROP_USE:
      g_value_set_boolean(value, self->priv->elipsoidColor);
      break;
    case PROP_SHAPE:
      g_value_set_uint(value, self->priv->shape);
      break;
    case PROP_CACHE:
      g_value_set_boolean(value, FALSE);
      break;
    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
      break;
    }
}
static void visu_element_spin_set_property(GObject* obj, guint property_id,
                                             const GValue *value, GParamSpec *pspec)
{
  VisuElementSpin *self = VISU_ELEMENT_SPIN(obj);

  g_debug("Visu Element Spin: set property '%s'",
	      g_param_spec_get_name(pspec));
  switch (property_id)
    {
    case PROP_H_LENGTH:
      self->priv->height = g_value_get_float(value);
      if (self->priv->shape == VISU_ELEMENT_SPIN_ARROW_SMOOTH ||
          self->priv->shape == VISU_ELEMENT_SPIN_ARROW_SHARP)
        g_signal_emit_by_name(self, "size-changed", _getExtent(VISU_ELEMENT_RENDERER(self)));
      break;
    case PROP_H_RADIUS:
      self->priv->length = g_value_get_float(value);
      if (self->priv->shape == VISU_ELEMENT_SPIN_ARROW_SMOOTH ||
          self->priv->shape == VISU_ELEMENT_SPIN_ARROW_SHARP)
        g_signal_emit_by_name(self, "size-changed", _getExtent(VISU_ELEMENT_RENDERER(self)));
      break;
    case PROP_T_LENGTH:
      self->priv->u_height = g_value_get_float(value);
      if (self->priv->shape == VISU_ELEMENT_SPIN_ARROW_SMOOTH ||
          self->priv->shape == VISU_ELEMENT_SPIN_ARROW_SHARP)
        g_signal_emit_by_name(self, "size-changed", _getExtent(VISU_ELEMENT_RENDERER(self)));
      break;
    case PROP_T_RADIUS:
      self->priv->u_length = g_value_get_float(value);
      if (self->priv->shape == VISU_ELEMENT_SPIN_ARROW_SMOOTH ||
          self->priv->shape == VISU_ELEMENT_SPIN_ARROW_SHARP)
        g_signal_emit_by_name(self, "size-changed", _getExtent(VISU_ELEMENT_RENDERER(self)));
      break;
    case PROP_T_USE:
      self->priv->use_element_color = g_value_get_boolean(value);
      if (self->priv->shape == VISU_ELEMENT_SPIN_ARROW_SMOOTH ||
          self->priv->shape == VISU_ELEMENT_SPIN_ARROW_SHARP)
        g_signal_emit_by_name(self, "size-changed", _getExtent(VISU_ELEMENT_RENDERER(self)));
      break;
    case PROP_H_USE:
      self->priv->use_element_color_hat = g_value_get_boolean(value);
      if (self->priv->shape == VISU_ELEMENT_SPIN_ARROW_SMOOTH ||
          self->priv->shape == VISU_ELEMENT_SPIN_ARROW_SHARP)
        g_signal_emit_by_name(self, "size-changed", _getExtent(VISU_ELEMENT_RENDERER(self)));
      break;
    case PROP_A:
      self->priv->aAxis = g_value_get_float(value);
      if (self->priv->shape == VISU_ELEMENT_SPIN_ELLIPSOID ||
          self->priv->shape == VISU_ELEMENT_SPIN_TORUS)
        g_signal_emit_by_name(self, "size-changed", _getExtent(VISU_ELEMENT_RENDERER(self)));
      break;
    case PROP_B:
      self->priv->bAxis = g_value_get_float(value);
      if (self->priv->shape == VISU_ELEMENT_SPIN_ELLIPSOID ||
          self->priv->shape == VISU_ELEMENT_SPIN_TORUS)
        g_signal_emit_by_name(self, "size-changed", _getExtent(VISU_ELEMENT_RENDERER(self)));
      break;
    case PROP_USE:
      self->priv->elipsoidColor = g_value_get_boolean(value);
      if (self->priv->shape == VISU_ELEMENT_SPIN_ELLIPSOID ||
          self->priv->shape == VISU_ELEMENT_SPIN_TORUS)
        g_signal_emit_by_name(self, "size-changed", _getExtent(VISU_ELEMENT_RENDERER(self)));
      break;
    case PROP_SHAPE:
      self->priv->shape = g_value_get_uint(value);
      g_signal_emit_by_name(self, "size-changed", _getExtent(VISU_ELEMENT_RENDERER(self)));
      break;
    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
      break;
    }
}

/**
 * visu_element_spin_new:
 * @element: a #VisuElement object.
 *
 * Creates a new #VisuElementSpin object to draw @element.
 *
 * Since: 3.8
 *
 * Returns: (transfer full): a newly created #VisuElementSpin object.
 **/
VisuElementSpin* visu_element_spin_new(VisuElement *element)
{
  return VISU_ELEMENT_SPIN(g_object_new(VISU_TYPE_ELEMENT_SPIN, "element", element, NULL));
}
/**
 * visu_element_spin_getFromPool:
 * @element: a #VisuElement object.
 *
 * Retrieve a #VisuElementSpin representing @element. This
 * #VisuElementSpin is unique and its parent properties are bound to
 * the unique #VisuElementAtomic for @element.
 *
 * Since: 3.8
 *
 * Returns: (transfer none): a #VisuElementSpin for @element.
 **/
VisuElementSpin* visu_element_spin_getFromPool(VisuElement *element)
{
  GList *lst;
  VisuElementSpin *spin;

  for (lst = _pool; lst; lst = g_list_next(lst))
    if (visu_element_renderer_getElement(VISU_ELEMENT_RENDERER(lst->data)) == element)
      return VISU_ELEMENT_SPIN(lst->data);
  
  spin = visu_element_spin_new(element);
  visu_element_atomic_bindToPool(VISU_ELEMENT_ATOMIC(spin));
  _pool = g_list_prepend(_pool, spin);  
  return spin;
}
/**
 * visu_element_spin_bindToPool:
 * @spin: A #VisuElementSpin object.
 *
 * Bind all properties of @spin to the properties of the
 * #VisuElementSpin object from the pool used for the same #VisuElement.
 *
 * Since: 3.8
 **/
void visu_element_spin_bindToPool(VisuElementSpin *spin)
{
  VisuElementSpin *pool;

  visu_element_atomic_bindToPool(VISU_ELEMENT_ATOMIC(spin));
  pool = visu_element_spin_getFromPool(visu_element_renderer_getElement(VISU_ELEMENT_RENDERER(spin)));
  g_object_bind_property(pool, "spin-shape", spin, "spin-shape",
                         G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);
  g_object_bind_property(pool, "hat-length", spin, "hat-length",
                         G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);
  g_object_bind_property(pool, "hat-radius", spin, "hat-radius",
                         G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);
  g_object_bind_property(pool, "tail-length", spin, "tail-length",
                         G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);
  g_object_bind_property(pool, "tail-radius", spin, "tail-radius",
                         G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);
  g_object_bind_property(pool, "hat-spin-colored", spin, "hat-spin-colored",
                         G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);
  g_object_bind_property(pool, "tail-spin-colored", spin, "tail-spin-colored",
                         G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);
  g_object_bind_property(pool, "a-axis", spin, "a-axis",
                         G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);
  g_object_bind_property(pool, "b-axis", spin, "b-axis",
                         G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);
  g_object_bind_property(pool, "spin-colored", spin, "spin-colored",
                         G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);
}

/**
 * visu_element_spin_getShape:
 * @self: a #VisuElementSpin object.
 *
 * Retrieves the shape of @self.
 *
 * Since: 3.8
 *
 * Returns: a #VisuElementSpinShapeId value.
 **/
VisuElementSpinShapeId visu_element_spin_getShape(const VisuElementSpin *self)
{
  g_return_val_if_fail(VISU_IS_ELEMENT_SPIN(self), _shape);

  return self->priv->shape;
}

static gfloat _getExtent(const VisuElementRenderer *self)
{
  VisuElementSpin *spin;

  g_return_val_if_fail(VISU_IS_ELEMENT_SPIN(self), 0.f);

  spin = VISU_ELEMENT_SPIN(self);
  
  if (spin->priv->shape == VISU_ELEMENT_SPIN_ARROW_SMOOTH ||
      spin->priv->shape == VISU_ELEMENT_SPIN_ARROW_SHARP)
    return MAX(VISU_ELEMENT_RENDERER_CLASS(visu_element_spin_parent_class)->getExtent(self),
               (spin->priv->height + spin->priv->u_height) / 2.f);
  else if (spin->priv->shape == VISU_ELEMENT_SPIN_ELLIPSOID)
    return MAX(VISU_ELEMENT_RENDERER_CLASS(visu_element_spin_parent_class)->getExtent(self),
               MAX(spin->priv->aAxis, spin->priv->bAxis) / 2.f);
  else
    return MAX(VISU_ELEMENT_RENDERER_CLASS(visu_element_spin_parent_class)->getExtent(self),
               (spin->priv->aAxis - spin->priv->bAxis * spin->priv->bAxis / spin->priv->aAxis) / 2.f);
}
static void _getLayout(const VisuElementRenderer *element,
                       guint id,
                       VisuGlExtPackingModels *packing,
                       VisuGlExtPrimitives *primitive)
{
  g_return_if_fail(VISU_IS_ELEMENT_SPIN(element));

  if (id)
    {
      *packing = VISU_GL_XYZ_NRM;
      *primitive = VISU_GL_TRIANGLES;
    }
  else
    VISU_ELEMENT_RENDERER_CLASS(visu_element_spin_parent_class)->getLayout(element, id, packing, primitive);
}

static void _addVertices(const VisuElementRenderer *element,
                         const VisuDataColorizer *colorizer,
                         const VisuData *dataObj, const VisuNode *node,
                         const gfloat xyz[3], gfloat scale,
                         GArray **vertices,
                         VisuCommitVertices commitFunc, gpointer data)

{
  VisuElementSpin *self = VISU_ELEMENT_SPIN(element);

  const gfloat *dxyz;
  gfloat rgba[4];
  float ratio;
  gboolean withAtomic;

  dxyz = visu_method_spin_getSpinVector(visu_method_spin_getDefault(),
                                        VISU_DATA_SPIN(dataObj), node, &ratio,
                                        rgba, &withAtomic);
  if (dxyz && vertices[1])
    {
      gfloat xyz1[3], xyz2[3], hratio, fact;
      const gfloat *material;
      const ToolColor *color;
      const gfloat *spinValues = dxyz + 3;
      const VisuGlView *view;
      gint nlat;

      color = visu_element_renderer_getColor(element);
      rgba[3] = color->rgba[3];
      material = visu_element_renderer_getMaterial(element);
      if (colorizer)
        visu_data_colorizer_getColor(colorizer, rgba, dataObj, node);
      view = visu_element_renderer_getGlView(VISU_ELEMENT_RENDERER(element));

      switch (self->priv->shape)
        {
        case VISU_ELEMENT_SPIN_ARROW_SMOOTH:
        case VISU_ELEMENT_SPIN_ARROW_SHARP:
          fact = (self->priv->u_height + self->priv->height) / spinValues[TOOL_MATRIX_SPHERICAL_MODULUS] * scale * ratio;
          hratio = 1.f / (1.f + self->priv->u_height / self->priv->height);
          xyz1[0] = xyz[0] - dxyz[0] * 0.5f * fact; 
          xyz1[1] = xyz[1] - dxyz[1] * 0.5f * fact;
          xyz1[2] = xyz[2] - dxyz[2] * 0.5f * fact;
          xyz2[0] = xyz[0] + dxyz[0] * (0.5f - hratio) * fact;
          xyz2[1] = xyz[1] + dxyz[1] * (0.5f - hratio) * fact;
          xyz2[2] = xyz[2] + dxyz[2] * (0.5f - hratio) * fact;
          if (self->priv->shape == VISU_ELEMENT_SPIN_ARROW_SMOOTH)
            nlat = visu_gl_view_getDetailLevel(view, self->priv->u_length * ratio * scale);
          else
            nlat = 4;
          tool_drawCylinder(vertices[1], xyz1, xyz2, self->priv->u_length * scale * ratio, nlat, TRUE);
          if (self->priv->use_element_color)
            commitFunc(vertices[1], 1, color->rgba, material, data);
          else
            commitFunc(vertices[1], 1, rgba, material, data);
          xyz1[0] = xyz[0] + dxyz[0] * 0.5f * fact; 
          xyz1[1] = xyz[1] + dxyz[1] * 0.5f * fact;
          xyz1[2] = xyz[2] + dxyz[2] * 0.5f * fact;
          if (self->priv->shape == VISU_ELEMENT_SPIN_ARROW_SMOOTH)
            nlat = visu_gl_view_getDetailLevel(view, self->priv->length * ratio * scale);
          else
            nlat = 4;
          tool_drawCone(vertices[1], xyz2, xyz1, self->priv->length * scale * ratio, nlat, TRUE);
          if (self->priv->use_element_color_hat)
            commitFunc(vertices[1], 1, color->rgba, material, data);
          else
            commitFunc(vertices[1], 1, rgba, material, data);
          break;
        case VISU_ELEMENT_SPIN_ELLIPSOID:
          nlat = visu_gl_view_getDetailLevel(view, self->priv->bAxis * ratio * scale);
          tool_drawEllipsoid(vertices[1], xyz,
                             spinValues[TOOL_MATRIX_SPHERICAL_THETA] * G_PI / 180.f,
                             spinValues[TOOL_MATRIX_SPHERICAL_PHI] * G_PI / 180.f,
                             scale * ratio * self->priv->aAxis,
                             self->priv->aAxis / self->priv->bAxis, nlat);
          if (self->priv->elipsoidColor)
            commitFunc(vertices[1], 1, color->rgba, material, data);
          else
            commitFunc(vertices[1], 1, rgba, material, data);
          break;
        case VISU_ELEMENT_SPIN_TORUS:
          nlat = visu_gl_view_getDetailLevel(view, self->priv->bAxis);
          tool_drawTorus(vertices[1], xyz,
                         spinValues[TOOL_MATRIX_SPHERICAL_THETA] * G_PI / 180.f,
                         spinValues[TOOL_MATRIX_SPHERICAL_PHI] * G_PI / 180.f,
                         scale * ratio * self->priv->aAxis,
                         self->priv->aAxis / self->priv->bAxis, nlat);
          if (self->priv->elipsoidColor)
            commitFunc(vertices[1], 1, color->rgba, material, data);
          else
            commitFunc(vertices[1], 1, rgba, material, data);
          break;
        default:
          g_return_if_reached();
        }
    }
  if (withAtomic && vertices[0])
    VISU_ELEMENT_RENDERER_CLASS(visu_element_spin_parent_class)->addVertices
      (element, colorizer, dataObj, node, xyz, scale, vertices, commitFunc, data);
}

/**
 * visu_element_spin_getShapeNames:
 * @asLabel: a boolean.
 *
 * Get the string defining #VisuElementSpinShapeId. If @asLabel is
 * %TRUE, then the string are translated and stored in UTF8.
 *
 * Since: 3.8
 *
 * Returns: (transfer none) (array zero-terminated=1): strings
 * representing #VisuElementSpinShapeId.
 **/
const gchar ** visu_element_spin_getShapeNames(gboolean asLabel)
{
  if (!_shapeNameI18n[0])
    {
      _shapeNameI18n[0] = _("Rounded arrow");
      _shapeNameI18n[1] = _("Edged arrow");
      _shapeNameI18n[2] = _("Elipsoid");
      _shapeNameI18n[3] = _("Torus");
      _shapeNameI18n[4] = (const char*)0;
    }
  if (asLabel)
    return _shapeNameI18n;
  else
    return _shapeName;
}

/*****************************************/
/* Dealing with parameters and resources */
/*****************************************/
static gboolean _shapeFromName(const gchar *name, VisuElementSpinShapeId *shape)
{
  g_return_val_if_fail(name && shape, FALSE);

  for (*shape = 0; *shape < VISU_ELEMENT_SPIN_N_SHAPES; *shape += 1)
    if (!g_strcmp0(name, _shapeName[*shape]))
      return TRUE;
  return FALSE;
}
static VisuElementSpin* _fromEntry(VisuConfigFileEntry *entry)
{
  const gchar *label;
  VisuElement *ele;

  label = visu_config_file_entry_getLabel(entry);
  ele = visu_element_retrieveFromName(label, (gboolean*)0);
  if (!ele)
    {
      visu_config_file_entry_setErrorMessage(entry, _("'%s' wrong element name"), label);
      return (VisuElementSpin*)0;
    }
  return visu_element_spin_getFromPool(ele);
}
static void onEntryShape(VisuConfigFile *obj _U_, VisuConfigFileEntry *entry, gpointer data _U_)
{
  VisuElementSpin *ele;

  ele = _fromEntry(entry);
  if (!ele)
    return;

  g_object_set(ele, "shape", _shape, NULL);
}
static void onEntryArrow(VisuConfigFile *obj _U_, VisuConfigFileEntry *entry, gpointer data _U_)
{
  VisuElementSpin *ele;

  ele = _fromEntry(entry);
  if (!ele)
    return;

  g_object_set(ele, "hat-length", _arrow[0], "hat-radius", _arrow[1],
               "tail-length", _arrow[2], "tail-radius", _arrow[3],
               "hat-spin-colored", (_arrow[4] == TRUE),
               "tail-spin-colored", (_arrow[5] == TRUE), NULL);
}
static void onEntryElip(VisuConfigFile *obj _U_, VisuConfigFileEntry *entry, gpointer data _U_)
{
  VisuElementSpin *ele;

  ele = _fromEntry(entry);
  if (!ele)
    return;

  g_object_set(ele, "a-axis", _elip[0], "b-axis", _elip[1],
               "spin-colored", (_elip[2] == TRUE), NULL);
}
/* These functions write all the element list to export there associated resources. */
static void exportSpin(GString *data, VisuData* dataObj)
{
  GList *pos;
  VisuElementSpin *spin;
  const VisuElement *ele;

  /* If dataObj is given and the rendering method is not spin,
     we return. */
  if (dataObj && VISU_IS_DATA_SPIN(dataObj))
    return;

  g_debug("Rendering Spin: exporting element resources...");

  visu_config_file_exportComment(data, DESC_RESOURCES_SPIN);
  /* We create a list of elements, or get the whole list. */
  for (pos = _pool; pos; pos = g_list_next(pos))
    {
      spin = VISU_ELEMENT_SPIN(pos->data);
      ele = visu_element_renderer_getConstElement(VISU_ELEMENT_RENDERER(spin));
      if (!dataObj || visu_node_array_containsElement(VISU_NODE_ARRAY(dataObj), ele))
        {
          if (spin->priv->shape != _shape)
            visu_config_file_exportEntry(data, FLAG_SPIN_SHAPE, visu_element_getName(ele),
                                         "%s", _shapeName[spin->priv->shape]);
          if (ABS(spin->priv->height - SPIN_ELEMENT_HAT_LENGTH_DEFAULT) > 1e-6 ||
              ABS(spin->priv->u_height - SPIN_ELEMENT_TAIL_LENGTH_DEFAULT) > 1e-6 ||
              ABS(spin->priv->length - SPIN_ELEMENT_HAT_RADIUS_DEFAULT) > 1e-6 ||
              ABS(spin->priv->u_length - SPIN_ELEMENT_TAIL_RADIUS_DEFAULT) > 1e-6 ||
              spin->priv->use_element_color != SPIN_ELEMENT_TAIL_COLOR_DEFAULT ||
              spin->priv->use_element_color_hat != SPIN_ELEMENT_HAT_COLOR_DEFAULT)
            visu_config_file_exportEntry(data, FLAG_SPIN_ARROW, visu_element_getName(ele),
                                         "%f %f %f %f %d %d",
                                         spin->priv->height, spin->priv->u_height, spin->priv->length, spin->priv->u_length,
                                         spin->priv->use_element_color, spin->priv->use_element_color_hat);
          if (ABS(spin->priv->aAxis - SPIN_ELEMENT_AAXIS_DEFAULT) > 1e-6 ||
              ABS(spin->priv->bAxis - SPIN_ELEMENT_BAXIS_DEFAULT) > 1e-6 ||
              spin->priv->elipsoidColor != SPIN_ELEMENT_ELIP_COLOR_DEFAULT)
            visu_config_file_exportEntry(data, FLAG_SPIN_ELIP, visu_element_getName(ele),
                                         "%f %f %d",
                                         spin->priv->aAxis, spin->priv->bAxis, spin->priv->elipsoidColor);
        }
    }

  visu_config_file_exportComment(data, "");
}
/**
 * visu_element_spin_pool_finalize: (skip)
 *
 * Destroy the internal list of known #VisuElementSpin objects, see
 * visu_element_spin_getFromPool().
 *
 * Since: 3.8
 **/
void visu_element_spin_pool_finalize(void)
{
  g_list_free_full(_pool, (GDestroyNotify)g_object_unref);
  _pool = (GList*)0;
}
