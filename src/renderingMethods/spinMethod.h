/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2017)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2017)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef VISU_METHODSPIN_H
#define VISU_METHODSPIN_H

#include <glib.h>
#include <glib-object.h>

#include <visu_tools.h>
#include <visu_dataspin.h>

G_BEGIN_DECLS

/**
 * VISU_TYPE_METHOD_SPIN:
 *
 * return the type of #VisuMethodSpin.
 */
#define VISU_TYPE_METHOD_SPIN	     (visu_method_spin_get_type ())
/**
 * VISU_METHOD_SPIN:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuMethodSpin type.
 */
#define VISU_METHOD_SPIN(obj)	     (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_METHOD_SPIN, VisuMethodSpin))
/**
 * VISU_METHOD_SPIN_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuMethodSpinClass.
 */
#define VISU_METHOD_SPIN_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_METHOD_SPIN, VisuMethodSpinClass))
/**
 * VISU_IS_METHOD_SPIN:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuMethodSpin object.
 */
#define VISU_IS_METHOD_SPIN(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_METHOD_SPIN))
/**
 * VISU_IS_METHOD_SPIN_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuMethodSpinClass class.
 */
#define VISU_IS_METHOD_SPIN_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_METHOD_SPIN))
/**
 * VISU_METHOD_SPIN_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_METHOD_SPIN_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_METHOD_SPIN, VisuMethodSpinClass))

typedef struct _VisuMethodSpinPrivate VisuMethodSpinPrivate;
typedef struct _VisuMethodSpin VisuMethodSpin;

/**
 * VisuMethodSpin:
 *
 * Structure used to define #VisuMethodSpin objects.
 *
 * Since: 3.8
 */
struct _VisuMethodSpin
{
  VisuObject parent;

  VisuMethodSpinPrivate *priv;
};

/**
 * VisuMethodSpinClass:
 * @parent: the parent class.
 *
 * A short way to identify #_VisuMethodSpinClass structure.
 */
typedef struct _VisuMethodSpinClass VisuMethodSpinClass;
struct _VisuMethodSpinClass
{
  VisuObjectClass parent;
};

/**
 * visu_method_spin_get_type:
 *
 * This method returns the type of #VisuMethodSpin, use VISU_TYPE_METHOD_SPIN instead.
 *
 * Returns: the type of #VisuMethodSpin.
 */
GType visu_method_spin_get_type(void);

/**
 * VisuMethodSpinDrawingPolicy:
 * @VISU_METHOD_SPIN_ALWAYS: Arrows are drawn whatever the modulus value.
 * @VISU_METHOD_SPIN_HIDE_NULL: Spin with a null modulus are hidden.
 * @VISU_METHOD_SPIN_ATOMIC_NULL: Follow atomic method for null modulus.
 * @VISU_METHOD_SPIN_N_MODES: a flag to count the number of modes.
 *
 * Different policy to render the spin when the modulus is null. This
 * policy is applied for all #VisuElement.
 */
typedef enum
  {
    VISU_METHOD_SPIN_ALWAYS,    
    VISU_METHOD_SPIN_HIDE_NULL,  
    VISU_METHOD_SPIN_ATOMIC_NULL,
    VISU_METHOD_SPIN_N_MODES
  } VisuMethodSpinDrawingPolicy;
/**
 * VisuMethodSpinModulusPolicy:
 * @VISU_METHOD_SPIN_CONSTANT: arrows have all the same size, whatever the
 * modulus value.
 * @VISU_METHOD_SPIN_PER_TYPE: arrows are scaled per node type.
 * @VISU_METHOD_SPIN_GLOBAL: arrows are scaled globaly.
 * @VISU_METHOD_SPIN_N_MODULUS_MODES: a flag to count the number of modes.
 *
 * Different policy to render the spin depending on the modulus.
 */
typedef enum
  {
    VISU_METHOD_SPIN_CONSTANT,    
    VISU_METHOD_SPIN_PER_TYPE,  
    VISU_METHOD_SPIN_GLOBAL,
    VISU_METHOD_SPIN_N_MODULUS_MODES
  } VisuMethodSpinModulusPolicy;

VisuMethodSpin* visu_method_spin_new(void);
VisuMethodSpin* visu_method_spin_getDefault(void);

const gfloat* visu_method_spin_getSpinVector(const VisuMethodSpin *method,
                                             const VisuDataSpin *dataSpin,
                                             const VisuNode *node,
                                             gfloat *ratio, gfloat rgb[3],
                                             gboolean *withAtomic);

G_END_DECLS

#endif
