/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2016)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2016)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include "fileDump.h"

/**
 * SECTION:fileDump
 * @short_description: A generic class defining interface to export
 * #VisuData into text formats.
 *
 * <para>Instance of this class can be used to export any #VisuData
 * into other text formats.</para>
 */

struct _VisuDumpDataPrivate
{
  VisuDumpDataWriteFunc writeFunc;
};

/* Local callbacks */

G_DEFINE_TYPE_WITH_CODE(VisuDumpData, visu_dump_data, VISU_TYPE_DUMP,
                        G_ADD_PRIVATE(VisuDumpData))

static void visu_dump_data_class_init(VisuDumpDataClass *klass _U_)
{
  g_debug("Visu Node Scene: creating the class of the object.");
  /* g_debug("                - adding new signals ;"); */
}

static void visu_dump_data_init(VisuDumpData *self)
{
  self->priv = visu_dump_data_get_instance_private(self);
  self->priv->writeFunc = NULL;
}

/**
 * visu_dump_data_new:
 * @descr: a description.
 * @patterns: (array zero-terminated=1): a list of file patterns.
 * @method: (scope call): the write method.
 *
 * Creates a new #VisuDumpData object.
 *
 * Since: 3.8
 *
 * Returns: (transfer full): a newly created #VisuDumpData object.
 **/
VisuDumpData* visu_dump_data_new(const gchar* descr, const gchar** patterns,
                                 VisuDumpDataWriteFunc method)
{
  VisuDumpData *dump;

  dump = VISU_DUMP_DATA(g_object_new(VISU_TYPE_DUMP_DATA,
                                     "name", descr, "ignore-type", FALSE, NULL));
  tool_file_format_addPatterns(TOOL_FILE_FORMAT(dump), patterns);
  dump->priv->writeFunc = method;
  
  return dump;
}

/**
 * visu_dump_data_write:
 * @dump: a #VisuDump object ;
 * @fileName: (type filename): a string that defined the file to write to ;
 * @dataObj: the #VisuData to be exported ;
 * @error: a location to store some error (not NULL) ;
 *
 * Use the write function of @dump to export the current @dataObj to
 * file @fileName.
 *
 * Since: 3.6
 *
 * Returns: TRUE if dump succeed.
 */
gboolean visu_dump_data_write(const VisuDumpData *dump, const char* fileName,
                              VisuData *dataObj, GError **error)
{
  g_return_val_if_fail(VISU_IS_DUMP_DATA(dump) && dump->priv->writeFunc, FALSE);

  return dump->priv->writeFunc(dump, fileName, dataObj, error);
}
