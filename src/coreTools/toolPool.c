/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2017)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2017)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include "toolPool.h"

/**
 * SECTION: toolPool
 * @short_description: a storage container that can uniquely stores
 * #GBoxed objects.
 *
 * <para>This storage is intended to be a signaling container for a
 * given type of #GBoxed objects. In addition, these objects are
 * guaranteed to be unique in the container.</para>
 */

enum
  {
    PROP_0,
    PROP_TYPE,
    N_PROP
  };
static GParamSpec *_properties[N_PROP];

enum
  {
    ADDED_SIGNAL,
    VISU_NB_SIGNAL
  };
static guint _signals[VISU_NB_SIGNAL];

/**
 * ToolPool:
 *
 * Structure defining #ToolPool objects.
 *
 * Since: 3.8
 */

struct _ToolPoolPrivate
{
  GType type;
  GCompareFunc compare;

  GList *lst;
};

static void tool_pool_finalize    (GObject* obj);
static void tool_pool_get_property(GObject* obj, guint property_id,
                                   GValue *value, GParamSpec *pspec);
static void tool_pool_set_property(GObject* obj, guint property_id,
                                   const GValue *value, GParamSpec *pspec);

G_DEFINE_TYPE_WITH_CODE(ToolPool, tool_pool, G_TYPE_OBJECT,
                        G_ADD_PRIVATE(ToolPool))

static void tool_pool_class_init(ToolPoolClass *klass)
{
  /* Connect the overloading methods. */
  G_OBJECT_CLASS(klass)->finalize     = tool_pool_finalize;
  G_OBJECT_CLASS(klass)->get_property = tool_pool_get_property;
  G_OBJECT_CLASS(klass)->set_property = tool_pool_set_property;

  /**
   * ToolPool::type:
   *
   * The type contained in the pool.
   *
   * Since: 3.8
   */
  _properties[PROP_TYPE] =
    g_param_spec_gtype("type", "Type", "stored type.", G_TYPE_BOXED,
                       G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

  g_object_class_install_properties(G_OBJECT_CLASS(klass), N_PROP, _properties);

  /**
   * ToolPool::new-element:
   * @pool: the object emitting the signal.
   * @boxed: the newly added object.
   *
   * A new object is available in the pool.
   *
   * Since: 3.8
   */
  _signals[ADDED_SIGNAL] = 
    g_signal_new("new-element", G_TYPE_FROM_CLASS(klass),
                 G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
                 0, NULL /* accumulator */, NULL /* accu_data */,
                 g_cclosure_marshal_VOID__POINTER,
                 G_TYPE_NONE /* return_type */, 1, G_TYPE_POINTER);
}
static void tool_pool_init(ToolPool *obj)
{
  obj->priv = tool_pool_get_instance_private(obj);

  /* Private data. */
  obj->priv->lst = (GList*)0;
  obj->priv->compare = NULL;
}
static void tool_pool_finalize(GObject* obj)
{
  ToolPool *data;
  GList *tmp;

  data = TOOL_POOL(obj);

  for (tmp = data->priv->lst; tmp; tmp = g_list_next(tmp))
    g_boxed_free(data->priv->type, tmp->data);
  g_list_free(data->priv->lst);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(tool_pool_parent_class)->finalize(obj);
}
static void tool_pool_get_property(GObject* obj, guint property_id,
                                   GValue *value, GParamSpec *pspec)
{
  ToolPool *self = TOOL_POOL(obj);

  switch (property_id)
    {
    case PROP_TYPE:
      g_value_set_gtype(value, self->priv->type);
      break;
    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
      break;
    }
}
static void tool_pool_set_property(GObject* obj, guint property_id,
                                   const GValue *value, GParamSpec *pspec)
{
  ToolPool *self = TOOL_POOL(obj);

  switch (property_id)
    {
    case PROP_TYPE:
      self->priv->type = g_value_get_gtype(value);
      break;
    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
      break;
    }
}

/**
 * tool_pool_new:
 * @type: a type inheriting #G_TYPE_BOXED.
 * @compare: (scope call): a comparison function.
 *
 * Create a new #ToolPool object to store #GBoxed objects defined by
 * @type. These objects must be comparable with @compare function.
 *
 * Since: 3.8
 *
 * Returns: (transfer full): a newly created #ToolPool object.
 **/
ToolPool* tool_pool_new(GType type, GCompareFunc compare)
{
  ToolPool *pool;

  pool = g_object_new(TOOL_TYPE_POOL, "type", type, NULL);
  g_return_val_if_fail(pool, NULL);

  pool->priv->compare = compare;
  return pool;
}

/**
 * tool_pool_add:
 * @pool: a #ToolPool object.
 * @boxed: a #GBoxed pointer.
 *
 * Copy @boxed into @pool if not already present.
 *
 * Since: 3.8
 *
 * Returns: (transfer none): a pointer on the copy of @boxed. This
 * copy is owned by V_Sim.
 **/
gpointer tool_pool_add(ToolPool *pool, gconstpointer boxed)
{
  gpointer elem;

  g_return_val_if_fail(TOOL_IS_POOL(pool), (gpointer)0);

  elem = g_list_find_custom(pool->priv->lst, boxed, pool->priv->compare);
  if (elem)
    return elem;

  elem = g_boxed_copy(pool->priv->type, boxed);
  pool->priv->lst = g_list_append(pool->priv->lst, elem);
  g_signal_emit(pool, _signals[ADDED_SIGNAL], 0, elem);

  return elem;
}
/**
 * tool_pool_take:
 * @pool: a #ToolPool object.
 * @boxed: a #GBoxed pointer.
 *
 * Insert @boxed in @pool and takes ownership.
 *
 * Since: 3.8
 *
 * Returns: (transfer none): a pointer on @boxed. This pointer is
 * owned by V_Sim.
 **/
gpointer tool_pool_take(ToolPool *pool, gpointer boxed)
{
  gpointer elem;

  g_return_val_if_fail(TOOL_IS_POOL(pool), (gpointer)0);

  elem = g_list_find_custom(pool->priv->lst, boxed, pool->priv->compare);
  if (elem)
    return elem;

  pool->priv->lst = g_list_append(pool->priv->lst, boxed);
  g_signal_emit(pool, _signals[ADDED_SIGNAL], 0, boxed);

  return boxed;
}
/**
 * tool_pool_asList:
 * @pool: a #ToolPool object.
 *
 * Get the list of elements stored in @pool.
 *
 * Since: 3.8
 *
 * Returns: (transfer container) (element-type GLib.Value): the list
 * should be freed with g_list_free() when no longer needed.
 **/
GList* tool_pool_asList(ToolPool *pool)
{
  g_return_val_if_fail(TOOL_IS_POOL(pool), (GList*)0);

  return g_list_copy(pool->priv->lst);
}
/**
 * tool_pool_getById:
 * @pool: a #ToolPool object.
 * @num: an integer (>0).
 *
 * This function retrieves the nth stored object. Number 0, is the last
 * added object.
 *
 * Returns: (transfer none): the corresponding object, or NULL if none
 * has been found.
 */
gpointer tool_pool_getById(ToolPool *pool, guint num)
{
  GList *tmpLst;

  g_return_val_if_fail(TOOL_IS_POOL(pool), (gpointer)0);

  tmpLst = g_list_nth(pool->priv->lst, num);
  if (tmpLst)
    return tmpLst->data;
  else
    return (gpointer)0;
}
/**
 * tool_pool_getByData:
 * @pool: a #ToolPool object.
 * @boxed: a #GBoxed object.
 *
 * This function retrieves the stored object that is equivalent to @boxed.
 *
 * Since: 3.8
 *
 * Returns: (transfer none): the corresponding object, or NULL if none
 * has been found.
 **/
gpointer tool_pool_getByData(ToolPool *pool, gconstpointer boxed)
{
  GList *tmpLst;

  g_return_val_if_fail(TOOL_IS_POOL(pool), (gpointer)0);

  tmpLst = g_list_find_custom(pool->priv->lst, boxed, pool->priv->compare);
  if (tmpLst)
    return tmpLst->data;
  else
    return (gpointer)0;
}
/**
 * tool_pool_index:
 * @pool: a #ToolPool object.
 * @boxed: an object.
 *
 * Retrieve the storage index of @boxed if it exists, or -1 if not.
 *
 * Since: 3.8
 *
 * Returns: an index.
 **/
gint tool_pool_index(ToolPool *pool, gconstpointer boxed)
{
  GList *tmp;
  gint i;

  g_return_val_if_fail(TOOL_IS_POOL(pool), -1);
  
  for (tmp = pool->priv->lst, i = 0; tmp; tmp = g_list_next(tmp), i += 1)
    if (pool->priv->compare(tmp->data, boxed) == 0)
      return i;
  return -1;
}
