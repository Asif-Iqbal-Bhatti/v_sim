/* -*- mode: C; c-basic-offset: 2 -*- */
/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2021)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2021)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include <glib.h>

void tool_drawSphere(GArray *vertices, const float xyz[3], float radius, guint nlat);
void tool_drawIcosahedron(GArray* vertices, const float xyz[3], gfloat radius, guint nlat);
void tool_drawEllipsoid(GArray *vertices, const float xyz[3], gfloat theta, gfloat phi,
                        float radius, gfloat ratio, guint nlat);
void tool_drawTorus(GArray *vertices, const gfloat xyz[3], gfloat theta, gfloat phi,
                    gfloat radius, gfloat ratio, guint nlat);
void tool_drawCube(GArray *vertices, const float xyz[3], float size);
void tool_drawCylinder(GArray *vertices, const float xyz1[3], const float xyz2[3],
                       float radius, guint nlat, gboolean filled);
void tool_drawCone(GArray *vertices, const float xyz1[3], const float xyz2[3],
                   float radius, guint nlat, gboolean filled);
void tool_drawArrow(GArray *vertices, const float xyz1[3], const float xyz2[3],
                    float tailRadius, float hatRadius, float ratio, guint nlat);
gfloat tool_drawAngle(GArray *vertices, const float xyzRef[3], const float xyzRef2[3],
                      const float xyz[3], gfloat at[3]);
