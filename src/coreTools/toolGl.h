/* -*- mode: C; c-basic-offset: 2 -*- */
/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2022)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2022)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#ifndef TOOLGL_H
#define TOOLGL_H

#include <glib.h>

/**
 * TOOL_GL_ERROR: (skip)
 *
 * Internal function for error handling.
 */
#define TOOL_GL_ERROR tool_gl_getErrorQuark()
GQuark tool_gl_getErrorQuark();
/**
 * ToolGlErrorFlag:
 * @ERROR_VERTEX_COMPILATION: error on vertex shader compilation.
 * @ERROR_FRAGMENT_COMPILATION: error on fragment shader compilation.
 * @ERROR_PROGRAM_LINK: error linking GPGPU program.
 * @ERROR_SHADER_ID: wrong shader id.
 * @ERROR_CREATE_OBJECT: error on GPU object creation.
 *
 * Possible errors when dealing with GL contexts.
 *
 * Since: 3.9
 */
typedef enum
  {
   ERROR_VERTEX_COMPILATION,
   ERROR_FRAGMENT_COMPILATION,
   ERROR_PROGRAM_LINK,
   ERROR_SHADER_ID,
   ERROR_CREATE_OBJECT
  } ToolGlErrorFlag;

/**
 * ToolGlUniformIds:
 * @UNIFORM_MVP: the MVP matrix
 * @UNIFORM_MV: only the MV matrix
 * @UNIFORM_OFFSET: an offset applied in model view coordinates.
 * @UNIFORM_TRANSLATION: a global translation
 * @UNIFORM_RESOLUTION: the viewport size
 * @UNIFORM_FOG: the fog start and end position
 * @UNIFORM_FOG_COLOR: the fog colour
 * @UNIFORM_COLOR: a global color
 * @UNIFORM_STIPPLE: the line stipple
 * @UNIFORM_TEXT_DIM: a texture dimension
 * @UNIFORM_TEXT_XYZ_POS: a texture position in object coordinates
 * @UNIFORM_TEXT_RED_POS: a texture position in window coordinates
 * @UNIFORM_TEXT_ALIGN: text alignment
 * @UNIFORM_LIGHT_POS: the light position
 * @UNIFORM_CAMERA_POS: the camera position
 * @UNIFORM_AMBIENT: the percentage of ambient light an object is reflecting
 * @UNIFORM_DIFFUSE: the percentage of directional light an object is reflecting
 * @UNIFORM_SPECULAR: the percentage of specular light an object is reflecting
 * @UNIFORM_SHININESS: the size of the specular spot
 * @UNIFORM_EMISSION: the percentage of light an object is emitting
 * @UNIFORM_USER: start of uniforms defined by the user
 * @N_UNIFORMS: total number of uniforms
 *
 * Uniform values in various shaders.
 *
 * Since: 3.9
 */
typedef enum
  {
   UNIFORM_MVP,
   UNIFORM_MV,
   UNIFORM_OFFSET,
   UNIFORM_TRANSLATION,
   UNIFORM_RESOLUTION,
   UNIFORM_FOG,
   UNIFORM_FOG_COLOR,
   UNIFORM_COLOR,
   UNIFORM_STIPPLE,
   UNIFORM_TEXT_DIM,
   UNIFORM_TEXT_XYZ_POS,
   UNIFORM_TEXT_RED_POS,
   UNIFORM_TEXT_ALIGN,
   UNIFORM_LIGHT_POS,
   UNIFORM_CAMERA_POS,
   UNIFORM_AMBIENT,
   UNIFORM_DIFFUSE,
   UNIFORM_SPECULAR,
   UNIFORM_SHININESS,
   UNIFORM_EMISSION,
   UNIFORM_USER,
   N_UNIFORMS = UNIFORM_USER + 12
  } ToolGlUniformIds;

gboolean tool_gl_createShader(guint *id, const char *vertexSource, const char *fragmentSource, GError **error);
gboolean tool_gl_createFlatShader(guint *id, gint locs[N_UNIFORMS],
                                  gboolean withFog, GError **error);
gboolean tool_gl_createSimpleShader(guint *id, gint locs[N_UNIFORMS],
                                    gboolean withFog, GError **error);
gboolean tool_gl_createLineShader(guint *id, gint locs[N_UNIFORMS],
                                  gboolean withFog, GError **error);
gboolean tool_gl_createColoredLineShader(guint *id, gint locs[N_UNIFORMS],
                                         gboolean withFog, GError **error);
gboolean tool_gl_createTextShader(guint *id, gint locs[N_UNIFORMS],
                                  gboolean withFog, GError **error);
gboolean tool_gl_createMaterialShader(guint *id, gint locs[N_UNIFORMS],
                                      gboolean withFog, GError **error);
gboolean tool_gl_createOrthoShader(guint *id, gint locs[N_UNIFORMS], GError **error);

#endif
