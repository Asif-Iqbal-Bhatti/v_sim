/* -*- mode: C; c-basic-offset: 2 -*- */
/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2023)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2023)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef IFACE_DUMPABLE_H
#define IFACE_DUMPABLE_H

#include <glib.h>
#include <glib-object.h>

G_BEGIN_DECLS

/* Dumpable interface. */
#define VISU_TYPE_DUMPABLE                (visu_dumpable_get_type ())
#define VISU_DUMPABLE(obj)                (G_TYPE_CHECK_INSTANCE_CAST ((obj), VISU_TYPE_DUMPABLE, VisuDumpable))
#define VISU_IS_DUMPABLE(obj)             (G_TYPE_CHECK_INSTANCE_TYPE ((obj), VISU_TYPE_DUMPABLE))
#define VISU_DUMPABLE_GET_INTERFACE(inst) (G_TYPE_INSTANCE_GET_INTERFACE ((inst), VISU_TYPE_DUMPABLE, VisuDumpableInterface))

typedef struct _VisuDumpableInterface VisuDumpableInterface;
typedef struct _VisuDumpable VisuDumpable; /* dummy object */

/**
 * VisuDumpable:
 *
 * Interface object.
 *
 * Since: 3.9
 */

/**
 * VisuDumpableInterface:
 * @parent: its parent.
 * @save: a method used to dump the data to disk.
 *
 * The different routines common to objects implementing a #VisuDumpable interface.
 * 
 * Since: 3.9
 */
struct _VisuDumpableInterface
{
  GTypeInterface parent;

  gboolean (*save)(const VisuDumpable *dumpable, GError **error);
};

GType visu_dumpable_get_type(void);

gboolean visu_dumpable_save(const VisuDumpable *dumpable, GError **error);

G_END_DECLS

#endif
