/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef LIGHT_H
#define LIGHT_H

#include <glib.h>
#include <glib-object.h>

G_BEGIN_DECLS

typedef struct _VisuGlLights VisuGlLights;

GType visu_gl_lights_get_type(void);
/**
 * VISU_TYPE_GL_LIGHTS:
 *
 * The type of #VisuGlLights objects.
 */
#define VISU_TYPE_GL_LIGHTS (visu_gl_lights_get_type())

VisuGlLights* visu_gl_lights_new();
VisuGlLights* visu_gl_lights_ref(VisuGlLights *env);
void visu_gl_lights_unref(VisuGlLights *env);
void visu_gl_lights_free(VisuGlLights *env);

typedef struct _VisuGlLight VisuGlLight;
struct _VisuGlLight
{  
  gboolean enabled;
  float ambient[4];
  float diffuse[4];
  float specular[4];
  float position[4];
  float multiplier;
};
GType visu_gl_light_get_type(void);
/**
 * VISU_TYPE_GL_LIGHT:
 *
 * The type of #VisuGlLight objects.
 */
#define VISU_TYPE_GL_LIGHT (visu_gl_light_get_type())

gboolean visu_gl_lights_available(VisuGlLights *env);
gboolean visu_gl_lights_add(VisuGlLights *env, VisuGlLight *light);
gboolean visu_gl_lights_remove(VisuGlLights *env, VisuGlLight *light);
GList* visu_gl_lights_getList(VisuGlLights *env);
gboolean visu_gl_lights_removeAll(VisuGlLights *env);

VisuGlLight* visu_gl_light_newDefault();

G_END_DECLS

#endif
