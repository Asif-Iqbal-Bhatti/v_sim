/* -*- mode: C; c-basic-offset: 2 -*- */
/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef CAMERA_H
#define CAMERA_H

#include <glib.h>
#include <glib-object.h>
#include <visu_box.h>
#include <iface_boxed.h>
#include <coreTools/toolMatrix.h>

G_BEGIN_DECLS

/***************************/
/* The #VisuGlView object. */
/***************************/
/**
 * VISU_TYPE_GL_VIEW:
 *
 * The type of #VisuGlView objects.
 */
#define VISU_TYPE_GL_VIEW (visu_gl_view_get_type())
/**
 * VISU_GL_VIEW:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuGlView type.
 */
#define VISU_GL_VIEW(obj)	     (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_GL_VIEW, VisuGlView))
/**
 * VISU_GL_VIEW_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuGlViewClass.
 */
#define VISU_GL_VIEW_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_GL_VIEW, VisuGlViewClass))
/**
 * VISU_IS_GL_VIEW:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuGlView object.
 */
#define VISU_IS_GL_VIEW(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_GL_VIEW))
/**
 * VISU_IS_GL_VIEW_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuGlViewClass class.
 */
#define VISU_IS_GL_VIEW_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_GL_VIEW))
/**
 * VISU_GL_VIEW_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_GL_VIEW_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_GL_VIEW, VisuGlViewClass))

typedef struct _VisuGlViewPrivate VisuGlViewPrivate;
typedef struct _VisuGlView VisuGlView;

/**
 * VisuGlView:
 *
 * A container structure to deal with OpenGL observer position, size of rendering
 * viewport...
 */
struct _VisuGlView
{
  VisuObject parent;

  ToolGlCamera camera;

  VisuGlViewPrivate *priv;
};

typedef struct _VisuGlViewClass VisuGlViewClass;
/**
 * VisuGlViewClass:
 * @parent: the parent class.
 *
 * An opaque structure.
 */
struct _VisuGlViewClass
{
  VisuObjectClass parent;
};

GType visu_gl_view_get_type(void);
VisuGlView* visu_gl_view_new(void);
VisuGlView* visu_gl_view_new_full(guint w, guint h, const ToolGlCamera *camera);

gboolean visu_gl_view_setThetaPhiOmega(VisuGlView *view, float valueTheta,
				 float valuePhi, float valueOmega, int mask);
gboolean visu_gl_view_setXsYs(VisuGlView *view,
				    float valueX, float valueY, int mask);
gboolean visu_gl_view_setGross(VisuGlView *view, float value);
gboolean visu_gl_view_setPersp(VisuGlView *view, float value);

gboolean visu_gl_view_setRefLength(VisuGlView *view, float lg, ToolUnits units);
gboolean visu_gl_view_setObjectRadius(VisuGlView *view, float lg, ToolUnits units);
gboolean visu_gl_view_setViewport(VisuGlView *view, guint width, guint height);
guint visu_gl_view_getWidth(const VisuGlView *view);
guint visu_gl_view_getHeight(const VisuGlView *view);

gint visu_gl_view_getDetailLevel(const VisuGlView *view, float dimension);
void visu_gl_view_rotateBox(VisuGlView *view, float dTheta, float dPhi, float angles[2]);
void visu_gl_view_rotateCamera(VisuGlView *view, float dTheta, float dPhi, float angles[3]);
void visu_gl_view_alignToAxis(VisuGlView *view, ToolXyzDir axis);
float visu_gl_view_getZCoordinate(VisuGlView *view, const float xyz[3]);
void visu_gl_view_getRealCoordinates(VisuGlView *view, float xyz[3],
                                     float winx, float winy, float winz);
void visu_gl_view_getWinCoordinates(const VisuGlView *view, gfloat win[3],
                                    const gfloat xyz[3]);

gboolean visu_gl_view_setPrecision(VisuGlView *view, float value);
float visu_gl_view_getPrecision(const VisuGlView *view);
float visu_gl_view_getFileUnitPerPixel(const VisuGlView *view);

void visu_gl_view_getProjection(const VisuGlView *view, ToolGlMatrix *P);
void visu_gl_view_getModelView(const VisuGlView *view, ToolGlMatrix *MV);
void visu_gl_view_getModelViewProjection(const VisuGlView *view, ToolGlMatrix *MVP);

G_END_DECLS

#endif
