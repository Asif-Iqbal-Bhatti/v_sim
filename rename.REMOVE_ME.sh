#!/bin/bash

for i in `find {src,lib,Documentation} -not -wholename \*\.svn\* -name \*.[ch]\* -exec grep -q "$1" {} \; -print` ; do sed -i "s;$1;$2;g" $i; echo $i; done
